<?php
require_once ('heliocms/core.php');
?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Cómo jugar - <?php echo $sitename; ?></title>
    <!--[if (lte IE 9)|(IEMobile)]><script>window.location = '<?php echo $site; ?>/br/upgrade/';</script>
        <!--<![endif]-->
    <meta name="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Como Jogar">
    <meta name="twitter:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="Como Jogar">
    <meta itemprop="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $og; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="//pagead2.googlesyndication.com/pagead/expansion_embed.js?source=safeframe"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20160119135516/recaptcha__pt_br.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ecommerce.js"></script>
    <script async="" type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="d1a83d6" data-releasestage="hhbr"></script>
    <script src="https://partner.googleadservices.com/gpt/pubads_impl_79.js" async=""></script>
    <link rel="stylesheet" href="https://d3hmp0045zy3cs.cloudfront.net/2.2.21/providers.css" type="text/css">
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/osd.js"></script>
</head>

<body class="" client-disable-scrollbars="">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view style="" class="">
            <div active="playing<?php echo $sitename; ?>" class="header header--small">
                <div class="header__background">
                    <div class="header__hotel"></div>
                    <header class="header__wrapper wrapper"><a href="/" class="header__habbo__logo"><h1 class="header__habbo__name" id="ga-linkid-habbo"><?php echo $sitename; ?></h1></a>
<?php if (isset($_SESSION['id'])) { ?>
				<!-- requireSession:  -->
                <div require-session="" class="header__aside header__aside--user-menu">
                    <div false-on-outside-click="toggle" class="user-menu">
                        <div class="user-menu__header">
                            <a id="ul-click" ng-click="click()">
                                <div class="user-menu__avatar__wrapper"><img width="54" height="62" class="user-menu__avatar imager" figure="<?php echo $user_q['look']; ?>" size="bighead" alt="<?php echo $user_q['username']; ?>" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&headonly=1&size=b&gesture=sml&direction=2&head_direction=2&action=std"></div>
                                <div class="user-menu__name__wrapper">
                                    <div id="ul-toggle" class="user-menu__name" ng-class="{ 'user-menu__name--open': toggle }">
                                        <div class="user-menu__name__container"><?php echo $user_q['username']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <ul ng-hide="!toggle" id="ul-hide" style="display: none" class="user-menu__list">
                            <li class="user-menu__item"><a ng-href="/profile/<?php echo $user_q['username']; ?>" ng-class="{ 'user-menu__link--active': isMyProfileActive() }" class="user-menu__link user-menu__link--profile" translate="NAVIGATION_PROFILE" href="/profile/<?php echo $user_q['username']; ?>">Ver mi perfil público</a></li>
                            <li class="user-menu__item"><a href="/settings" ng-class="{ 'user-menu__link--active': isSettingsActive() }" class="user-menu__link user-menu__link--settings" translate="NAVIGATION_SETTINGS">Ajustes</a></li>
                            <li class="user-menu__item"><a ng-href="/api/public/help?returnTo=https://help.habbo.es" class="user-menu__link user-menu__link--help" target="_blank" translate="NAVIGATION_HELP" href="/api/public/help?returnTo=https://help.habbo.es">Ayuda</a></li>
                            <li class="user-menu__item"><a onclick="logout()" ng-click="logout()" class="user-menu__link user-menu__link--logout" translate="NAVIGATION_LOGOUT">Salir</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end ngIf: function (){return t.hasSession()} -->
				<?php }else{ ?>
                        <!-- requireNoSession:  -->
                        <div require-no-session="" class="header__aside">
                            <button ng-click="" data-toggle="modal" data-target="#login" class="header__login__button"><span class="header__login__icon" translate="LOGIN">Acceder</span></button>
                        </div>
                        <!-- end ngIf: function (){
"use strict";
return!t.hasSession()} -->
				<?php } ?>
                        <!-- requireNoSession:  -->
                    </header>
                    <nav active="playing<?php echo $sitename; ?>" class="navigation">
                        <ul class="navigation__menu">
                            <li class="navigation__item"><a href="/" ng-class="{ 'navigation__link--active': isActive('home') }" class="navigation__link navigation__link--home" translate="NAVIGATION_HOME" id="ga-linkid-home">Inicio</a></li>
                            <li class="navigation__item"><a href="/community" ng-class="{ 'navigation__link--active': isActive('community') }" class="navigation__link navigation__link--community" translate="NAVIGATION_COMMUNITY" id="ga-linkid-community">Comunidad</a></li>
                            <li class="navigation__item"><a href="/playing-habbo" ng-class="{ 'navigation__link--active': isActive('playing<?php echo $sitename; ?>') }" class="navigation__link navigation__link--playing-habbo navigation__link--active" translate="NAVIGATION_PLAYING_HABBO" id="ga-linkid-playing-habbo">Descubre <?php echo $sitename; ?></a></li>
                            <!-- requireSession:  -->
                            <li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel">
                                <!-- end ngIf: function (){
"use strict";
return a.isEnabled()||!t.test(e.navigator.userAgent)} -->
                            </li>
                            <!-- end ngIf: function (){
"use strict";
return t.hasSession()} -->
                        												<?php if (isset($_SESSION['id'])) { ?>
					<li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel"><!-- requireFlash:  --><a require-flash="" href="/hotel" class="hotel-button" id="ga-linkid-hotel"><span class="hotel-button__text" translate="NAVIGATION_HOTEL">Hotel</span></a><!-- end ngIf: function (){return a.isEnabled()||!t.test(e.navigator.userAgent)} --></li>
                    <?php } ?>
                        </ul>
                    </nav>
                    <div class="wrapper" ng-transclude=""></div>
                </div>
            </div>
            <section>
                <nav ng-hide="tabs.length < 2" false-on-outside-click="isOpen" class="tabs">
                    <div class="tabs__wrapper">
                        <!-- ngIf: titleKey -->
                        <div id="tab-click" ng-click="isOpen = !isOpen" class="tabs__toggle">
                            <div id="tab-arrow" ng-class="{'tabs__toggle__title--active': isOpen}" class="tabs__toggle__title" translate="PLAYING_HABBO_HOW_TO_PLAY_TAB">Cómo jugar</div>
                        </div>
                        <ul id="tab-hide" class="tabs__menu ng-hide" ng-hide="!isOpen" ng-transclude="">
                            <li path="/playing-habbo/what-is-habbo" translation-key="PLAYING_HABBO_WHAT_IS_HABBO_TAB" class="tabs__item"><a style="" href="/playing-habbo/what-is-habbo" ng-href="/playing-habbo/what-is-habbo" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_WHAT_IS_HABBO_TAB">¿Qué es <?php echo $sitename; ?>?</a></li>
                            <li path="/playing-habbo/how-to-play" translation-key="PLAYING_HABBO_HOW_TO_PLAY_TAB" class="tabs__item"><a style="" href="/playing-habbo/how-to-play" ng-href="/playing-habbo/how-to-play" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link tabs__link--active" translate="PLAYING_HABBO_HOW_TO_PLAY_TAB">Cómo jugar</a></li>
                            <li path="/playing-habbo/habbo-way" translation-key="PLAYING_HABBO_HABBO_WAY_TAB" class="tabs__item"><a href="/playing-habbo/habbo-way" ng-href="/playing-habbo/habbo-way" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HABBO_WAY_TAB">Manera <?php echo $sitename; ?></a></li>
                            <li path="/playing-habbo/safety" translation-key="PLAYING_HABBO_SAFETY_TAB" class="tabs__item"><a href="/playing-habbo/safety" ng-href="/playing-habbo/safety" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_SAFETY_TAB">Seguridad</a></li>
                            <li path="/playing-habbo/help" translation-key="PLAYING_HABBO_HELP_TAB" class="tabs__item"><a href="/playing-habbo/help" ng-href="/playing-habbo/help" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HELP_TAB">Ayuda</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- uiView:  -->
                <section style="" class="wrapper wrapper--content" ui-view="">
                    <article class="main main--fixed static-content">
                        <h1>Cómo jugar</h1>
                        <p>Ya has puesto un look a tu avatar, has hecho confortable tu Territorio y Frank te ha mostrado cómo funcionan algunas cosas del Hotel… Así que, ¿qué es lo siguiente?</p>
                        <p>Aquí te dejamos algunas ideas:</p>
                        <p><img src="<?php echo $aka; ?>/c_images/HowToPlay/navigator_es.png" alt="Navegador" class="align-right"></p>
                        <h3>Explorar salas</h3>
                        <p>Haz clic en el Navegador y selecciona una de las salas públicas en donde podrás chatear con otros <?php echo $sitename; ?>s.</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/HowToPlay/ask_friend_es.png" alt="Faça amigos" class="align-right"></p>
                        <h3>Hacer amigos</h3>
                        <p>¡Haz clic sobre un <?php echo $sitename; ?> y pídele amistad o dale respetos!</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/HowToPlay/citizenship_es.png" alt="Cidadania <?php echo $sitename; ?>" class="align-right"></p>
                        <h3>COMPLETA TU <?php echo $sitename; ?> CAMINO DE LA CIUDADANÍA</h3>
                        <p>Para conocer el Hotel un poco mejor, empieza por hacer clic en la herramienta Ayuda en la esquina superior derecha en el hotel.</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/HowToPlay/gamehub_es.png" alt="Sala de Jogos" class="align-right"></p>
                        <h3>VISITA SALAS DE JUEGOS</h3>
                        <p>Encuentra la Central de Juegos entre las salas públicas del navegador. ¡Una vez aquí, utiliza cualquiera de las máquinas arcade para ir a un juego!</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/HowToPlay/shop.png" alt="<?php echo $sitename; ?> Loja" class="align-right"></p>
                        <h3>IR DE COMPRAS</h3>
                        <p>¡Ve a la tienda de Duckets y mira qué puedes conseguir gratis por tus Duckets!</p>
                        <hr>
                        <h3>ÉCHALE UN OJO A LAS ACTIVIDADES</h3>
                        <p>¡En la <a href="/">Home</a> encontrarás las últimas noticias, competiciones y eventos generales de <?php echo $sitename; ?>!</p>
                        <p>¡Una vez que hayas hecho un poco de todo esto estarás en camino de ser un auténtico <?php echo $sitename; ?> ciudadano!</p>
                        <blockquote>
                            <h3>¡ENTRA EN <?php echo $sitename; ?>!</h3>
                            <p>¿No te has registrado en <?php echo $sitename; ?> aún? ¡<a href="/registration">Únete ahora</a>!</p>
                        </blockquote>
                    </article>
                    <article style="" key="common/box_habbo_way" class="aside aside--box aside--fixed aside--push-down static-content" ng-show="show">
                        <h3>MANERA <?php echo $sitename; ?></h3>
                        <p>Sigue la <a href="/playing-habbo/habbo-way">Manera <?php echo $sitename; ?></a> y podrás disfrutar de todo lo que te ofrece el Hotel</p>
                    </article>
                    <article style="" key="common/box_learn_how_to_stay_safe" class="aside aside--box aside--fixed static-content" ng-show="show">
                        <h3>CONSEJOS DE SEGURIDAD</h3>
                        <p>¡Protégete con conciencia! Aprende cómo <a href="/playing-habbo/safety">Navegar seguro por Internet</a>.</p>
                    </article>
                    <article key="common/box_need_help" class="aside aside--box aside--fixed static-content" ng-show="show">
                        <h3>¿NECESITAS AYUDA?</h3>
                        <p>Consulta cómo puedes echarte un cable o pedir la ayuda de un moderador en nuestro apartado de <a href="/playing-habbo/help">Atención al Usuario</a>. También incluye una lista de números de teléfono y páginas web en caso de que necesites hablar con alguien. Si no encuentras una respuesta adecuada en esta página, ponte en contacto con el <a href="https://help.habbo.es/">Ayudante de <?php echo $sitename; ?></a></p>
                    </article>
                </section>
            </section>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <!-- requireSession:  -->
    <div require-session="">
        <div ngsf-fullscreen="" client-close-fullscreen-on-hide="" ng-class="{ 'client--visible': visible }" class="client">
            <div class="client__buttons">
                <button ng-click="close()" class="client__close"><span class="client__close__text" translate="CLIENT_TO_WEB_BUTTON">INÍCIO</span></button>
                <button ngsf-toggle-fullscreen="" class="client__fullscreen"><i show-if-fullscreen="false" class="client__fullscreen__icon icon--fullscreen"></i> <i show-if-fullscreen="" class="client__fullscreen__icon icon--fullscreen-back ng-hide"></i></button>
            </div>
            <!-- ngIf: isOpen &&flashEnabled -->
            <iframe src="https://www.habbo.es/client/hhbr-50f31989cb82a102e3343fc8d28aaf82/fbce0f94-4748-493e-b4d5-ac81e6091a81-73312456" ng-src="https://www.habbo.es/client/hhbr-50f31989cb82a102e3343fc8d28aaf82/fbce0f94-4748-493e-b4d5-ac81e6091a81-73312456" id="hotel-client" client-communication="" ng-if="isOpen &amp;&amp;flashEnabled" class="client__frame"></iframe>
            <!-- end ngIf: isOpen &&flashEnabled -->
            <!-- ngIf: isOpen && flashEnabled && !running -->
            <!-- ngIf: isOpen && !flashEnabled -->
            <!-- ngIf: !isOpen -->
        </div>
    </div>
    <!-- end ngIf: function (){
"use strict";
return t.hasSession()} -->
    <!-- requireNoSession:  -->
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root"></div>
    <script src="//connect.facebook.net/pt_BR/sdk.js" async="" id="facebook-jssdk"></script>
</body>

</html>
<?php
if (!isset($_SESSION['id'])) {
require_once ('includes/modal_login.php');
}
?>