<?php
require_once ('heliocms/core.php');
?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Manera <?php echo $sitename; ?> - <?php echo $sitename; ?></title>
    <!--[if (lte IE 9)|(IEMobile)]><script>window.location = '<?php echo $site; ?>/br/upgrade/';</script>
        <!--<![endif]-->
    <meta name="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo $sitename; ?> Etiqueta">
    <meta name="twitter:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="<?php echo $sitename; ?> Etiqueta">
    <meta itemprop="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $og; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="//pagead2.googlesyndication.com/pagead/expansion_embed.js?source=safeframe"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20160119135516/recaptcha__pt_br.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ecommerce.js"></script>
    <script async="" type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="d1a83d6" data-releasestage="hhbr"></script>
    <script src="https://partner.googleadservices.com/gpt/pubads_impl_79.js" async=""></script>
    <link rel="stylesheet" href="https://d3hmp0045zy3cs.cloudfront.net/2.2.21/providers.css" type="text/css">
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/osd.js"></script>

<body class="" client-disable-scrollbars="">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view style="" class="">
            <div active="playing<?php echo $sitename; ?>" class="header header--small">
                <div class="header__background">
                    <div class="header__hotel"></div>
                    <header class="header__wrapper wrapper"><a href="/" class="header__habbo__logo"><h1 class="header__habbo__name" id="ga-linkid-habbo"><?php echo $sitename; ?></h1></a>
                        						                <?php if (isset($_SESSION['id'])) { ?>
				<!-- requireSession:  -->
                <div require-session="" class="header__aside header__aside--user-menu">
                    <div false-on-outside-click="toggle" class="user-menu">
                        <div class="user-menu__header">
                            <a id="ul-click" ng-click="click()">
                                <div class="user-menu__avatar__wrapper"><img width="54" height="62" class="user-menu__avatar imager" figure="<?php echo $user_q['look']; ?>" size="bighead" alt="<?php echo $user_q['username']; ?>" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&headonly=1&size=b&gesture=sml&direction=2&head_direction=2&action=std"></div>
                                <div class="user-menu__name__wrapper">
                                    <div id="ul-toggle" class="user-menu__name" ng-class="{ 'user-menu__name--open': toggle }">
                                        <div class="user-menu__name__container"><?php echo $user_q['username']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <ul ng-hide="!toggle" id="ul-hide" style="display: none" class="user-menu__list">
                            <li class="user-menu__item"><a ng-href="/profile/<?php echo $user_q['username']; ?>" ng-class="{ 'user-menu__link--active': isMyProfileActive() }" class="user-menu__link user-menu__link--profile" translate="NAVIGATION_PROFILE" href="/profile/<?php echo $user_q['username']; ?>">Ver mi perfil público</a></li>
                            <li class="user-menu__item"><a href="/settings" ng-class="{ 'user-menu__link--active': isSettingsActive() }" class="user-menu__link user-menu__link--settings" translate="NAVIGATION_SETTINGS">Ajustes</a></li>
                            <li class="user-menu__item"><a ng-href="/api/public/help?returnTo=https://help.habbo.es" class="user-menu__link user-menu__link--help" target="_blank" translate="NAVIGATION_HELP" href="/api/public/help?returnTo=https://help.habbo.es">Ajuda</a></li>
                            <li class="user-menu__item"><a onclick="logout()" ng-click="logout()" class="user-menu__link user-menu__link--logout" translate="NAVIGATION_LOGOUT">Salir</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end ngIf: function (){return t.hasSession()} -->
				<?php }else{ ?>
                        <!-- requireNoSession:  -->
                        <div require-no-session="" class="header__aside">
                            <button ng-click="" data-toggle="modal" data-target="#login" class="header__login__button"><span class="header__login__icon" translate="LOGIN">Acceder</span></button>
                        </div>
                        <!-- end ngIf: function (){
"use strict";
return!t.hasSession()} -->
				<?php } ?>
                    </header>
                    <nav active="playing<?php echo $sitename; ?>" class="navigation">
                        <ul class="navigation__menu">
                            <li class="navigation__item"><a href="/" ng-class="{ 'navigation__link--active': isActive('home') }" class="navigation__link navigation__link--home" translate="NAVIGATION_HOME" id="ga-linkid-home">Inicio</a></li>
                            <li class="navigation__item"><a href="/community" ng-class="{ 'navigation__link--active': isActive('community') }" class="navigation__link navigation__link--community" translate="NAVIGATION_COMMUNITY" id="ga-linkid-community">Comunidad</a></li>
                            <li class="navigation__item"><a href="/playing-habbo" ng-class="{ 'navigation__link--active': isActive('playing<?php echo $sitename; ?>') }" class="navigation__link navigation__link--playing-habbo navigation__link--active" translate="NAVIGATION_PLAYING_HABBO" id="ga-linkid-playing-habbo">Descubre <?php echo $sitename; ?></a></li>
                            <!-- requireSession:  -->
                        												<?php if (isset($_SESSION['id'])) { ?>
					<li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel"><!-- requireFlash:  --><a require-flash="" href="/hotel" class="hotel-button" id="ga-linkid-hotel"><span class="hotel-button__text" translate="NAVIGATION_HOTEL">Hotel</span></a><!-- end ngIf: function (){return a.isEnabled()||!t.test(e.navigator.userAgent)} --></li>
                    <?php } ?>
                            <!-- end ngIf: function (){
"use strict";
return t.hasSession()} -->
                        </ul>
                    </nav>
                    <div class="wrapper" ng-transclude=""></div>
                </div>
            </div>
            <section>
                <nav ng-hide="tabs.length < 2" false-on-outside-click="isOpen" class="tabs">
                    <div class="tabs__wrapper">
                        <!-- ngIf: titleKey -->
                        <div id="tab-click" ng-click="isOpen = !isOpen" class="tabs__toggle">
                            <div id="tab-arrow" ng-class="{'tabs__toggle__title--active': isOpen}" class="tabs__toggle__title" translate="PLAYING_HABBO_HABBO_WAY_TAB">Manera <?php echo $sitename; ?></div>
                        </div>
                        <ul id="tab-hide" class="tabs__menu ng-hide" ng-hide="!isOpen" ng-transclude="">
                            <li path="/playing-habbo/what-is-habbo" translation-key="PLAYING_HABBO_WHAT_IS_HABBO_TAB" class="tabs__item"><a style="" href="/playing-habbo/what-is-habbo" ng-href="/playing-habbo/what-is-habbo" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_WHAT_IS_HABBO_TAB">¿Qué es <?php echo $sitename; ?>?</a></li>
                            <li path="/playing-habbo/how-to-play" translation-key="PLAYING_HABBO_HOW_TO_PLAY_TAB" class="tabs__item"><a style="" href="/playing-habbo/how-to-play" ng-href="/playing-habbo/how-to-play" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HOW_TO_PLAY_TAB">Cómo jugar</a></li>
                            <li path="/playing-habbo/habbo-way" translation-key="PLAYING_HABBO_HABBO_WAY_TAB" class="tabs__item"><a style="" href="/playing-habbo/habbo-way" ng-href="/playing-habbo/habbo-way" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link tabs__link--active" translate="PLAYING_HABBO_HABBO_WAY_TAB">Manera <?php echo $sitename; ?></a></li>
                            <li path="/playing-habbo/safety" translation-key="PLAYING_HABBO_SAFETY_TAB" class="tabs__item"><a style="" href="/playing-habbo/safety" ng-href="/playing-habbo/safety" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_SAFETY_TAB">Seguridad</a></li>
                            <li path="/playing-habbo/help" translation-key="PLAYING_HABBO_HELP_TAB" class="tabs__item"><a style="" href="/playing-habbo/help" ng-href="/playing-habbo/help" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HELP_TAB">Ayuda</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- uiView:  -->
                <section style="" class="wrapper wrapper--content" ui-view="">
                    <article class="main main--fixed static-content">
                        <h1>Manera <?php echo $sitename; ?></h1>
                        <p>La Manera <?php echo $sitename; ?> es como un código de conducta, una guía sobre cómo los <?php echo $sitename; ?>s deben actuar en el Hotel. ¡Jugando bajo estas reglas te aseguras la diversión en <?php echo $sitename; ?>!</p>
                        <h2>SÍ...</h2>
                        <p><img src="<?php echo $aka; ?>/c_images/HabboWay/habboway_2a.png" alt="Do chat" class="align-right"></p>
                        <h4>CHATEAR</h4>
                        <p>Habla para conocer a otros <?php echo $sitename; ?>s, haz nuevos amigos y ¡diviértete!</p>
                        <h4>CREAR</h4>
                        <p>¡Da rienda suelta a tu creatividad! ¡Lleva tu estilo y tus habilidades de diseño al límite para demostrar que eres el mejor! Desde construir salas épicas hasta crear maravillosos selfies. ¡Podrías ser el próximo Píxel Picasso!</p>
                        <p><img src="<?php echo $aka; ?>/c_images/HabboWay/habboway_5a.png" alt="Do create" class="align-right"></p>
                        <h4>AYUDAR</h4>
                        <p>¡Ayuda a un extraño, gana un amigo! Ayuda siempre a otros <?php echo $sitename; ?>s. Nunca se sabe cuándo serás tú quien necesite dicha ayuda.</p>
                        <h4>TRADEAR</h4>
                        <p>¡Crea tu propio imperio furni a través de los intercambios! Si tienes olfato para los negocios, utiliza el Mercadillo para vender furnis y convertirlos en créditos.</p>
                        <p><img src="<?php echo $aka; ?>/c_images/HabboWay/habboway_1a.png" alt="Do play games" class="align-right"></p>
                        <h4>JUGAR</h4>
                        <p>¡Juega con amigos y crea tus propios juegos! ¡Mételes caña a todos tus adversarios!</p>
                        <h4>BUSCAR NUEVOS AMIGOS</h4>
                        <p>¡Diviértete, conoce a gente de todos los rincones del mundo y, por qué no, encuentra a ese ser pixelado tan especial!</p>
                        <hr>
                        <h2>NO…</h2>
                        <p><img src="<?php echo $aka; ?>/c_images/HabboWay/habboway_2b.png" alt="Don't troll" class="align-right"></p>
                        <h4>TROLEAR</h4>
                        <p>A nadie le gustan los trols. ¡De hecho no le gustan ni a sus madres! Así que el acoso está terminantemente prohibido.</p>
                        <h4>TIMAR</h4>
                        <p>Robar cosas no te hará más rico, sólo hará que te conviertas en un criminal y en un mal ejemplo para los demás.</p>
                        <h4>HACER TRAMPAS</h4>
                        <p>Los tramposos siempre acaban mal, lo único que hacen es estropear la experiencia de los demás.</p>
                        <p><img src="<?php echo $aka; ?>/c_images/HabboWay/habboway_7b.png" alt="Não venda nada por dinheiro real" class="align-right"></p>
                        <h4>VENDER POR DINERO REAL</h4>
                        <p>No vendas tus Furnis a cambio de dinero real. Es muy probable que lo pierdas todo en algún sitio no seguro. Además, estarás tirando a la basura todo el tiempo y esfuerzo que has invertido para llegar hasta donde estás.</p>
                        <h4>REALIZAR O ACEPTAR APUESTAS</h4>
                        <p>El uso de furnis con resultados aleatorios para realizar apuestas puede meterte en problemas. Muestra tus habilidades, ¡no dejes nada al azar!</p>
                        <p><img src="<?php echo $aka; ?>/c_images/HabboWay/habboway_3b.png" alt="Don't cyber" class="align-right"></p>
                        <h4>CIBER</h4>
                        <p>El sexo no está permitido en <?php echo $sitename; ?>, ni tampoco intentar contactar con alguien a través de webcam. Ambas conductas serán penalizadas. Además, recuerda que no debes quedar con nadie que hayas conocido en Internet. A veces las personas no son quienes dicen ser.</p>
                        <hr>
                        <blockquote>
                            <h4>Cómo jugar</h4>
                            <p>¡Sé creativo, sé constructivo, sé social! Mira nuestros <a href="/playing-habbo/how-to-play">consejos sobre qué hacer en <?php echo $sitename; ?></a>.</p>
                        </blockquote>
                    </article>
                    <article style="" key="common/box_learn_how_to_stay_safe" class="aside aside--box aside--fixed aside--push-down static-content" ng-show="show">
                        <h3>Consejos de seguridad</h3>
                        <p>¡Protégete con conciencia! Aprende cómo <a href="/playing-habbo/safety">Navegar seguro por Internet</a>.</p>
                    </article>
                    <article style="" key="common/box_parents_guide" class="aside aside--box aside--fixed static-content" ng-show="show">
                        <h3>Consejos para padres</h3>
                        <p>¿Conoces las herramientas disponibles para que los usuarios puedan divertirse en <?php echo $sitename; ?> dentro de un ambiente seguro? Echa un vistazo a nuestros <a href="https://help.habbo.es/hc/es/articles/221678648">Consejos para Padres en el Ayudante de <?php echo $sitename; ?></a>.</p>
                    </article>
                </section>
            </section>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <!-- requireNoSession:  -->
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root"></div>
    <script src="//connect.facebook.net/pt_BR/sdk.js" async="" id="facebook-jssdk"></script>
</body>

</html>
<?php
if (!isset($_SESSION['id'])) {
require_once ('includes/modal_login.php');
}
?>