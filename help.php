<?php
require_once ('heliocms/core.php');
?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Ayuda - <?php echo $sitename; ?></title>
    <!--[if (lte IE 9)|(IEMobile)]><script>window.location = '<?php echo $site; ?>/br/upgrade/';</script>
        <!--<![endif]-->
    <meta name="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Ajuda">
    <meta name="twitter:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="Ajuda">
    <meta itemprop="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $og; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="//pagead2.googlesyndication.com/pagead/expansion_embed.js?source=safeframe"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20160119135516/recaptcha__pt_br.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ecommerce.js"></script>
    <script async="" type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="d1a83d6" data-releasestage="hhbr"></script>
    <script src="https://partner.googleadservices.com/gpt/pubads_impl_79.js" async=""></script>
    <link rel="stylesheet" href="https://d3hmp0045zy3cs.cloudfront.net/2.2.21/providers.css" type="text/css">
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/osd.js"></script>
</head>


<body class="" client-disable-scrollbars="">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view style="" class="">
            <div active="playing<?php echo $sitename; ?>" class="header header--small">
                <div class="header__background">
                    <!-- ngIf: hasAd -->
                    <div class="header__hotel"></div>
                    <header class="header__wrapper wrapper"><a href="/" class="header__habbo__logo"><h1 class="header__habbo__name" id="ga-linkid-habbo"><?php echo $sitename; ?></h1></a>
<?php if (isset($_SESSION['id'])) { ?>
				<!-- requireSession:  -->
                <div require-session="" class="header__aside header__aside--user-menu">
                    <div false-on-outside-click="toggle" class="user-menu">
                        <div class="user-menu__header">
                            <a id="ul-click" ng-click="click()">
                                <div class="user-menu__avatar__wrapper"><img width="54" height="62" class="user-menu__avatar imager" figure="<?php echo $user_q['look']; ?>" size="bighead" alt="<?php echo $user_q['username']; ?>" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&headonly=1&size=b&gesture=sml&direction=2&head_direction=2&action=std"></div>
                                <div class="user-menu__name__wrapper">
                                    <div id="ul-toggle" class="user-menu__name" ng-class="{ 'user-menu__name--open': toggle }">
                                        <div class="user-menu__name__container"><?php echo $user_q['username']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <ul ng-hide="!toggle" id="ul-hide" style="display: none" class="user-menu__list">
                            <li class="user-menu__item"><a ng-href="/profile/<?php echo $user_q['username']; ?>" ng-class="{ 'user-menu__link--active': isMyProfileActive() }" class="user-menu__link user-menu__link--profile" translate="NAVIGATION_PROFILE" href="/profile/<?php echo $user_q['username']; ?>">Ver mi perfil público</a></li>
                            <li class="user-menu__item"><a href="/settings" ng-class="{ 'user-menu__link--active': isSettingsActive() }" class="user-menu__link user-menu__link--settings" translate="NAVIGATION_SETTINGS">Ajustes</a></li>
                            <li class="user-menu__item"><a ng-href="/api/public/help?returnTo=https://help.habbo.es" class="user-menu__link user-menu__link--help" target="_blank" translate="NAVIGATION_HELP" href="/api/public/help?returnTo=https://help.habbo.es">Ayuda</a></li>
                            <li class="user-menu__item"><a onclick="logout()" ng-click="logout()" class="user-menu__link user-menu__link--logout" translate="NAVIGATION_LOGOUT">Salir</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end ngIf: function (){return t.hasSession()} -->
				<?php }else{ ?>
                        <!-- requireNoSession:  -->
                        <div require-no-session="" class="header__aside">
                            <button ng-click="" data-toggle="modal" data-target="#login" class="header__login__button"><span class="header__login__icon" translate="LOGIN">Acceder</span></button>
                        </div>
                        <!-- end ngIf: function (){
"use strict";
return!t.hasSession()} -->
				<?php } ?>
                    </header>
                    <nav active="playing<?php echo $sitename; ?>" class="navigation">
                        <ul class="navigation__menu">
                            <li class="navigation__item"><a href="/" ng-class="{ 'navigation__link--active': isActive('home') }" class="navigation__link navigation__link--home" translate="NAVIGATION_HOME" id="ga-linkid-home">Inicio</a></li>
                            <li class="navigation__item"><a href="/community" ng-class="{ 'navigation__link--active': isActive('community') }" class="navigation__link navigation__link--community" translate="NAVIGATION_COMMUNITY" id="ga-linkid-community">Comunidad</a></li>
                            <li class="navigation__item"><a href="/playing-habbo" ng-class="{ 'navigation__link--active': isActive('playing<?php echo $sitename; ?>') }" class="navigation__link navigation__link--playing-habbo navigation__link--active" translate="NAVIGATION_PLAYING_HABBO" id="ga-linkid-playing-habbo">Descubre <?php echo $sitename; ?></a></li>
                        												<?php if (isset($_SESSION['id'])) { ?>
					<li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel"><!-- requireFlash:  --><a require-flash="" href="/hotel" class="hotel-button" id="ga-linkid-hotel"><span class="hotel-button__text" translate="NAVIGATION_HOTEL">Hotel</span></a><!-- end ngIf: function (){return a.isEnabled()||!t.test(e.navigator.userAgent)} --></li>
                    <?php } ?>
                        </ul>
                    </nav>
                    <div class="wrapper" ng-transclude=""></div>
                </div>
            </div>
            <section>
                <nav ng-hide="tabs.length < 2" false-on-outside-click="isOpen" class="tabs">
                    <div class="tabs__wrapper">
                        <!-- ngIf: titleKey -->
                        <div id="tab-click" ng-click="isOpen = !isOpen" class="tabs__toggle">
                            <div id="tab-arrow" ng-class="{'tabs__toggle__title--active': isOpen}" class="tabs__toggle__title" translate="PLAYING_HABBO_HELP_TAB">Ayuda</div>
                        </div>
                        <ul id="tab-hide" class="tabs__menu ng-hide" ng-hide="!isOpen" ng-transclude="">
                            <li path="/playing-habbo/what-is-habbo" translation-key="PLAYING_HABBO_WHAT_IS_HABBO_TAB" class="tabs__item"><a style="" href="/playing-habbo/what-is-habbo" ng-href="/playing-habbo/what-is-habbo" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_WHAT_IS_HABBO_TAB">¿Qué es <?php echo $sitename; ?>?</a></li>
                            <li path="/playing-habbo/how-to-play" translation-key="PLAYING_HABBO_HOW_TO_PLAY_TAB" class="tabs__item"><a href="/playing-habbo/how-to-play" ng-href="/playing-habbo/how-to-play" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HOW_TO_PLAY_TAB">Cómo jugar</a></li>
                            <li path="/playing-habbo/habbo-way" translation-key="PLAYING_HABBO_HABBO_WAY_TAB" class="tabs__item"><a href="/playing-habbo/habbo-way" ng-href="/playing-habbo/habbo-way" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HABBO_WAY_TAB">Manera <?php echo $sitename; ?></a></li>
                            <li path="/playing-habbo/safety" translation-key="PLAYING_HABBO_SAFETY_TAB" class="tabs__item"><a href="/playing-habbo/safety" ng-href="/playing-habbo/safety" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_SAFETY_TAB">Seguridad</a></li>
                            <li path="/playing-habbo/help" translation-key="PLAYING_HABBO_HELP_TAB" class="tabs__item"><a style="" href="/playing-habbo/help" ng-href="/playing-habbo/help" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link tabs__link--active" translate="PLAYING_HABBO_HELP_TAB">Ayuda</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- uiView:  -->
                <section style="" class="wrapper wrapper--content" ui-view="">
                    <article class="main main--fixed static-content">
                        <h1>CÓMO TRATAR UN PROBLEMA EN <?php echo $sitename; ?></h1>
                        <p>Quizás te encuentres de forma ocasional con el mal comportamiento de un <?php echo $sitename; ?>. ¡Nada que temer! ¡La ayuda está en camino! En esta página te diremos qué herramientas funcionan mejor en cada situación.</p>
                        <h2>EN UNA SALA</h2>
                        <p>Si estás en una sala y otro <?php echo $sitename; ?> te está molestando, puedes hacer clic en su avatar y aparecerá un menú desplegable. Esto te permitirá ignorar, moderar o, en casos realmente graves, reportarle.</p>
                        <h4>IGNORAR A UN <?php echo $sitename; ?></h4>
                        <p>Si <strong>un <?php echo $sitename; ?> está diciendo cosas que te hacen sentir incómodo</strong>, puedes seleccionar la opción "ignorar!. Esta es una solución ideal para provocaciones, spam o cuando simplemente quieres que te dejen tranquilo y no sabes cómo decirlo.</p>
                        <p><img src="<?php echo $aka; ?>/c_images/Help/report_es.png" alt="Clique no usuário para calar, moderar ou reportar" class="align-right"></p>
                        <ol>
                            <li>Haz clic en su avatar. Se desplegará un menú.</li>
                            <li>Selecciona la opción <em>Ignorar</em>.</li>
                            <li>No verás nunca más lo que ese <?php echo $sitename; ?> dice. Si quieres dejar de ignorarle, haz clic en su avatar de nuevo y elige <em>Escuchar</em>.</li>
                        </ol>
                        <h4>MODERAR A UN <?php echo $sitename; ?></h4>
                        <p><strong>Tanto si estás en una sala tuya, como si es de un amgio pero tienes derechos sobre ella</strong>, puedes decidir quién puede entrar a visitarla. Además, también tienes la posibilidad de mutear, expulsar o bannear a otros usuarios. Esto te permite ser parte activa de la moderación general de <?php echo $sitename; ?> y contribuir con ello a crear una comunidad más segura y divertida.</p>
                        <p>Puedes obtener más información en nuestra sección de Atención al Usuario en los apartados <a href="https://help.habbo.es/hc/es/articles/221676748">Herramientas de Moderación de Sala</a> y <a href="https://help.habbo.es/hc/es/articles/221676788">Ajustes de Sala</a>.</p>
                        <h4>REPORTAR A UN <?php echo $sitename; ?></h4>
                        <p>Si <strong>en tu sala se empiezan a dar las siguientes situaciones</strong>: los <?php echo $sitename; ?>s están hablando de encontrarse en la vida real, quieren una videollamada, algunos están intercambiando información personal de contacto o alguien está sufriendo bullying, puedes considerar reportar a esa persona. A nadie le gustan los soplones, así que recuerda usarlo sólo cuando se está dañando a otros intencionadamente o a sí mismos.</p>
                        <ol>
                            <li>Haz clic en el avatar del <?php echo $sitename; ?> que crea problemas. Se desplegará un menú.</li>
                            <li>Selecciona <em>Reportar</em>.</li>
                            <li>Marca las líneas de chat que los moderadores deberían ver.</li>
                            <li>Elige la mejor descripción de la situación.</li>
							<li>Dile al moderador lo que ha ocurrido.</li>
							<li>Finalmente haz clic en <em>Pedir Ayuda</em>. Si eliges <em>Acosos</em> un Guardián podrá intervenir.</li>
                        </ol>
                        <p><img src="<?php echo $aka; ?>/c_images/Help/help_button_es.png" alt="Botão Ajuda" class="align-right"></p>
                        <p>Otra manera de hacer lo mismo es ésta:</p>
                        <ol>
                            <li>Haz clic en <em>Ayuda</em> en la esquina superior derecha.</li>
                            <li>Selecciona <em>Reportar un acosador</em>. Verás una lista con todos los <?php echo $sitename; ?>s en la sala.</li>
                            <li>Haz clic sobre el <?php echo $sitename; ?> que se está comportando mal.</li>
                            <li>Marca las líneas de chat que los moderadores deberían ver.</li>
							<li>Elige la mejor descripción de la situación.</li>
							<li>Dile al moderador lo que ha ocurrido.</li>
							<li>Manda el reporte y el moderador intentará resolver el asunto.</li>
                        </ol>
                        <hr>
                        <h2>EN CONSOLA/CHAT</h2>
                        <p>Si <strong>estás chateando con alguien en consola y te está haciendo sentir incómodo</strong>:</p>
                        <ol>
                            <li>Haz clic en el botón <em>Denunciar</em> que encontrarás debajo de la imagen del <?php echo $sitename; ?> en la ventana de chat.</li>
                            <li>Te preguntarán más detalles sobre lo ocurrido.</li>
                            <li>Tras valorar lo sucedido, un Moderador tomará la acción adecuada.</li>
                        </ol>
                        <p><img src="<?php echo $aka; ?>/c_images/Help/report_im_es.png" alt="Denunciando um <?php echo $sitename; ?> no Bate-Papo"></p>
                        <h2>EN UN FORO DE GRUPO</h2>
                        <p><img src="<?php echo $aka; ?>/c_images/Help/flag_3.png" alt="bandeira laranja para denunciar tópicos e posts de Fóruns" class="align-right"></p>
                        <p>Puedes <strong>reportar un hilo o post inapropiado de un foro de grupo</strong>:</p>
                        <ol>
                            <li>Haz clic sobre icono con forma de bandera naranja del foro.</li>
                            <li>Te preguntarán más detalles sobre la situación.</li>
                            <li>Indícanos qué hay de malo en la sala o foto</li>
							<li>Un Moderador tomará la acción apropiada</li>
                        </ol>
						<h2>EN UNA PÁGINA WEB</h2>
                        <p><img src="<?php echo $aka; ?>/c_images/Help/reportroom.png" alt="bandeira laranja para denunciar tópicos e posts de Fóruns" class="align-right"></p>
                        <p>Puedes <strong>reportar una imagen inapropiada, sala o imagen de sala en una room homepage o página de fotos</strong>:</p>
                        <ol>
                            <li>Haz clic en el icono de la bandera blanca</li>
                            <li>Elige el asunto para tu alerta que mejor se ajuste al asunto</li>
                            <li>Tras valorar la situación, un moderador tomará las medidas oportunas.</li>
                        </ol>
                        <hr>
                        <h2>CONSEJOS DE SEGURIDAD</h2>
                        <p>En tu página <a href="/playing-habbo/safety">Consejos de Seguridad</a> encontrarás <strong>sugerencias sobre cómo divertirte sin correr riesgos</strong>. ¡Échale un vistazo porque hay un montón de información útil!</p>
                        <h2>MANERA <?php echo $sitename; ?></h2>
                        <p>¿Todavía no has leído <a href="/playing-habbo/habbo-way">La Manera <?php echo $sitename; ?></a>? ¡Por favor, hazlo! Es realmente importante. La Manera <?php echo $sitename; ?> son un <strong>conjunto de reglas simples</strong> que debes seguir para hacer del hotel un lugar más divertido.</p>
                        <h2>CÓMO JUGAR</h2>
                        <p>¿Buscando <strong>ideas sobre qué hacer en <?php echo $sitename; ?></strong>? ¡Lee nuestros <a href="/playing-habbo/how-to-play">consejos sobre cómo jugar</a>!</p>
                        <p>Si necesitas <strong>instrucciones sobre como usar furnis, efectos u otra herramienta</strong> en el Hotel, haz clic en el botón <em>Ayuda</em> en la esquina superior derecha y después selecciona la opción <em>Pedir instrucciones</em>. Un Ayudante irá en camino.</p>
                        <h2><?php echo $sitename; ?> AYUDA AL USUARIO</h2>
                        <p>Si tienes un <strong>problema con tu cuenta <?php echo $sitename; ?>, hubo un error con los créditos que compraste o tienes preguntas sobre una cuenta baneada</strong>, echa un ojo a nuestra página de <a href="https://help.habbo.es/hc/es">Atención al Usuario</a>.</p>
                    </article>
                    <article key="playing_habbo/box_helplines" class="aside aside--box aside--fixed aside--push-down static-content" ng-show="show">
                        <h3>LÍNEAS DE AYUDA PARA JÓVENES</h3>
                        <p>Si te sientes mal, estás siendo acosado o molestado, o simplemente quieres hablar con alguien sobre cuestiones externas a <?php echo $sitename; ?> puedes obtener asesoramiento y ayuda en los teléfonos que indicamos a continuación.</p>
                        <p>Para asistencia dentro de <?php echo $sitename; ?>, baneos, o cualquier otro asunto relacionado con <?php echo $sitename; ?>, puedes contactar en el <a href="https://help.habbo.es/">Ayudante de <?php echo $sitename; ?></a>.</p>
                        <h4>ESPAÑA</h4>
                        <p><strong>Teléfono de Atención al menor - Fundación ANAR</strong></p>
                        <ul>
							<li>Teléfono: 900 20 20 10</li>
                            <li><a href="www.anar.org/telefono-de-ayuda-a-ninos-y-adolescentes" target="_blank">www.anar.org/telefono-de-ayuda-a-ninos-y-adolescentes</a></li>
                        </ul>
						<p><strong>Padres 2.0</strong> - Seguridad en Internet</p>
                        <ul>
							<li>Línea de Ayuda 'La niña de la Tablet': <a href="http://padres20.org/la-nina-de-la-tablet/" target="_blank">http://padres20.org/la-nina-de-la-tablet/</a></li>
						</ul>
                        <h4>MÉXICO</h4>
                        <p><strong>Teléfono de Atención al Menor</strong></p>
                        <ul>
                            <li>México D.F.: 5604 - 2458</li>
                            <li>Tijuana B.C.: 01 (664) 684 - 2318</li>
                        </ul>
						<h4>CHILE</h4>
						<p><strong>Atención gratuita</strong></p>
						<ul>
							<li>Teléfono: 800 730</li>
						</ul>
						<h4>COLOMBIA</h4>
						<p><strong>Fundación ANAR</strong></p>
						<ul>
							<li><a href="www.anar.org/anar-colombia-2">www.anar.org/anar-colombia-2</a></li>
						</ul>
						<p><strong>Te Protejo</strong></p>
						<ul>
							<li><a href="www.teprotejo.org">www.teprotejo.org</a></li>
						</ul>
						<h4>COSTA RICA</h4>
						<ul>
							<li>Línea gratuita: 1147</li>
						</ul>
                    </article>
                </section>
            </section>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root"></div>
    <script src="//connect.facebook.net/pt_BR/sdk.js" async="" id="facebook-jssdk"></script>
</body>

</html>
<?php
if (!isset($_SESSION['id'])) {
require_once ('includes/modal_login.php');
}
?>