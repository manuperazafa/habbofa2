<?php
require_once ('heliocms/core.php');
require_once ('heliocms/session.php');
?>
<?php if ($hotel_q['status_client'] == 'on') { ?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Hotel - <?php echo $sitename; ?></title>
    <meta name="description" content="<?php echo $sitename; ?> Hotel">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="<?php echo $sitename; ?> Hotel">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Hotel">
    <meta name="twitter:description" content="<?php echo $sitename; ?> Hotel">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="Hotel">
    <meta itemprop="description" content="<?php echo $sitename; ?> Hotel">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $og; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
</head>

<body client-disable-scrollbars="" class="" style="overflow: hidden;">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view class="">
            <div class="hotel"></div>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <!-- requireSession:  -->
    <div require-session="" class="">
        <div ngsf-fullscreen="" client-close-fullscreen-on-hide="" ng-class="{ 'client--visible': visible }" class="client client--visible" style="">
            <div class="client__buttons">
                <button onclick="c()" ng-click="close()" class="client__close"><span class="client__close__text" translate="CLIENT_TO_WEB_BUTTON">WEB</span></button>
                <button onclick="fullscreen()" ngsf-toggle-fullscreen="" class="client__fullscreen"><i show-if-fullscreen="false" class="client__fullscreen__icon icon--fullscreen"></i> <i show-if-fullscreen="" class="client__fullscreen__icon icon--fullscreen-back ng-hide"></i></button>
            </div>
            <!-- ngIf: isOpen &&flashEnabled -->
            <iframe id="hotel-client" client-communication="" ng-if="isOpen &amp;&amp;flashEnabled" class="client__frame" ng-src="<?php echo $site; ?>/client/hhbr-<?php echo $w; ?>" src="<?php echo $site; ?>/client/hhbr-<?php echo $w; ?>"></iframe>
            <!-- end ngIf: isOpen &&flashEnabled -->
            <!-- ngIf: isOpen && flashEnabled && !running -->
            <!-- ngIf: isOpen && !flashEnabled -->
            <!-- ngIf: !isOpen -->
        </div>
    </div>
    <!-- end ngIf: function (){return t.hasSession()} -->
    <!-- requireNoSession:  -->
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root" class=" fb_reset">
        <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
            <div>
                <iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="Facebook Cross Domain Communication Frame" aria-hidden="true" tabindex="-1" id="fb_xdm_frame_https" src="https://staticxx.facebook.com/connect/xd_arbiter.php?version=42#channel=f1a5b50708&amp;origin=https%3A%2F%2Fwww.habbo.com.br" style="border: none;"></iframe>
            </div>
        </div>
        <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
            <div></div>
        </div>
    </div>
    <script id="facebook-jssdk" async="" src="//connect.facebook.net/pt_BR/sdk.js"></script>
</body>

</html>
<?php } ?>
<?php if ($hotel_q['status_client'] == 'off') { ?>
<html>
	<head>
	  <title><?php echo $sitename; ?> fuera de servicio.</title>
	  <style>
		body {
			background: #000;
			color: #fff;
			font-family: sans-serif;
		}
		
		.container {
			width: 70%;
			margin-left: auto;
			margin-right: auto;
		}
		
		.centered {
			text-align: center;
		}
		
		.btn {
			text-transform: uppercase;
			color: #fff;
			text-decoration: none;
			font-weight: bold;
			background: #3b843e;
			padding: 9px;
			border-radius: 3px;
			border: 3px solid #4CAF50;
		}
		
		.btn:hover {
			background: #4CAF50;
		}
	  </style>
	</head>
	
	<body>
	<br><br><br><br>
	<div class="container">
		<div class="centered">
			<h1>¡EL HOTEL ESTÁ APAGADO!</h1>
			<?php echo $sitename; ?> Hotel está actualmente apagado, por favor vuelve de aquí a 15 minutos.
			<br>
			<br>
			<br>
			<a href="<?php echo $site; ?>" class="btn">VOLVER A <?php echo $sitename; ?></a>
		</div>
	</div>
	</body>
</html>
<?php } ?>