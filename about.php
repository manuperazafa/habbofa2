<?php 
require_once ('heliocms/core.php');
include 'web/templates/header.php'; ?>
	  <div class="container">
		  <div class="row">
			<!-- Top de economía -->
			<div class="col s12 m4" style="margin-top: 5px;">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Duckeros</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 0px 20px;">
				  <table class="centered">
					<tbody>
					 <?php $staff_a = mysql_query("SELECT * FROM users ORDER BY activity_points DESC LIMIT 10"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
					  <tr>
						<td style="padding: 10px 0px 3px 0px;"><img src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $staff_q['look']; ?>&gesture=sml&size=s&headonly=1"></td>
						<td style="padding: 10px 0px 3px 0px;"><?php print $staff_q['username']; ?></td>
						<td style="padding: 10px 0px 3px 0px;"><?php print $staff_q['activity_points']; ?> <img src="./web/images/icon_duckets.png"></td>
					  </tr>
					 <?php } ?>
					</tbody>
				  </table>
				</div>
			  </div>
			</div>
			<div class="col s12 m4">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Créditeros</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 0px 20px;">
				  <table class="centered">
					<tbody>
					 <?php $staff_a = mysql_query("SELECT * FROM users ORDER BY credits DESC LIMIT 10"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
					  <tr>
						<td style="padding: 10px 0px 3px 0px;"><img src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $staff_q['look']; ?>&gesture=sml&size=s&headonly=1"></td>
						<td style="padding: 10px 0px 3px 0px;"><?php print $staff_q['username']; ?></td>
						<td style="padding: 10px 0px 3px 0px;"><?php print $staff_q['credits']; ?> <img src="./web/images/icon_credits.png"></td>
					  </tr>
					 <?php } ?>
					</tbody>
				  </table>
				</div>
			  </div>
			</div>
			<div class="col s12 m4">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Diamanteros</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 0px 20px;">
				  <table class="centered">
					<tbody>
					 <?php $staff_a = mysql_query("SELECT * FROM users ORDER BY vip_points DESC LIMIT 10"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
					  <tr>
						<td style="padding: 10px 0px 3px 0px;"><img src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php print $staff_q['look']; ?>&gesture=sml&size=s&headonly=1"></td>
						<td style="padding: 10px 0px 3px 0px;"><?php print $staff_q['username']; ?></td>
						<td style="padding: 10px 0px 3px 0px;"><?php print $staff_q['vip_points']; ?> <img src="./web/images/icon_diamonds.png"></td>
					  </tr>
					 <?php } ?>
					</tbody>
				  </table>
				</div>
			  </div>
			</div>
		</div>
		<div class="row">
			<!-- Últimas noticias -->
			<div class="col s12 m6">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Últimos usuarios registrados</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <table class="centered">
					<tbody>
					 
					  <tr><?php $staff_a = mysql_query("SELECT * FROM users ORDER BY id DESC LIMIT 9"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
						<td title="<?php echo $staff_q['username']; ?> registrado el <?php echo date("d M Y", $staff_q['account_created']); ?>" style="padding: 10px 0px 3px 0px;"><img src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php print $staff_q['look']; ?>&gesture=sml&size=s&headonly=1"></td>
					  <?php } ?></tr>
					 
					</tbody>
				  </table>	
				</div>
			  </div>
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Últimos usuarios desconectados</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <table class="centered">
					<tbody>
					 
					  <tr><?php $staff_a = mysql_query("SELECT * FROM users ORDER BY last_online DESC LIMIT 9"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
						<td title="<?php echo $staff_q['username']; ?> se desconectó el <?php print date("d M Y H:i", $staff_q['last_online']); ?>" style="padding: 10px 0px 3px 0px;"><img src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php print $staff_q['look']; ?>&gesture=sml&size=s&headonly=1"></td>
					  <?php } ?></tr>
					 
					</tbody>
				  </table>	
				</div>
			  </div>
			</div>
			<div class="col s12 m6">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Facebook oficial</div>
			  </div>
				<div class="card-content black-text">
                        <div id="fb-root" style="width: 100%;"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.5&appId=308059415886911";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="fb-page" style="width: 100%;" data-href="https://www.facebook.com/<?php echo $FBpage; ?>" data-width="389" data-height="310" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/<?php echo $FBpage; ?>"><a href="https://www.facebook.com/<?php echo $FBpage; ?>"><?php print Settings('Name'); ?></a></blockquote></div></div>
				</div>
			  </div>
			</div>
		  </div>
	  </div>
	  <?php include '$site/web/templates/footer.php'; ?>
    </body>
  </html>