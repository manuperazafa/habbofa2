<?php
require_once ('heliocms/core.php');
require_once ('heliocms/session.php');
if ($_GET['save'] == "$w") {
$message = '<div id="toast_container_s" class="toast-top-center" aria-live="polite" role="alert"><div class="toast toast-success" style="display: table;"><div class="toast-message">¡Guardado!</div></div></div>';
}
$profilevisible_a = mysql_query("SELECT * FROM heliocms_profilesettings WHERE email='$user_q[mail]'");
$profilevisible_q = mysql_fetch_assoc($profilevisible_a);
if (isset($_POST['save'])) {
$profilevisible = $_POST['profileVisible'];
$friendsrequests = $_POST['friendRequestEnabled'];
$online = $_POST['onlineStatusVisible'];
$follow = $_POST['friendCanFollow'];
if ($friendsrequests == null) {
$friendsrequests = '1';
}
if ($follow == null) {
$follow = '1';
}
mysql_query("UPDATE users SET block_newfriends='$friendsrequests', hide_online='$online', hide_inroom='$follow' WHERE id='$user_q[id]'");
mysql_query("UPDATE heliocms_profilesettings SET profile_visible='$profilevisible' WHERE email='$user_q[mail]'");
header ("Location: $site/settings/privacy?save=$w");
}
?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Ajustes de privacidad - <?php echo $sitename; ?></title>
    <!--[if (lte IE 9)|(IEMobile)]><script>window.location = '<?php echo $site; ?>/br/upgrade/';</script>
        <!--<![endif]-->
    <meta name="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Configurações de Privacidade">
    <meta name="twitter:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="Configurações de Privacidade">
    <meta itemprop="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $ob; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="//pagead2.googlesyndication.com/pagead/expansion_embed.js?source=safeframe"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20160119135516/recaptcha__pt_br.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ecommerce.js"></script>
    <script async="" type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="d1a83d6" data-releasestage="hhbr"></script>
    <script src="https://partner.googleadservices.com/gpt/pubads_impl_79.js" async=""></script>
    <link rel="stylesheet" href="https://d3hmp0045zy3cs.cloudfront.net/2.2.21/providers.css" type="text/css">
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/osd.js"></script>
</head>
<?php echo $message; ?>
<body client-disable-scrollbars="" class="">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view>
            <div class="header header--small" active="settings">
                <div class="header__background">
                    <div class="header__hotel"></div>
                    <header class="header__wrapper wrapper"><a href="/" class="header__habbo__logo"><h1 class="header__habbo__name" id="ga-linkid-habbo"><?php echo $sitename; ?></h1></a>
                        <!-- requireSession:  -->
                <div require-session="" class="header__aside header__aside--user-menu">
                    <div false-on-outside-click="toggle" class="user-menu">
                        <div class="user-menu__header">
                            <a id="ul-click" ng-click="click()">
                                <div class="user-menu__avatar__wrapper"><img width="54" height="62" class="user-menu__avatar imager" figure="<?php echo $user_q['look']; ?>" size="bighead" alt="<?php echo $user_q['username']; ?>" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&headonly=1&size=b&gesture=sml&direction=2&head_direction=2&action=std"></div>
                                <div class="user-menu__name__wrapper">
                                    <div id="ul-toggle" class="user-menu__name" ng-class="{ 'user-menu__name--open': toggle }">
                                        <div class="user-menu__name__container"><?php echo $user_q['username']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <ul ng-hide="!toggle" id="ul-hide" style="display: none" class="user-menu__list">
                            <li class="user-menu__item"><a ng-href="/profile/<?php echo $user_q['username']; ?>" ng-class="{ 'user-menu__link--active': isMyProfileActive() }" class="user-menu__link user-menu__link--profile" translate="NAVIGATION_PROFILE" href="/profile/<?php echo $user_q['username']; ?>">Ver mi perfil público</a></li>
                            <li class="user-menu__item"><a href="/settings" ng-class="{ 'user-menu__link--active': isSettingsActive() }" class="user-menu__link user-menu__link--settings user-menu__link--active" translate="NAVIGATION_SETTINGS">Ajustes</a></li>
                            <li class="user-menu__item"><a ng-href="/api/public/help?returnTo=https://help.habbo.es" class="user-menu__link user-menu__link--help" target="_blank" translate="NAVIGATION_HELP" href="/api/public/help?returnTo=https://help.habbo.es">Ayuda</a></li>
                            <li class="user-menu__item"><a onclick="logout()" ng-click="logout()" class="user-menu__link user-menu__link--logout" translate="NAVIGATION_LOGOUT">Salir</a></li>
                        </ul>
                    </div>
                </div>
                        </div>
                        <!-- end ngIf: function (){return e.hasSession()} -->
                        <!-- requireNoSession:  -->
                    </header>
                    <nav class="navigation" active="settings">
                        <ul class="navigation__menu">
                            <li class="navigation__item"><a href="/" ng-class="{ 'navigation__link--active': isActive('home') }" class="navigation__link navigation__link--home" translate="NAVIGATION_HOME" id="ga-linkid-home">Inicio</a></li>
                            <li class="navigation__item"><a href="/community" ng-class="{ 'navigation__link--active': isActive('community') }" class="navigation__link navigation__link--community" translate="NAVIGATION_COMMUNITY" id="ga-linkid-community">Comunidad</a></li>
                            <li class="navigation__item"><a href="/playing-habbo" ng-class="{ 'navigation__link--active': isActive('playing<?php echo $sitename; ?>') }" class="navigation__link navigation__link--playing-habbo" translate="NAVIGATION_PLAYING_HABBO" id="ga-linkid-playing-habbo">Descubre <?php echo $sitename; ?></a></li>
                            <!-- requireSession:  -->
                            <li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel">
                                <!-- requireFlash:  --><a require-flash="" href="/hotel" class="hotel-button" id="ga-linkid-hotel"><span class="hotel-button__text" translate="NAVIGATION_HOTEL">Hotel</span></a>
                                <!-- end ngIf: function (){return a.isEnabled()||!t.test(e.navigator.userAgent)} -->
                            </li>
                            <!-- end ngIf: function (){return e.hasSession()} -->
                        </ul>
                    </nav>
                    <div class="wrapper" ng-transclude=""></div>
                </div>
            </div>
            <nav ng-hide="tabs.length < 2" false-on-outside-click="isOpen" class="tabs" title-key="SETTINGS_TITLE">
                <div class="tabs__wrapper">
                    <!-- ngIf: titleKey -->
                    <h1 ng-if="titleKey" class="tabs__title" translate="SETTINGS_TITLE">Ajustes</h1>
                    <!-- end ngIf: titleKey -->
                    <div id="tab-click" ng-click="isOpen = !isOpen" class="tabs__toggle">
                        <div id="tab-arrow" ng-class="{'tabs__toggle__title--active': isOpen}" class="tabs__toggle__title" translate="SETTINGS_PRIVACY_TAB">Privacidad</div>
                    </div>
                    <ul id="tab-hide" class="tabs__menu ng-hide" ng-hide="!isOpen" ng-transclude="">
                        <li class="tabs__item" path="/settings/privacy" translation-key="SETTINGS_PRIVACY_TAB"><a ng-href="/settings/privacy" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link tabs__link--active" translate="SETTINGS_PRIVACY_TAB" href="/settings/privacy">Privacidad</a></li>
                        <!-- require<?php echo $sitename; ?>AccountSession:  -->
                        <li class="tabs__item" require-habbo-account-session="" path="/settings/security" translation-key="SETTINGS_ACCOUNT_SECURITY_TAB"><a ng-href="/settings/security" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_ACCOUNT_SECURITY_TAB" <?php if ($account_blocked_q['active'] == '1') { ?>data-toggle="modal" data-target="#account_blocked"<?php }else{ ?>href="/settings/security"<?php } ?>>Protección de la cuenta</a></li>
                        <!-- end ngIf: function (){return e.is<?php echo $sitename; ?>AccountSession()} -->
                        <!-- require<?php echo $sitename; ?>AccountSession:  -->
                        <li class="tabs__item" require-habbo-account-session="" path="/settings/password" translation-key="SETTINGS_PASSWORD_TAB"><a ng-href="/settings/password" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_PASSWORD_TAB" <?php if ($account_blocked_q['active'] == '1') { ?>data-toggle="modal" data-target="#account_blocked"<?php }else{ ?>href="/settings/password"<?php } ?>>Contraseña</a></li>
                        <!-- end ngIf: function (){return e.is<?php echo $sitename; ?>AccountSession()} -->
                        <!-- require<?php echo $sitename; ?>AccountSession:  -->
                        <li class="tabs__item" require-habbo-account-session="" path="/settings/email" translation-key="SETTINGS_EMAIL_TAB"><a ng-href="/settings/email" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_EMAIL_TAB" <?php if ($account_blocked_q['active'] == '1') { ?>data-toggle="modal" data-target="#account_blocked"<?php }else{ ?>href="/settings/email"<?php } ?>>Email</a></li>
                        <!-- end ngIf: function (){return e.is<?php echo $sitename; ?>AccountSession()} -->
                        <li class="tabs__item" path="/settings/avatars" translation-key="SETTINGS_AVATAR_TAB"><a ng-href="/settings/avatars" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_AVATAR_TAB" href="/settings/avatars">Avatars</a></li>
                    </ul>
                </div>
            </nav>
            <section class="wrapper wrapper--content">
                <div class="row">
                    <!-- uiView:  -->
                    <div class="main" ui-view="">
                        <h2 translate="PRIVACY_SETTINGS_TITLE">Ajustes de privacidad</h2>
                        <form method="post" privacy-settings="privacySettings" ng-submit="save()" name="privacySettingsForm" novalidate="" class="form form--left ng-pristine ng-valid">
                            <div>
                                <fieldset class="form__fieldset">
                                    <h4 translate="SETTINGS_PROFILE_VISIBILITY_TITLE">VISIBILIDAD DE PERFIL</h4>
                                    <p translate="SETTINGS_PROFILE_VISIBILITY_DESCRIPTION">Quién puede ver tu perfil</p>
                                    <div class="form__field">
                                        <label class="form__label form__label--radiobutton">
                                            <input value="1" <?php if ($profilevisible_q['profile_visible'] == '1') { ?>checked<?php } ?> name="profileVisible" ng-model="privacySettings.profileVisible" ng-value="true" class="form__radiobutton ng-pristine ng-untouched ng-valid" type="radio"> <span translate="SETTINGS_EVERYONE_LABEL">Todos</span></label>
                                    </div>
                                    <div class="form__field">
                                        <label class="form__label form__label--radiobutton">
                                            <input value="0" <?php if ($profilevisible_q['profile_visible'] == '0') { ?>checked<?php } ?> name="profileVisible" ng-model="privacySettings.profileVisible" ng-value="false" class="form__radiobutton ng-pristine ng-untouched ng-valid" type="radio"> <span translate="SETTINGS_ME_LABEL">Nadie</span></label>
                                    </div>
                                </fieldset>
                            </div>
                            <div>
                                <fieldset class="form__fieldset form__fieldset--box">
                                    <h4 translate="SETTINGS_ONLINE_STATUS_TITLE">ESTADO ONLINE</h4>
                                    <p translate="SETTINGS_ONLINE_STATUS_DESCRIPTION">Quién puede ver tu estado online:</p>
                                    <div class="form__field">
                                        <label class="form__label form__label--radiobutton">
                                            <input value="0" <?php if ($user_q['hide_online'] == '0') { ?>checked="checked"<?php } ?> value="true" name="onlineStatusVisible" ng-model="privacySettings.onlineStatusVisible" ng-value="true" class="form__radiobutton ng-pristine ng-untouched ng-valid" type="radio"> <span translate="SETTINGS_EVERYONE_LABEL">Todos</span></label>
                                    </div>
                                    <div class="form__field">
                                        <label class="form__label form__label--radiobutton">
                                            <input value="1" <?php if ($user_q['hide_online'] == '1') { ?>checked="checked"<?php } ?> value="false" name="onlineStatusVisible" ng-model="privacySettings.onlineStatusVisible" ng-value="false" class="form__radiobutton ng-pristine ng-untouched ng-valid" type="radio"> <span translate="SETTINGS_ME_LABEL">Nadie</span></label>
                                    </div>
                                </fieldset>
                            </div>
                            <div>
                                <fieldset class="form__fieldset">
                                    <h4 translate="SETTINGS_FRIENDS_CAN_FOLLOW_TITLE">AJUSTES DE SEGUIMIENTO</h4>
                                    <div class="form__field">
                                        <label for="friend-can-follow" class="form__label form__label--checkbox">
                                            <input value="0" <?php if ($user_q['hide_inroom'] == '0') { ?>checked="checked"<?php } ?> id="friend-can-follow" name="friendCanFollow" ng-model="privacySettings.friendCanFollow" class="form__checkbox ng-pristine ng-untouched ng-valid" type="checkbox"> <span translate="SETTINGS_ENABLE_FRIEND_CAN_FOLLOW_LABEL">Mis amigos me pueden seguir de una sala a otra</span></label>
                                    </div>
                                </fieldset>
                            </div>
                            <div>
                                <fieldset class="form__fieldset form__fieldset--box">
                                    <h4 translate="SETTINGS_FRIEND_REQUESTS_ENABLED_TITLE">PETICIONES DE AMISTAD</h4>
                                    <div class="form__field">
                                        <label for="friend-request-enable" class="form__label form__label--checkbox">
                                            <input value="0" <?php if ($user_q['block_newfriends'] == '0') { ?>checked="checked"<?php } ?> id="friend-request-enable" name="friendRequestEnabled" ng-model="privacySettings.friendRequestEnabled" class="form__checkbox ng-pristine ng-untouched ng-valid" type="checkbox"> <span translate="SETTINGS_ENABLE_FRIEND_REQUESTS_LABEL">Permitir a otros <?php echo $sitename; ?>s enviarme peticiones de amistad</span></label>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form__footer">
                                <button name="save" type="submit" ng-disabled="sendInProgress" translate="FORM_BUTTON_SAVE" class="form__submit">Salvar</button>
                            </div>
                        </form>
                    </div>
                    <article style="" key="common/box_learn_how_to_stay_safe" class="aside aside--box aside--push-down static-content" ng-show="show">
                        <h3>CONSEJOS DE SEGURIDAD</h3>
                        <p>¡Protégete con conciencia! Aprende cómo <a href="/playing-habbo/safety">Navegar seguro por Internet</a>.</p>
                    </article>
                    <article style="" key="common/box_need_help" class="aside aside--box static-content" ng-show="show">
                        <h3>¿NECESITAS AYUDA?</h3>
                        <p>Consulta cómo puedes echarte un cable o pedir la ayuda de un moderador en nuestro apartado de <a href="/playing-habbo/help">Atención al Usuario</a>. También incluye una lista de números de teléfono y páginas web en caso de que necesites hablar con alguien. Si no encuentras una respuesta adecuada en esta página, ponte en contacto con el <a href="https://help.habbo.es">Ayudante de <?php echo $sitename; ?></a>.</p>
                    </article>
                </div>
            </section>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <!-- requireNoSession:  -->
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="20e33e2" data-releasestage="hhbr"></script>
    <script>
        ! function() {
            var h = Date.now();
            window.Bugsnag && (window.Bugsnag.notifyReleaseStages = ["hhde", "hhus", "hhes", "hhfi", "hhfr", "hhit", "hhnl", "hhbr", "hhtr"], window.Bugsnag.beforeNotify = function(e) {
                return e.metaData.secondsSincePageLoad = (Date.now() - h) / 1e3, /habbo\./.test(e.url)
            })
        }();
    </script>
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js" defer="defer"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root" class=" fb_reset">
        <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
            <div><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="Facebook Cross Domain Communication Frame" aria-hidden="true" tabindex="-1" id="fb_xdm_frame_https" src="https://staticxx.facebook.com/connect/xd_arbiter.php?version=42#channel=f2b2646024&amp;origin=https%3A%2F%2Fwww.habbo.com.br" style="border: none;"></iframe></div>
        </div>
        <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
            <div><iframe name="f290f75a64" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" src="https://www.facebook.com/connect/ping?client_id=672834216073545&amp;domain=www.habbo.com.br&amp;origin=1&amp;redirect_uri=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df15c090cf8%26domain%3Dwww.habbo.com.br%26origin%3Dhttps%253A%252F%252Fwww.habbo.com.br%252Ff2b2646024%26relation%3Dparent&amp;response_type=token%2Csigned_request%2Ccode&amp;sdk=joey" style="display: none;"></iframe></div>
        </div>
    </div>
    <script id="facebook-jssdk" async="" src="//connect.facebook.net/pt_BR/sdk.js"></script><iframe src="https://tpc.googlesyndication.com/safeframe/1-0-2/html/container.html" style="visibility: hidden; display: none;"></iframe><iframe id="google_osd_static_frame_6747793962713" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;"></iframe></body>

</html>
<?php
if ($account_blocked_q['active'] == '1') {
require_once ('includes/modal_account_blocked.php');
}
?>