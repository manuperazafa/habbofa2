<?php
require_once ('heliocms/core.php');
?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Seguridad - <?php echo $sitename; ?></title>
    <!--[if (lte IE 9)|(IEMobile)]><script>window.location = '<?php echo $site; ?>/br/upgrade/';</script>
        <!--<![endif]-->
    <meta name="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Segurança">
    <meta name="twitter:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="Segurança">
    <meta itemprop="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $ob; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="//pagead2.googlesyndication.com/pagead/expansion_embed.js?source=safeframe"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20160119135516/recaptcha__pt_br.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ecommerce.js"></script>
    <script async="" type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="d1a83d6" data-releasestage="hhbr"></script>
    <script src="https://partner.googleadservices.com/gpt/pubads_impl_79.js" async=""></script>
    <link rel="stylesheet" href="https://d3hmp0045zy3cs.cloudfront.net/2.2.21/providers.css" type="text/css">
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/osd.js"></script>
</head>

<body class="" client-disable-scrollbars="">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view style="" class="">
            <div active="playing<?php echo $sitename; ?>" class="header header--small">
                <div class="header__background">
                    <div class="header__hotel"></div>
                    <header class="header__wrapper wrapper"><a href="/" class="header__habbo__logo"><h1 class="header__habbo__name" id="ga-linkid-habbo"><?php echo $sitename; ?></h1></a>
<?php if (isset($_SESSION['id'])) { ?>
				<!-- requireSession:  -->
                <div require-session="" class="header__aside header__aside--user-menu">
                    <div false-on-outside-click="toggle" class="user-menu">
                        <div class="user-menu__header">
                            <a id="ul-click" ng-click="click()">
                                <div class="user-menu__avatar__wrapper"><img width="54" height="62" class="user-menu__avatar imager" figure="<?php echo $user_q['look']; ?>" size="bighead" alt="<?php echo $user_q['username']; ?>" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&headonly=1&size=b&gesture=sml&direction=2&head_direction=2&action=std"></div>
                                <div class="user-menu__name__wrapper">
                                    <div id="ul-toggle" class="user-menu__name" ng-class="{ 'user-menu__name--open': toggle }">
                                        <div class="user-menu__name__container"><?php echo $user_q['username']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <ul ng-hide="!toggle" id="ul-hide" style="display: none" class="user-menu__list">
                            <li class="user-menu__item"><a ng-href="/profile/<?php echo $user_q['username']; ?>" ng-class="{ 'user-menu__link--active': isMyProfileActive() }" class="user-menu__link user-menu__link--profile" translate="NAVIGATION_PROFILE" href="/profile/<?php echo $user_q['username']; ?>">Ver mi perfil público</a></li>
                            <li class="user-menu__item"><a href="/settings" ng-class="{ 'user-menu__link--active': isSettingsActive() }" class="user-menu__link user-menu__link--settings" translate="NAVIGATION_SETTINGS">Ajustes</a></li>
                            <li class="user-menu__item"><a ng-href="/api/public/help?returnTo=https://help.habbo.es" class="user-menu__link user-menu__link--help" target="_blank" translate="NAVIGATION_HELP" href="/api/public/help?returnTo=https://help.habbo.es">Ayuda</a></li>
                            <li class="user-menu__item"><a onclick="logout()" ng-click="logout()" class="user-menu__link user-menu__link--logout" translate="NAVIGATION_LOGOUT">Salir</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end ngIf: function (){return t.hasSession()} -->
				<?php }else{ ?>
                        <!-- requireNoSession:  -->
                        <div require-no-session="" class="header__aside">
                            <button ng-click="" data-toggle="modal" data-target="#login" class="header__login__button"><span class="header__login__icon" translate="LOGIN">Acceder</span></button>
                        </div>
                        <!-- end ngIf: function (){
"use strict";
return!t.hasSession()} -->
				<?php } ?>
                    </header>
                    <nav active="playing<?php echo $sitename; ?>" class="navigation">
                        <ul class="navigation__menu">
                            <li class="navigation__item"><a href="/" ng-class="{ 'navigation__link--active': isActive('home') }" class="navigation__link navigation__link--home" translate="NAVIGATION_HOME" id="ga-linkid-home">Inicio</a></li>
                            <li class="navigation__item"><a href="/community" ng-class="{ 'navigation__link--active': isActive('community') }" class="navigation__link navigation__link--community" translate="NAVIGATION_COMMUNITY" id="ga-linkid-community">Comunidad</a></li>
                            <li class="navigation__item"><a href="/playing-habbo" ng-class="{ 'navigation__link--active': isActive('playing<?php echo $sitename; ?>') }" class="navigation__link navigation__link--playing-habbo navigation__link--active" translate="NAVIGATION_PLAYING_HABBO" id="ga-linkid-playing-habbo">Descubre <?php echo $sitename; ?></a></li>
                        												<?php if (isset($_SESSION['id'])) { ?>
					<li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel"><!-- requireFlash:  --><a require-flash="" href="/hotel" class="hotel-button" id="ga-linkid-hotel"><span class="hotel-button__text" translate="NAVIGATION_HOTEL">Hotel</span></a><!-- end ngIf: function (){return a.isEnabled()||!t.test(e.navigator.userAgent)} --></li>
                    <?php } ?>
                        </ul>
                    </nav>
                    <div class="wrapper" ng-transclude=""></div>
                </div>
            </div>
            <section>
                <nav ng-hide="tabs.length < 2" false-on-outside-click="isOpen" class="tabs">
                    <div class="tabs__wrapper">
                        <!-- ngIf: titleKey -->
                        <div id="tab-click" ng-click="isOpen = !isOpen" class="tabs__toggle">
                            <div id="tab-arrow" ng-class="{'tabs__toggle__title--active': isOpen}" class="tabs__toggle__title" translate="PLAYING_HABBO_SAFETY_TAB">Seguridad</div>
                        </div>
                        <ul id="tab-hide" class="tabs__menu ng-hide" ng-hide="!isOpen" ng-transclude="">
                            <li path="/playing-habbo/what-is-habbo" translation-key="PLAYING_HABBO_WHAT_IS_HABBO_TAB" class="tabs__item"><a style="" href="/playing-habbo/what-is-habbo" ng-href="/playing-habbo/what-is-habbo" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_WHAT_IS_HABBO_TAB">¿Qué es <?php echo $sitename; ?>?</a></li>
                            <li path="/playing-habbo/how-to-play" translation-key="PLAYING_HABBO_HOW_TO_PLAY_TAB" class="tabs__item"><a style="" href="/playing-habbo/how-to-play" ng-href="/playing-habbo/how-to-play" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HOW_TO_PLAY_TAB">Cómo jugar</a></li>
                            <li path="/playing-habbo/habbo-way" translation-key="PLAYING_HABBO_HABBO_WAY_TAB" class="tabs__item"><a style="" href="/playing-habbo/habbo-way" ng-href="/playing-habbo/habbo-way" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HABBO_WAY_TAB">Manera <?php echo $sitename; ?></a></li>
                            <li path="/playing-habbo/safety" translation-key="PLAYING_HABBO_SAFETY_TAB" class="tabs__item"><a style="" href="/playing-habbo/safety" ng-href="/playing-habbo/safety" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link tabs__link--active" translate="PLAYING_HABBO_SAFETY_TAB">Seguridad</a></li>
                            <li path="/playing-habbo/help" translation-key="PLAYING_HABBO_HELP_TAB" class="tabs__item"><a style="" href="/playing-habbo/help" ng-href="/playing-habbo/help" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="PLAYING_HABBO_HELP_TAB">Ayuda</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- uiView:  -->
                <section style="" class="wrapper wrapper--content" ui-view="">
                    <article class="main main--fixed static-content">
                        <h1>CONSEJOS DE SEGURIDAD</h1>
                        <p>¡Estos son los 7 consejos más importantes sobre cómo navegar seguros por Internet!</p>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips1_n.png" alt="Proteja as suas informações pessoais" class="align-right"></p>
                        <h3>PROTEGE TU INFORMACIÓN PERSONAL</h3>
                        <p>¡Nunca sabes quién puede estar al otro lado de la pantalla, así que nunca compartas tu información personal! Facilitar tus datos personales (nombre, dirección, números de teléfono, fotos o escuela a la que asistes) podría llevarte a ser timado, acosado o expuesto a un peligro serio.</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips2_n2.png" alt="Proteja a sua privacidade" class="align-right"></p>
                        <h3>PROTEGE TU PRIVACIDAD</h3>
                        <p>Nunca compartas ninguno de tus detalles personales. Esto incluye info de Facebook, Twitter, Skype, Instagram y Snapchat. ¡Nunca sabes quién puede poner sus manos en ella!</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips3_n.png" alt="Não ceda à pressão dos amigos" class="align-right"></p>
                        <h3>NO CEDAS A LA PRESIÓN DE LOS DEMÁS</h3>
                        <p>Aunque todos lo hagan, no significa que tú tengas que hacerlo. Si no te sientes cómodo con algo, ¡no lo hagas!!</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips4_n.png" alt="Mantenha os seus amigos de pixels em pixels" class="align-right"></p>
                        <h3>AMISTADES DE PÍXEL</h3>
                        <p>¡No quedes con nadie que sólo conozcas de Internet! Las personas no siempre son quienes dicen ser. Si un <?php echo $sitename; ?> te pide que os encontréis en la vida real dile: "¡No, gracias!", e inmediatamente después díselo a un moderador, a tus padres o a otro adulto de confianza.</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips5_n.png" alt="Não tenha medo de falar" class="align-right"></p>
                        <h3>QUE NO TE DE MIEDO HABLAR</h3>
                        <p>Si alguien te hace sentir incómod@ o te asusta con amenazas en <?php echo $sitename; ?>, repórtalo a un moderador usando el Botón del Pánico.</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips6_n.png" alt="Esqueça a webcam" class="align-right"></p>
                        <h3>APAGA LA CÁMARA</h3>
                        <p>No puedes controlar tus fotos e imágenes una vez que las has lanzado a través de Internet o la webcam. No hay vuelta atrás… Cualquiera puede compartirlas con quien quiera y puede usarlas para acosarte o amenazarte. Por favor, si lo vas a hacer, piensa antes: ¿Estás seguro de que te da igual que cualquiera pueda ver esa imagen?</p>
                        <hr>
                        <p><img src="<?php echo $aka; ?>/c_images/Security/safetytips7_n.png" alt="Seja esperto" class="align-right"></p>
                        <h3>NAVEGA CON CABEZA</h3>
                        <p>Las webs que ofrecen Créditos gratis, Furni, o simulan ser <?php echo $sitename; ?> Hotel o páginas de trabajadores de <?php echo $sitename; ?>, son sites para timar. Están diseñados para robar tu contraseña. No facilites ningún dato personal ni te descargues archivos en ellas; podrías bajarte algún virus o keylogger.</p>
                    </article>
                    <article style="" key="common/box_parents_guide" class="aside aside--box aside--fixed aside--push-down static-content" ng-show="show">
                        <h3>CONSEJOS PARA PADRES</h3>
                        <p>¿Conoces las herramientas disponibles para que los usuarios puedan divertirse en <?php echo $sitename; ?> dentro de un ambiente seguro? Echa un vistazo a nuestros <a href="https://help.habbo.es/hc/es/articles/221678648">Consejos para Padres en el Ayudante de <?php echo $sitename; ?></a>.</p>
                    </article>
                    <article style="" key="common/box_need_help" class="aside aside--box aside--fixed static-content" ng-show="show">
                        <h3>¿NECESITAS AYUDA?</h3>
                        <p>Consulta cómo puedes echarte un cable o pedir la ayuda de un moderador en nuestro apartado de <a href="/playing-habbo/help">Atención al Usuario</a>. También incluye una lista de números de teléfono y páginas web en caso de que necesites hablar con alguien. Si no encuentras una respuesta adecuada en esta página, ponte en contacto con el <a href="https://help.habbo.es">Ayudante de <?php echo $sitename; ?></a>.</p>
                    </article>
                </section>
            </section>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root"></div>
    <script src="//connect.facebook.net/pt_BR/sdk.js" async="" id="facebook-jssdk"></script>
</body>

</html>
<?php
if (!isset($_SESSION['id'])) {
require_once ('includes/modal_login.php');
}
?>