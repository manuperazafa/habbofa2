<?php
require_once ('heliocms/core.php');
require_once ('heliocms/session.php');
require_once ('web/templates/header.php');
if ($_GET['save'] == "$w") {
$message = '<div class="msg"><b>EXITO!</b> has actualizado tu perfil correctamente</div>';
}
$profilevisible_a = mysql_query("SELECT * FROM heliocms_profilesettings WHERE email='$user_q[mail]'");
$profilevisible_q = mysql_fetch_assoc($profilevisible_a);
if (isset($_POST['save'])) {
$password = $_POST['passwordCurrent'];
$email = $_POST['emailAddress'];
$password_verify = mysql_query("SELECT * FROM users WHERE mail='$user_q[mail]' AND password='".MD5($password)."'");
$emailaddress_verify = mysql_query ("SELECT * FROM users WHERE mail='$email'");
if (empty($password)) {
$error = '1';
$passwordcurrent_errors_2 = '<div class="box-rojo">Este campo es obligatorio.</div>';
$passwordcurrent_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
}else{
if (mysql_num_rows($password_verify) == 0) {
$error = '1';
$passwordcurrent_errors = '<div class="box-rojo">Vaya, la contraseña no es correcta. ¡Inténtalo de nuevo!</div>';
$passwordcurrent_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}}
if (mysql_num_rows($emailaddress_verify) == 1) {
$error = '1';
$emailaddress_errors = '<div class="box-rojo">El email ya está en uso, por favor usa otra dirección.</div>';
$emailaddress_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}else{
if (empty($email)) {
$error = '1';
$emailaddress_errors_2 = '<div class="box-rojo">Este campo es obligatorio.</div>';
$emailaddress_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}else{
if (!preg_match("/^[A-Z0-9._-]{2,}+@[A-Z0-9._-]{2,}\.[A-Z0-9._-]{2,}$/i", $email)) {
$error = '1';
$emailaddress_errors = '<div class="box-rojo">Necesitas un email válido.</div>';
$emailaddress_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}}}
if ($error <> 1) {
mysql_query ("UPDATE users SET mail='$email' WHERE mail='$user_q[mail]'");
mysql_query ("UPDATE heliocms_avatars SET parent_email='$email' WHERE parent_email='$user_q[mail]'");
mysql_query ("UPDATE heliocms_safetyquestions SET email='$email' WHERE email='$user_q[mail]'");
mysql_query ("UPDATE heliocms_profilesettings SET email='$email' WHERE email='$user_q[mail]'");
header ("Location: $site/ajustes/correo?save=$w");
}}
?>
<style>
.msg {
	color: #fff;
	background: green;
	padding: 5px;
	width: 100%;
}

input {
	background-color: transparent !important; 
    border: 1px solid #ccc !important;
    border-radius: 0;
	height: 20px !important;
    outline: none;
    width: 50% !important;
}

input:focus {border-bottom: none 1px solid #ccc;box-shadow: none !important;}

[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:after {background-color: #424242 !important;}
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:before, [type="radio"].with-gap:checked+label:after {border: 2px solid #424242 !important;}
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {padding-left: 25px !important;}

#slideselector {
    position: absolue;
    top:0;
    left:0;
    border: 2px solid black;
    padding-top: 1px;
}
.slidebutton {
    height: 21px;
    margin: 2px;
}
#slideshow { 
    margin: 50px auto; 
    position: relative; 
    width: 240px; 
    height: 240px; 
    padding: 10px; 
    box-shadow: 0 0 20px rgba(0,0,0,0.4); 
}

#slideshow > div { 
    position: absolute; 
    top: 10px; 
    left: 10px; 
    right: 10px; 
    bottom: 10px;
    overflow:hidden;
}

.imgLike {
    width:100%;
    height:100%;
}
/* Radio */

input[type="radio"] {
    background-color: #ddd;
    background-image: -webkit-linear-gradient(0deg, transparent 20%, hsla(0,0%,100%,.7), transparent 80%),
                      -webkit-linear-gradient(90deg, transparent 20%, hsla(0,0%,100%,.7), transparent 80%);
    border-radius: 10px;
    box-shadow: inset 0 1px 1px hsla(0,0%,100%,.8),
                0 0 0 1px hsla(0,0%,0%,.6),
                0 2px 3px hsla(0,0%,0%,.6),
                0 4px 3px hsla(0,0%,0%,.4),
                0 6px 6px hsla(0,0%,0%,.2),
                0 10px 6px hsla(0,0%,0%,.2);
    cursor: pointer;
    display: inline-block;
    height: 15px;
    margin-right: 15px;
    position: relative;
    width: 15px;
    -webkit-appearance: none;
}
input[type="radio"]:after {
    background-color: #444;
    border-radius: 25px;
    box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.4),
                0 1px 1px hsla(0,0%,100%,.8);
    content: '';
    display: block;
    height: 7px;
    left: 4px;
    position: relative;
    top: 4px;
    width: 7px;
}
input[type="radio"]:checked:after {
    background-color: #f66;
    box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.4),
                inset 0 2px 2px hsla(0,0%,100%,.4),
                0 1px 1px hsla(0,0%,100%,.8),
                0 0 2px 2px hsla(0,70%,70%,.4);
}

.mercuryzero_btn {
	background: #fff;
    border: 3px solid #000;
    border-bottom: 5px solid #000;
    border-radius: 4px;
    font-weight: 600;
    padding: 2px 13px;
}

.box-rojo {
	width: 60%;
	background: rgba(255, 0, 0, 0.64);
	color: #fff;
	border-radius: 3px;
	text-align: center;
}
</style>
<div class="container">
		<div class="row">
			<div class="col s12 m3">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Ajustes</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <a href="<?php echo $site; ?>/ajustes/perfil" style="color: #000;">Mi perfil</a><br>
				  <b><a href="<?php echo $site; ?>/ajustes/correo" style="color: #000;">Email y verificaci&oacute;n</a><br></b>
				  <a href="<?php echo $site; ?>/ajustes/contrasena" style="color: #000;">Mi contrase&ntilde;a</a><br>
				</div>
			  </div>
			</div>
			
			<div class="col s12 m9">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Correo electr&oacute;nico</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				 <?php echo $message; ?>
				 <form name="emailChangeForm" method="post">
				  <p>
					<br>
					Tu correo electr&oacute;nico actual: <?php echo $user_q['mail']; ?>
					
					<br>
					<br>
					<hr>
				  </p>
				  <p>
				    <b>Nuevo email</b><br>
					<?php echo $emailaddress_errors; ?>
					<?php echo $emailaddress_errors_2; ?>
				    Introduce un correo v&aacute;lido por si pierdes tu contrase&ntilde;a o la newlestter de <?php echo $sitename; ?> Hotel<br>
				    Correo: <input name="emailAddress" value="<?php echo $email; ?>">
				  </p>
				  <p>
				    <b>Contrase&ntilde;a</b><br>
					<?php echo $passwordcurrent_errors_2; ?>
					<?php echo $passwordcurrent_errors; ?>
				    Por metodos de seguridad pedimos que introduzcas tu contrase&ntilde;a<br>
				    Contrase&ntilde;a: <input type="password" name="passwordCurrent" value="<?php echo $password; ?>">
				  </p>
				  
				  <button type="submit" class="mercuryzero_btn" name="save">Salvar cambios</button>
				 </form>
				</div>
			  </div>
			</div>
		</div>
</div>
<?php require_once 'web/templates/footer.php'; ?>