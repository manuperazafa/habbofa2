<?php
require_once ('heliocms/core.php');
require_once ('heliocms/session.php');
if ($account_blocked_q['active'] == '1') {
header ("Location: $site");
}
if ($_GET['save'] == "$w") {
$success_message = '<div id="toast_container_s" class="toast-top-center" aria-live="polite" role="alert"><div class="toast toast-success" style="display: table;"><div class="toast-message">Tu contraseña fue modificada correctamente.</div></div></div>';
}
$passwordcurrent_class = 'form__input ng-pristine password-toggle-mask ng-invalid ng-invalid-required ng-touched';
$passwordnewrepeat_class = 'form__input ng-pristine ng-untouched password-toggle-mask ng-invalid ng-invalid-required ng-valid-matches';
$passwordnew_class = 'form__input ng-pristine password-toggle-mask ng-invalid ng-invalid-required ng-valid-password-name ng-valid-password-email ng-valid-minlength ng-valid-maxlength ng-valid-password-pattern ng-touched';
if (isset($_POST['save'])) {
$password = $_POST['passwordNew'];
$password_repeat = $_POST['passwordNewRepeated'];
$passwordcurrent = $_POST['passwordCurrent'];
$password_verify = mysql_query("SELECT * FROM users WHERE mail='$user_q[mail]' AND password='".MD5($passwordcurrent)."'");
if (empty($password)) {
$error = '1';
$passwordNew_errors = '<div ng-if="form.passwordNew.$invalid &amp;&amp; (!form.passwordNew.$pristine || form.$submitted)" ng-messages="form.passwordNew.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><div ng-message="required">Este campo es obligatorio.</div><!-- ngMessage: maxlength --><!-- ngMessage: minlength --><!-- ngMessage: passwordPattern --><!-- ngMessage: passwordName, remoteDataPasswordName --><!-- ngMessage: passwordEmail, remoteDataPasswordEmail --></div>';
$passwordnew_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}else{
if(strlen($password) < 6) {
$error = '1';
$passwordNew_errors = '<div ng-if="form.passwordNew.$invalid &amp;&amp; (!form.passwordNew.$pristine || form.$submitted)" ng-messages="form.passwordNew.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><!-- ngMessage: maxlength --><!-- ngMessage: minlength --><div ng-message="minlength" class=""><div class="password-strength" model-value="passwordNew" model-name="passwordNew" error-label="ERROR_PASSWORD_MIN_LENGTH"><div class="password-strength__label"><span>Seguridad de la contraseña:</span> <span>Baja</span></div><div class="password-strength__field"><div ng-class="(score | strengthRating)" class="password-strength__indicator fail" style="width: 0%;"></div></div><!-- ngIf: errorLabel --><p ng-if="errorLabel" class="password-strength__error">Tienes que introducir una contraseña más larga. Al menos 6 caracteres.</p><!-- end ngIf: errorLabel --></div></div><!-- ngMessage: passwordPattern --><!-- ngMessage: passwordName, remoteDataPasswordName --><!-- ngMessage: passwordEmail, remoteDataPasswordEmail --></div>';
$passwordnew_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
}else{
if (empty($password_repeat)) {
$error = '1';
$passwordNewRepeat_errors = '<div ng-if="form.passwordNew.$invalid &amp;&amp; (!form.passwordNew.$pristine || form.$submitted)" ng-messages="form.passwordNew.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><div ng-message="required">Este campo es obligatorio.</div><!-- ngMessage: maxlength --><!-- ngMessage: minlength --><!-- ngMessage: passwordPattern --><!-- ngMessage: passwordName, remoteDataPasswordName --><!-- ngMessage: passwordEmail, remoteDataPasswordEmail --></div>';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}else{
if ($password <> $password_repeat) {
$error = '1';
$passwordNewRepeat_errors = '<div ng-if="form.passwordNew.$valid &amp;&amp; form.passwordNewRepeated.$invalid" ng-messages="form.passwordNewRepeated.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><!-- ngMessage: matches --><div ng-message="matches">Las contraseñas no coinciden.</div></div>';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}}}}
if (empty($passwordcurrent)) {
$error = '1';
$passwordcurrent_errors = '<div style="" ng-if="form.passwordCurrent.$invalid &amp;&amp; (!form.passwordCurrent.$pristine || form.$submitted)" ng-messages="form.passwordCurrent.$error" class="form__popover form__popover--error ng-active"><!-- ngMessage: required --><div ng-message="required">Este campo es obligatorio.</div><!-- ngMessage: remoteDataPassword --></div>';
$passwordcurrent_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
}else{
if (mysql_num_rows($password_verify) == 0) {
$error = '1';
$passwordcurrent_errors = '<div style="" ng-if="form.passwordCurrent.$invalid &amp;&amp; (!form.passwordCurrent.$pristine || form.$submitted)" ng-messages="form.passwordCurrent.$error" class="form__popover form__popover--error ng-active"><!-- ngMessage: required --><!-- ngMessage: remoteDataPassword --><div ng-message="remoteDataPassword">Tu contraseña no es correcta, ¡intenta nuevamente!</div></div>';
$passwordcurrent_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}}
if ($error <> 1) {
mysql_query ("UPDATE users SET password='".MD5($password)."' WHERE mail='$user_q[mail]'");
header ("Location: $site/settings/password?save=$w");
}}
?>
<!DOCTYPE html>
<html ng-app="app" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="NOODP">
    <title>Cambiar contraseña - <?php echo $sitename; ?></title>
    <!--[if (lte IE 9)|(IEMobile)]><script>window.location = '<?php echo $site; ?>/br/upgrade/';</script>
        <!--<![endif]-->
    <meta name="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?php echo $sitename; ?>">
    <meta property="og:title" content="Faça amigos, divirta-se e seja famoso!">
    <meta property="og:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta property="og:url" content="<?php echo $site; ?><?php echo $og; ?>" head-url="content">
    <meta property="og:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta property="og:image:height" content="628">
    <meta property="og:image:width" content="1200">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Modificar senha">
    <meta name="twitter:description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta name="twitter:image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="twitter:site" content="@<?php echo $sitename; ?>PTBR">
    <meta itemprop="name" content="Modificar senha">
    <meta itemprop="description" content="Faça o seu check-in no maior Hotel virtual do mundo DE GRAÇA! Você poderá fazer novos amigos, jogar e criar seus próprios jogos, bater papo, construir seus quartos e muito mais!">
    <meta itemprop="image" content="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_summary_image-1200x628.png">
    <meta name="apple-itunes-app" content="app-id=794866182">
    <meta name="fragment" content="!">
    <meta name="revision" content="d1a83d6">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="prerender-status-code" prerender-status-code="" content="200">
    <meta name="prerender-header" prerender-header="" content="Location: <?php echo $site; ?><?php echo $ob; ?>">
    <link rel="stylesheet" href="<?php echo $aka; ?>/habbo-web/america/pt/app.css">
    <link rel="canonical" href="<?php echo $site; ?>/" head-url="href">
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <link rel="shortcut icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/favicon.ico">
    <link rel="icon" sizes="196x196" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-196x196.png">
    <link rel="apple-touch-icon" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-60x60-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-76x76-precomposed.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $aka; ?>/habbo-web/america/pt/assets/images/app_icon-152x152-precomposed.png">
    <link rel="alternate" type="application/rss+xml" href="<?php echo $site; ?>/rss.xml" title="<?php echo $sitename; ?> News">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular">
    <script src="//pagead2.googlesyndication.com/pagead/expansion_embed.js?source=safeframe"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/manifest/login?version=1.110.0_widgets_497" type="text/javascript"></script>
    <script src="https://d29usylhdk1xyu.cloudfront.net/translations/login/pt-BR" type="text/javascript"></script>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20160119135516/recaptcha__pt_br.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ecommerce.js"></script>
    <script async="" type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="d1a83d6" data-releasestage="hhbr"></script>
    <script src="https://partner.googleadservices.com/gpt/pubads_impl_79.js" async=""></script>
    <link rel="stylesheet" href="https://d3hmp0045zy3cs.cloudfront.net/2.2.21/providers.css" type="text/css">
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/osd.js"></script>
</head>
<?php echo $success_message; ?>
<body class="" client-disable-scrollbars="">
    <section class="content">
        <!-- uiView: undefined -->
        <ui-view style="" class="">
            <div active="settings" class="header header--small">
                <div class="header__background">
                    <!-- ngIf: hasAd -->
                    <div class="header__hotel"></div>
                    <header class="header__wrapper wrapper"><a href="/" class="header__habbo__logo"><h1 class="header__habbo__name" id="ga-linkid-habbo"><?php echo $sitename; ?></h1></a>
                        <!-- requireSession:  -->
                <div require-session="" class="header__aside header__aside--user-menu">
                    <div false-on-outside-click="toggle" class="user-menu">
                        <div class="user-menu__header">
                            <a id="ul-click" ng-click="click()">
                                <div class="user-menu__avatar__wrapper"><img width="54" height="62" class="user-menu__avatar imager" figure="<?php echo $user_q['look']; ?>" size="bighead" alt="<?php echo $user_q['username']; ?>" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&headonly=1&size=b&gesture=sml&direction=2&head_direction=2&action=std"></div>
                                <div class="user-menu__name__wrapper">
                                    <div id="ul-toggle" class="user-menu__name" ng-class="{ 'user-menu__name--open': toggle }">
                                        <div class="user-menu__name__container"><?php echo $user_q['username']; ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <ul ng-hide="!toggle" id="ul-hide" style="display: none" class="user-menu__list">
                            <li class="user-menu__item"><a ng-href="/profile/<?php echo $user_q['username']; ?>" ng-class="{ 'user-menu__link--active': isMyProfileActive() }" class="user-menu__link user-menu__link--profile" translate="NAVIGATION_PROFILE" href="/profile/<?php echo $user_q['username']; ?>">Ver mi perfil público</a></li>
                            <li class="user-menu__item"><a href="/settings" ng-class="{ 'user-menu__link--active': isSettingsActive() }" class="user-menu__link user-menu__link--settings user-menu__link--active" translate="NAVIGATION_SETTINGS">Ajustes</a></li>
                            <li class="user-menu__item"><a ng-href="/api/public/help?returnTo=https://help.habbo.es" class="user-menu__link user-menu__link--help" target="_blank" translate="NAVIGATION_HELP" href="/api/public/help?returnTo=https://help.habbo.es">Ayuda</a></li>
                            <li class="user-menu__item"><a onclick="logout()" ng-click="logout()" class="user-menu__link user-menu__link--logout" translate="NAVIGATION_LOGOUT">Salir</a></li>
                        </ul>
                    </div>
                </div>
                        <!-- end ngIf: function (){
"use strict";
return t.hasSession()} -->
                        <!-- requireNoSession:  -->
                    </header>
                    <nav active="settings" class="navigation">
                        <ul class="navigation__menu">
                            <li class="navigation__item"><a href="/" ng-class="{ 'navigation__link--active': isActive('home') }" class="navigation__link navigation__link--home" translate="NAVIGATION_HOME" id="ga-linkid-home">Inicio</a></li>
                            <li class="navigation__item"><a href="/community" ng-class="{ 'navigation__link--active': isActive('community') }" class="navigation__link navigation__link--community" translate="NAVIGATION_COMMUNITY" id="ga-linkid-community">Comunidad</a></li>
                            <li class="navigation__item"><a href="/playing-habbo" ng-class="{ 'navigation__link--active': isActive('playing<?php echo $sitename; ?>') }" class="navigation__link navigation__link--playing-habbo" translate="NAVIGATION_PLAYING_HABBO" id="ga-linkid-playing-habbo">Descubre <?php echo $sitename; ?></a></li>
                            <!-- requireSession:  -->
                            <li require-session="" class="navigation__item navigation__item--aside navigation__item--hotel">
                                <!-- requireFlash:  --><a id="ga-linkid-hotel" require-flash="" href="/hotel" class="hotel-button"><span class="hotel-button__text" translate="NAVIGATION_HOTEL">Hotel</span></a>
                                <!-- end ngIf: function (){
"use strict";
return a.isEnabled()||!t.test(e.navigator.userAgent)} -->
                            </li>
                            <!-- end ngIf: function (){
"use strict";
return e.hasSession()} -->
                        </ul>
                    </nav>
                    <div class="wrapper" ng-transclude=""></div>
                </div>
            </div>
            <nav title-key="SETTINGS_TITLE" ng-hide="tabs.length < 2" false-on-outside-click="isOpen" class="tabs">
                <div class="tabs__wrapper">
                    <!-- ngIf: titleKey -->
                    <h1 ng-if="titleKey" class="tabs__title" translate="SETTINGS_TITLE">Ajustes</h1>
                    <!-- end ngIf: titleKey -->
                    <div id="tab-click" ng-click="isOpen = !isOpen" class="tabs__toggle">
                        <div id="tab-arrow" ng-class="{'tabs__toggle__title--active': isOpen}" class="tabs__toggle__title" translate="SETTINGS_PASSWORD_TAB">Contraseña</div>
                    </div>
                    <ul id="tab-hide" class="tabs__menu ng-hide" ng-hide="!isOpen" ng-transclude="">
                        <li path="/settings/privacy" translation-key="SETTINGS_PRIVACY_TAB" class="tabs__item"><a style="" href="/settings/privacy" ng-href="/settings/privacy" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_PRIVACY_TAB">Privacidad</a></li>
                        <!-- require<?php echo $sitename; ?>AccountSession:  -->
                        <li require-habbo-account-session="" path="/settings/security" translation-key="SETTINGS_ACCOUNT_SECURITY_TAB" class="tabs__item"><a style="" href="/settings/security" ng-href="/settings/security" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_ACCOUNT_SECURITY_TAB">Protección de la cuenta</a></li>
                        <!-- end ngIf: function (){
"use strict";
return e.is<?php echo $sitename; ?>AccountSession()} -->
                        <!-- require<?php echo $sitename; ?>AccountSession:  -->
                        <li require-habbo-account-session="" path="/settings/password" translation-key="SETTINGS_PASSWORD_TAB" class="tabs__item"><a style="" href="/settings/password" ng-href="/settings/password" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link tabs__link--active" translate="SETTINGS_PASSWORD_TAB">Contraseña</a></li>
                        <!-- end ngIf: function (){
"use strict";
return e.is<?php echo $sitename; ?>AccountSession()} -->
                        <!-- require<?php echo $sitename; ?>AccountSession:  -->
                        <li require-habbo-account-session="" path="/settings/email" translation-key="SETTINGS_EMAIL_TAB" class="tabs__item"><a href="/settings/email" ng-href="/settings/email" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_EMAIL_TAB">Email</a></li>
                        <!-- end ngIf: function (){
"use strict";
return e.is<?php echo $sitename; ?>AccountSession()} -->
                        <li path="/settings/avatars" translation-key="SETTINGS_AVATAR_TAB" class="tabs__item"><a href="/settings/avatars" ng-href="/settings/avatars" ng-class="{ 'tabs__link--active': isActive }" class="tabs__link" translate="SETTINGS_AVATAR_TAB">Avatars</a></li>
                    </ul>
                </div>
            </nav>
            <section class="wrapper wrapper--content">
                <div class="row">
                    <!-- uiView:  -->
                    <div style="" class="main" ui-view="">
                        <h2 translate="PASSWORD_CHANGE_TITLE">Cambiar contraseña</h2>
                        <form method="post" ng-submit="update()" name="changePasswordForm" autocomplete="off" novalidate="" class="form form--left ng-pristine ng-invalid ng-invalid-required ng-valid-password-name ng-valid-password-email ng-valid-minlength ng-valid-maxlength ng-valid-password-pattern ng-valid-matches">
                            <fieldset password-current="passwords.currentPassword" class="form__fieldset">
                                <label for="password-current" translate="CURRENT_PASSWORD_LABEL" class="form__label">Contraseña actual</label>
                                <div class="form__field">
                                    <input value="<?php echo $passwordcurrent; ?>" id="password_current" name="passwordCurrent" ng-model="passwordCurrent" ng-model-options="{ updateOn: 'default blur', debounce: { default: 500, blur: 0 } }" required="" password-toggle-mask="" remote-data="'password'" autocomplete="off" class="<?php echo $passwordcurrent_class; ?>" type="password"><i id="password_current_mask" class="password-toggle-mask__icon"></i>
                                    <!-- ngIf: form.passwordCurrent.$invalid && (!form.passwordCurrent.$pristine || form.$submitted) -->
									<?php echo $passwordcurrent_errors; ?>
                                </div>
                            </fieldset>
                            <div is-new="true" password-new="passwords.password" password-new-repeated="passwords.passwordRepeated" user-name="<?php echo $user_q['username']; ?>" user-email="<?php echo $user_q['mail']; ?>" class="password-new">
                                <fieldset class="form__fieldset form__fieldset--box form__fieldset--box-top">
                                    <label for="password-new" class="form__label" translate="NEW_PASSWORD_LABEL">Contraseña nueva</label>
                                    <p translate="PASSWORD_CHANGE_HELP">Usa al menos 6 caracteres. Incluye al menos una letra y un número o un caracter especial.</p>
                                    <div class="form__field">
                                        <input value="<?php echo $password; ?>" id="password_new" name="passwordNew" ng-model="passwordNew" ng-model-options="{ updateOn: 'default blur', debounce: { default: 500, blur: 0 } }" required="" ng-maxlength="32" ng-minlength="6" password-pattern="" password-email="bauerrodrigues@live.com" password-name="bauerchef" password-toggle-mask="" remote-data="['passwordName', 'passwordEmail']" autocomplete="off" class="<?php echo $passwordnew_class; ?>" type="password"><i id="password_new_mask" class="password-toggle-mask__icon"></i>
                                        <!-- ngIf: form.passwordNew.$invalid && (!form.passwordNew.$pristine || form.$submitted) -->
										<?php echo $passwordNew_errors; ?>
                                        <!-- ngIf: !form.passwordNew.$pristine && form.passwordNew.$valid -->
                                    </div>
                                </fieldset>
                                <fieldset class="form__fieldset form__fieldset--box form__fieldset--box-bottom">
                                    <label for="password-new-repeated" class="form__label" translate="PASSWORD_REPEAT_LABEL">Repite contraseña</label>
                                    <div class="form__field">
                                        <input value="<?php echo $password_repeat; ?>" id="password_new_repeated" name="passwordNewRepeated" ng-model="passwordNewRepeated" ng-model-options="{ updateOn: 'default blur', debounce: { default: 500, blur: 0 } }" required="" matches="passwordNew" password-toggle-mask="" autocomplete="off" class="<?php echo $passwordnewrepeat_class; ?>" type="password"><i id="password_new_repeated_mask" class="password-toggle-mask__icon"></i>
                                        <!-- ngIf: form.passwordNew.$valid && form.passwordNewRepeated.$invalid -->
										<?php echo $passwordNewRepeat_errors; ?>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form__footer">
                                <button name="save" ng-disabled="updateInProgress" type="submit" class="form__submit" translate="FORM_BUTTON_CHANGE">Cambiar</button>
                            </div>
                        </form>
                    </div>
                    <article style="" key="common/box_learn_how_to_stay_safe" class="aside aside--box aside--push-down static-content" ng-show="show">
                        <h3>CONSEJOS DE SEGURIDAD</h3>
                        <p>¡Protégete con conciencia! Aprende cómo <a href="/playing-habbo/safety">Navegar seguro por Internet</a>.</p>
                    </article>
                    <article style="" key="common/box_need_help" class="aside aside--box static-content" ng-show="show">
                        <h3>¿NECESITAS AYUDA?</h3>
                        <p>Consulta cómo puedes echarte un cable o pedir la ayuda de un moderador en nuestro apartado de <a href="/playing-habbo/help">Atención al Usuario</a>. También incluye una lista de números de teléfono y páginas web en caso de que necesites hablar con alguien. Si no encuentras una respuesta adecuada en esta página, ponte en contacto con el <a href="https://help.habbo.es">Ayudante de <?php echo $sitename; ?></a>.</p>
                    </article>
                </div>
            </section>
        </ui-view>
    </section>
<?php
require_once ('includes/footer.php');
?>
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="1492699e4b5e2ef6b25d19d4e1b9e64e" data-appversion="20e33e2" data-releasestage="hhbr"></script>
    <script>
        ! function() {
            var h = Date.now();
            window.Bugsnag && (window.Bugsnag.notifyReleaseStages = ["hhde", "hhus", "hhes", "hhfi", "hhfr", "hhit", "hhnl", "hhbr", "hhtr"], window.Bugsnag.beforeNotify = function(e) {
                return e.metaData.secondsSincePageLoad = (Date.now() - h) / 1e3, /habbo\./.test(e.url)
            })
        }();
    </script>
    <script src="<?php echo $aka; ?>/habbo-web/america/pt/scripts.js" defer="defer"></script>
    <script>
        ! function(e, n, a, o, t, r, i) {
            e.GoogleAnalyticsObject = t, e[t] = e[t] || function() {
                (e[t].q = e[t].q || []).push(arguments)
            }, e[t].l = 1 * new Date, r = n.createElement(a), i = n.getElementsByTagName(a)[0], r.async = 1, r.src = o, i.parentNode.insertBefore(r, i)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-448325-57", "auto"), ga("require", "ecommerce"), ga("require", "linkid", "linkid.js"), window.partnerCodeInfo && (ga("set", "campaignName", window.partnerCodeInfo.campaign), ga("set", "campaignSource", window.partnerCodeInfo.theme || window.partnerCodeInfo.partner), ga("set", "campaignMedium", window.partnerCodeInfo.media));
    </script>
    <div id="fb-root"></div>
    <script src="//connect.facebook.net/pt_BR/sdk.js" async="" id="facebook-jssdk"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=recaptchaOnloadCallback&amp;render=explicit&amp;hl=pt-BR" type="application/javascript"></script>
</body>

</html>