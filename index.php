<?php require_once ('heliocms/core.php'); ?>
<?php if (isset($_SESSION['id'])) { ?>
<?php include 'web/templates/header.php'; ?>
<style>
.info-profile {
	background: url('http://www.habbo.com/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&action=wav&gesture=sml&size=l');
}
.credits {
	background: url('<?php echo $site; ?>/web/images/icon_credits.png') no-repeat;
}
.duckets {
	background: url('<?php echo $site; ?>/web/images/icon_duckets.png') no-repeat;
}
.diamonds {
	background: url('<?php echo $site; ?>/web/images/icon_diamonds.png') no-repeat;
}
.button .left {
	background: url('<?php echo $site; ?>/web/images/left-button.png') no-repeat;
}
.button .center {
	background: url('<?php echo $site; ?>/web/images/center-button.png');
}
.button .right {
	background: url('<?php echo $site; ?>/web/images/right-button.png') no-repeat;
}
</style>
	<title></title>
	  <div class="container">
		  <div class="row">
		    <!-- Información del personaje -->
			<div class="col s12 m12">
			  <div class="card blue-grey darken-1" style="border-radius: 5px !important;border-top: none !important;padding: 0px !important;">
				<div class="card-content white-text" style="background: url('<?php echo $site; ?>/web/images/home-info-me.png') 100%;padding: 0px;border: 3px solid #fff;border-radius: 5px;">
				  <p>
				  <table>
					<tr>
					  <td style="float: left;padding: 0px;">
						<div class="info-profile">
						
						</div>
					  </td>
					  <td style="float: left;">
						<h5 style="color: #fff;font-size: 22px;">Bienvenid@, <?php echo $user_q['username']; ?>!</h5>
								<?php if ($hotel_q['status_client'] == 'on') { ?>
								<a href="<?php echo $site; ?>/hotel" class="waves-effect waves-light btn"><i class="material-icons left">label</i> Entrar en <?php echo $sitename; ?> Hotel <i class="material-icons right">label</i></a>				
								<style>
								.btn {background: #4CAF50;box-shadow: none;font-size: 13px;}
								.btn:hover {background: #52c756;opacity: 0.9;box-shadow: none;}
								</style>
								<?php } ?>
								<?php if ($hotel_q['status_client'] == 'off') { ?>
								<a href="#<?php echo $sitename; ?>-off" class="waves-effect waves-light btn"><i class="material-icons left">label</i>Entrar en <?php echo $sitename; ?> Hotel <i class="material-icons right">label</i></a>				
								<style>
								.btn {background: #af4c4c;box-shadow: none;font-size: 13px;}
								.btn:hover {background: #c75252;opacity: 0.9;box-shadow: none;}
								</style>
								<?php } ?>
					  </td>
					  <td style="float: right;">
					  <div class="bubble-stats" style="color: #fff;width: 90px;height: 90px;background: rgba(0, 0, 0, 0.52);    padding: 18px;	font-size: 13px;    border-radius: 60px;    border: 2px dashed #fff;    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.68);    text-align: center;	margin-right: 90px;    margin-top: 7px;">
					  <b><?php echo $users_online; ?></b> <br>online
					  </div>
					  </td>
					</tr>
				  </table>
				  </p>
				</div>
			  </div>
			</div>
			<!-- Créditos, duckets, referidos... -->
			<div class="col s12 m12">
			  <div class="card blue-white darken-1">
				<div class="card-content black-text" style="padding: 0px;">
				  <table class="centered">
					<tbody>
					  <tr>
						<td><font class="credits" style="font-size: 13px;"><?php echo $user_q['credits']; ?></font></td>
						<td><font class="duckets" style="font-size: 13px;"><?php echo $user_q['activity_points']; ?></font></td>
						<td><font class="diamonds" style="font-size: 13px;"><?php echo $user_q['vip_points']; ?></font></td>
					  </tr>
					</tbody>
				  </table>
				</div>
			  </div>
			</div>
		</div>
		<div class="row">
		<div class="col s12 m12">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
				<div class="card-content black-text" style="padding: 0px;">
				<link href="<?php echo $site; ?>/web/css/promo.css" rel="stylesheet" >
				<script type="text/javascript" src="<?php echo $site; ?>/web/js/promo.js"></script>
				  <div id="promo-box">
				  <div id="promo-bullets" style="visibility: hidden;"></div>
	<?php $b = mysql_query("SELECT * FROM heliocms_news ORDER BY id DESC LIMIT 10"); while($a = mysql_fetch_assoc($b)){ ?>
	<?php $user = mysql_query("SELECT * FROM users WHERE id ='". $a['addedby'] ."'");$id = mysql_fetch_assoc($user); ?>

<div class="promo-container" style="background-image: url(&quot;<?php echo $a['image_url']; ?>&quot;); background-position: center center; display: none;">
<div class="promo-content">
<div class="title_news_100005372197939"><?php echo $a['title']; ?></div>
<div class="body_news_100005372197939">Noticia creada por: <?php echo $id['username']; ?></div><br>
</div><br>
<div id="bottom_promo_100005372197939"></div>
<style>
.showmore-news-center {
right: 12px;
bottom: 8px;
}
</style>
<a style="text-decoration:none;" href="<?php echo $site; ?>/news/<?php echo $a['title']; ?>" class="showmore-news-center">Leer Notícia »</a>

</div>
	<?php } ?>


</div>
				</div>
			  </div>
			  </div>
		</div>
		<div class="row">
			<div class="col s12 m7">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Noticias importantes</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <table class="striped">
				  <tbody>
				  <?php $staff_a = mysql_query("SELECT * FROM mercuryzero_news_important ORDER BY id DESC LIMIT 1"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
					<tr>
					<td><b></b></td>
					<td><b><?php print $staff_q['title']; //Titulo ?></b><br><?php print $staff_q['content']; //Titulo ?></td>
					</tr>
				  <?php } ?>
				  </tbody>
				  </table>
				</div>
			  </div>
			  
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue">
				<div class="title">Chat <?php echo $sitename; ?></div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <embed wmode="transparent" src="http://www.xatech.com/web_gear/chat/chat.swf" quality="high" width="100%" height="405" name="chat" FlashVars="id=<?php echo $chatid; ?>&rl=Spanish" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://xat.com/update_flash.php" />
				</div>
			  </div>
			</div>
			<div class="col s12 m5">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #e74c3c;">
				<div class="title">Radio oficial</div>
			  </div>
				<div class="card-content black-text" style="text-align: center;">
				<?php if ($user_q['user_radio'] == '1') { ?>
				<audio style="width: 100%;" controls="" autoplay="">
					<source src="http://listen.radionomy.com/electrodancer" type="audio/mp3">Tu explorador no es compatible con este elemento de audio.
				</audio>
				<br>
				La radio está <font color="green"><b>activada</b></font>, desactivala <a href="<?php echo $site; ?>/ajustes">aqui</a>.
				<?php } ?>
				<?php if ($user_q['user_radio'] == '0') { ?>
				La radio está <font color="red"><b>desactivada</b></font>, activala <a href="<?php echo $site; ?>/ajustes">aqui</a>.
				<?php } ?>
				</div>
			  </div>
			  
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #dec900;">
				<div class="title">Horario del servidor</div>
			  </div>
				<div class="card-content black-text" style="text-align: center;">
				 <center>
				  <b>
				   <div id="jclock2">
				    <script type="text/javascript" src="<?php echo $site; ?>/web/js/clock.js"></script>
				    <span class="clock24s" id="clock24_42969">
				    <script type="text/javascript">
				    var clock24_42969 = new clock24('42969',1500,'%HH:%nn:%ss %P','es');
				    clock24_42969.daylight('ES'); clock24_87630.refresh();
				    </script>
				   </div>
				  </b> 
				  <font size=1>(CEST) - Hora en <b>España</b>, Madrid</font>
				 </center>
				</div>
			  </div>
			  
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: transparent;border-bottom: 1.5px solid green;border-radius: 0px;">
				<div class="title" style="color: green;">Nuestras estadísticas</div>
			  </div>
				<div class="card-content black-text" style="text-align: center;">
				<p style="text-align: left;">
				Número de usuarios registrados: <b><?php $users = mysql_query("SELECT * from users"); echo mysql_num_rows($users); ?></b><br>
				<img src="http://i.imgur.com/ACmitKL.gif" style="float: right;">
				Número de salas: <b><?php $rooms = mysql_query("SELECT * from rooms"); echo mysql_num_rows($rooms); ?></b><br>
				Número de baneados: <b><?php $rooms = mysql_query("SELECT * from bans"); echo mysql_num_rows($rooms); ?></b><br>
				Número de noticias creadas: <b><?php $rooms = mysql_query("SELECT * from heliocms_news"); echo mysql_num_rows($rooms); ?></b><br>
				Número de furnis en el catálogo: <b><?php $rooms = mysql_query("SELECT * from catalog_items"); echo mysql_num_rows($rooms); ?></b>
				</p>
				</div>
			  </div>
			  
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #1BAFE0;">
				<div class="title">Últimos registrados</div>
			  </div>
				<div class="card-content black-text" style="text-align: center;">
				 <table class="centered striped">
				   <tbody>
					<?php $staff_a = mysql_query("SELECT * FROM users ORDER BY id DESC LIMIT 4"); while($staff_q = mysql_fetch_assoc($staff_a)){ ?>
					<tr>
						<td style="padding: 0px;"><div style="background: url(<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $staff_q['look']; ?>&direction=3&head_direction=3&gesture=sml&action=&size=2) no-repeat 0% 7%;height: 60px;width: 54px;"></div></td>
						<td style="padding: 0px;"><a href="<?php echo $site; ?>/profile/<?php echo $staff_q['username']; ?>" style="color: #000;"><?php echo $staff_q['username']; ?></a></td>
						<td style="padding: 0px;"><?php echo date_numeric("d/M/Y", $staff_q['account_created']); ?></td>
					</tr>
					<?php } ?>
				   </tbody>
				 </table>
				</div>
			  </div>
			</div>
		  </div>
	  </div>
	  <?php include 'web/templates/footer.php'; ?>
    </body>
  </html>
<?php if (isset($_SESSION['id'])) { ?>

<?php } ?>

<?php }else{ ?>
<?php 
require_once ('heliocms/core.php'); 
if (isset($_POST['login'])) {
$emailorusername = $_POST['emailorusername'];
$password = $_POST['password'];
$user_verify = mysql_query("SELECT * FROM users WHERE mail='$emailorusername' or username='$emailorusername' && password='".MD5($password)."' LIMIT 1");
$user_fetch = mysql_fetch_assoc($user_verify);
if (mysql_num_rows($user_verify) == 0) {
$error = '1';
$emailorusername_class = '<div class="loginerror">Usuario o contraseña incorrecto.</div>';
$password_class = '';
}else{
$ban_check = mysql_query("SELECT * FROM bans WHERE value='$user_fetch[username]'");
if(mysql_num_rows($ban_check) == 1){
$ban = mysql_fetch_assoc($ban_check);
$message = '<div class="redmsg #d50000 red accent-4" style="text-align: center;color: #fff;padding: 12px 0px;">Lo sentimos '.$ban['value'].', pero has sido baneado hasta el '. date_numeric("d/M/Y", $ban["expire"]) .' a las '. date_numeric("H:i", $ban["expire"]) .'</div>';
}else{
$user_safety_a = mysql_query("SELECT * FROM heliocms_safetyquestions WHERE email='$user_fetch[mail]'");
$user_safety_q = mysql_fetch_assoc($user_safety_a);
$_SESSION['id'] = $user_fetch['id'];
if ($user_safety_q['trusted_ip'] <> $ip) {
mysql_query("UPDATE heliocms_safetyquestions SET active='1' WHERE email='$user_fetch[mail]'");
}
mysql_query("UPDATE users SET ip_last='$ip' WHERE mail='$user_fetch[mail]'");
mysql_query("INSERT INTO heliocms_sessions (last,user_id) VALUES ('".time()."','$user_fetch[id]')");
header ("Location: $site/me");
}}}
?>

<html>
	<head>
		<title><?php echo $sitename; ?></title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
		<link rel="stylesheet" href="web/css/message.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet">
		<link href="web/css/style.index.css" rel="stylesheet" >
		<link rel="icon" type="image/png" href="web/images/favicon.png" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
		<script type="text/javascript">
			var num = 0
			function cuentaOns()
			{
				if(num <= 12)
				{
					document.getElementById('ca').firstChild.nodeValue = num
					num++
					setTimeout('cuentaOns()',10)
				}
			}
			function Ons()
			{
				document.write ('<span id="ca">' + num + '</span>')
			}
		</script>
		
	</head>

	<body onLoad="cuentaOns()">
		<div class="container">
		<?php echo $message; ?>
		<nav>
		  <div class="nav-wrapper">
			<a href="#!" class="brand-logo" style="padding: 20px 35px;"><img src="http://www.habbcrazy.net/resources/fonts/119/<?php echo $sitename; ?>.gif"></a>
			<a href="#" data-activates="mobile-demo" class="button-collapse"><i style="color: #000;padding: 10px 5px;" class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
			  <div class="bubble"><img class="icon" src="web/images/bubble_icon.png"><font style="font-weight: 300;"><b><script type="text/javascript">Ons()</script></b> Usuarios Online</font></div>
		    </ul>
		  </div>
		</nav>
		
		<div class="row">
        <div class="col s12 m6">
          <?php $b = mysql_query("SELECT * FROM heliocms_news ORDER BY id DESC LIMIT 1"); while($a = mysql_fetch_assoc($b)){ ?>
		  <div class="card" style="background: url('<?php echo $a['image_url']; ?>') no-repeat 100% 100%;border-radius: 7px;width: 100%;height: 240px;">
            <div class="card-content black-text">
				<span><?php echo $a['title']; ?></span>
				<br>
				<br>
				<font><?php echo date("d M Y", $a['time']); ?><!-- 13 Oct de 2016 --></font><br>
				<xd><?php echo $a['stext']; ?></xd>
				<br>
				<br>
				<br>
				<a href="<?php echo $site; ?>/noticias/<?php echo $a['title']; ?>" class="boton-noticia">Leer más</a>
            </div>
          </div>
		  <?php } ?>
        </div>
		
		<div class="col s12 m6">
          <div class="card transparent">
            <div class="card-content black-text" style="padding: 0px;">
				<div style="color: #424252;font-size: 26px;">Accede a tu cuenta</div>
				<a href="registro"><div style="background: url(http://img.webme.com/pic/k/kekochat2/murcielagos.gif);color: #fff;text-shadow: 0px 1px 0px #000;font-size: 15px;margin-top: -12px;font-weight: bold;">¿No tienes cuenta? ¡Crea una ahora mismo!</div></a>
				<br>
				<form name="loginForm" method="post">
				  <?php echo $emailorusername_class; ?>
				  <input type="text" class="userinput" placeholder="Usuario" value="<?php echo $emailorusername; ?>" name="emailorusername" required>
				  <input type="password" class="passwordinput" placeholder="Contraseña" value="<?php echo $password; ?>" name="password" required>
				  <button type="submit" name="login" class="btn-login">ACCEDER A MI CUENTA</button>
				</form>
            </div>
          </div>
        </div>
      </div>
	  
	  <div class="row">
	  <div class="col s12 m4">
          <div class="card">
            <div class="card-content black-text">
				<div class="module-header colorDiamond">
							<center>¿Qué es <?php echo $sitename; ?>?</center><img src="web/images/h.ico" style="margin-top: -18px;position: absolute;">
				</div>
				<p>Es una red social virtual donde puedes conocer a otras personas y crear lugares donde quedar.
			<b>Crea y Conoce</b>
			Crea salas donde quedar con tus amigos, desde un bar, hasta una discoteca, u otras cosas que se te ocurran.
			<b>Haz amigos</b>
			Podras encontrar a muchas personas de habla hispana que podras hacerte amigos de ellos.</p>
			<center><img style="margin-top: 8px;" src="web/images/way1.png"></center>
            </div>
          </div>
        </div>
		
		<div class="col s12 m4">
          <div class="card">
            <div class="card-content black-text">
				<div class="module-header colorBeige" style="font-size: 13px;">
							<center>¿Qué puedes hacer aquí?</center><img src="web/images/news.gif" style="margin-top: -18px;position: absolute;">
				</div>
				<p>Podrás hacer nuevos amigos, darte una vuelta con ellos por salas, desde lugares fiesteros, discotecas, playas, juegos... <br>
			<b>Construye</b>
			 lugares mas tranquilos para relajarte, biblioteca, spa, balneario... ¿no encuentras la sala que quieres? puedes creala tu mismo, ya que todos los créditos son totalmente <b>GRATIS</b>.</p>
			 <center><img style="margin-top: 8px;" src="web/images/way2.png"></center>
            </div>
          </div>
        </div>
		
		<div class="col s12 m4">
          <div class="card">
            <div class="card-content black-text">
				<div class="module-header colorDiamond">
							<center>Último registrado</center><img src="web/images/new_19.gif" style="margin-top: -18px;position: absolute;">
				</div>
				<center>					
				<?php $b = mysql_query("SELECT * FROM users ORDER BY id DESC LIMIT 1"); while($a = mysql_fetch_assoc($b)){ ?>
					<div class="list-index">
						<div class="avatar" style="background-image:url('http://avatar-retro.com/habbo-imaging/avatarimage?figure=<?php echo $a['look']; ?>&amp;size=b&amp;direction=2&amp;head_direction=2&amp;crr=2&amp;gesture=default&amp;frame=2&amp;action=');height: 119px;    width: 69px;    margin-top: -16px;"></div>
						<b>Nombre: </b><?php echo $a['username']; ?><br> 
						<b>Misión: </b><?php if ($a['motto'] == '') { ?>Nuev@ en <?php echo $sitename; ?><?php } ?><?php echo $a['motto']; ?>
					</div>
				<?php } ?>
				</center>
            </div>
          </div>
		  <div class="card">
		  <div class="card-content black-text">
		  <div class="module-header colorDiamond">
							<center>Opiniones de <?php echo $sitename; ?></center><img src="web/images/news.gif" style="margin-top: -18px;position: absolute;">
				</div>
				
		  
		  <div class="contenidoxd">
		    <div class="visible-message">
				<p>
				<ul style="margin-top:0;  text-align:left;  list-style:none;  animation:6s linear 0s normal none infinite change;  -webkit-animation:6s linear 0s normal none infinite change;  -moz-animation:6s linear 0s normal none infinite change;  -o-animation:6s linear 0s normal none infinite change;">
				<li style="color: #4c8486;line-height: 46px;">¡Es magnifico, me encanta!</li>
				<li style="line-height: 17px;"><x style="color: #BD9300;">Créditos</x>, <x style="color: #4C8486;">diamantes</x> y <x style="color: #9c27b0;">duckets</x> GRATIS y hay de todo :o</li>
				<li style="text-transform: uppercase;font-size: 20px;">#SOMOS <img src="web/images/bubble_icon.png"> <?php echo $sitename; ?></li>
				</ul>
				</p>
			</div>
		  </div>
		  <br>
				<br>
	</div>
	</div>
	
        </div>
	  </div>
		
        
		<div class="row" style="margin-bottom: 0px;padding: 0px;">
        <div class="col s12 m12" style="margin: 0px;">
          <div class="card white" style="margin: 0px;border-radius: 0px;box-shadow: 0px -1px 9px rgba(0, 0, 0, 0.68);">
            <div class="card-content black-text">
              <p>
			  <div class="c-left" style="float: right;text-align: right;margin-top: -13px;">
			    <b><?php echo $sitename; ?></b> es un proyecto independiente sin ánimo de lucro.<br>
			    No estamos aprobados u ofrecidos por los afiliados <b>Sulake Corporation LTD</b>.<br>
				<?php echo date("Y"); ?> &copy; <b><?php echo $sitename; ?></b> utiliza <b>MercuryCMS</b> by <b>Z3RO</b>
			  </div>
			  <div class="c-left"><img src="http://www.habbcrazy.net/resources/fonts/119/<?php echo $sitename; ?>.gif"></div>
			  </p>
            </div>
          </div>
        </div>
      </div>
		
		</div>
	</body>
</html>
<?php } ?>

<?php if (isset($_SESSION['id'])) { ?>

<?php } ?>
		