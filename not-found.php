<?php
require_once ('heliocms/core.php');
?><!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>404 error page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<META HTTP-EQUIV="REFRESH" CONTENT="3;URL=<?php echo $site; ?>">
<link href="<?php echo $site; ?>/web/404/css/style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
	<!-----start-wrap--------->
	<div class="wrap">
		<!-----start-content--------->
		<div class="content">
			<!-----start-logo--------->
			<div class="logo">
				<h1><a href="#"><img src="<?php echo $site; ?>/web/404/images/logo.png"/></a></h1>
				<span>Ups! La página solicitada no se ha encontrado!</span>
			</div>
			<!-----end-logo--------->
			<!-----start-search-bar-section--------->
			<div class="buttom">
				<div class="seach_bar">
				<br>
					<p>Serás redireccionado en 5 segundos .. </p>
					<!-----start-sear-box--------->
					<br>
					<br>
				</div>
			</div>
			<!-----end-sear-bar--------->
		</div>
		<!----copy-right-------------->
	</div>
	
	<!---------end-wrap---------->
</body>
</html>