﻿<footer class="page-footer" style="opacity: 0.7;background: #fff;">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="black-text"><?php echo $sitename; ?> Hotel</h5>
                <p class="black-text text-lighten-4"><b><?php echo $sitename; ?></b> es un proyecto independiente sin &aacute;nimo de lucro<br>
				No estamos aprobados u ofrecidos por los afiliados <b>Sulake Corporation LTD</b></p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="black-text">Enlaces</h5>
                <ul>
                  <li><a class="black-text text-lighten-3" href="#!">Facebook</a></li>
                  <li><a class="black-text text-lighten-3" href="#!">Twitter</a></li>
                  <li><a class="black-text text-lighten-3" href="#!">Instagram</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container black-text">
            <?php echo date("Y"); ?> &copy; <?php echo $sitename; ?> utiliza <a href="http://mercuryzero.com" class="black-text"><b>MercuryCMS</b> by <a href="https://www.facebook.com/zeroo.holos" class="black-text"><b>Z3RO</b></a>
            <a class="black-text text-lighten-4 right" href="http://mercuryzero.com">Powered by <b>mercuryzero.com</b></a>
            </div>
          </div>
        </footer>