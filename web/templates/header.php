<?php
if (!isset($_SESSION['id'])) {
header ("Location: $site/index.php");
}
?>
<!DOCTYPE html>
  <html>
    <head>
	  <title><?php echo $sitename; ?></title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="<?php echo $site; ?>/web/css/materialize.min.css"  media="screen,projection"/>
	  <link type="text/css" rel="stylesheet" href="<?php echo $site; ?>/web/css/popcms.css">
	  
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo $site; ?>/web/js/popcms.js"></script>
      <script type="text/javascript" src="<?php echo $site; ?>/web/js/materialize.min.js"></script>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	  <style>
		.nav-logo {
			background: url('http://www.habbcrazy.net/resources/fonts/118/<?php echo $sitename; ?>.gif') no-repeat;
			width: 103px;
			height: 38px;
			margin-top: 9px;
		}
		body {
	background: url('http://static.habbo-happy.net/img/articles/76ba12_izquierda.png') no-repeat 0% 100%, url('<?php echo $site; ?>/web/images/bgs.png');
    background-attachment: fixed;
	/*font-family: sans-serif;*/
	font-size: 11px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
}
@media only screen and (min-width: 993px){
	.container {
		width: 835px !important;
		    max-width: 1020px !important;
	}
}
.box-blue {
/* ANTIGUO STYLE
	clear: both;
    min-height: 50px;
    padding: 10px;
    color: rgba(0,0,0,.8);
    font-size: 17px;
    line-height: 30px;
    font-weight: 500;
    border-bottom: 1px solid #eee; */
	
	padding: 3px;
	margin-bottom: 4px;
    border-radius: 2px;
	opacity: 0.7;
    color: #fff;
    font-weight: bold !important;
    font-size: 14px;
    text-align: center;
    background: #2767a7/* ROJO #a40429*/;
}
		.card {
	position: relative;
    margin: 0.5rem 0 1rem 0;
    border: 1px solid #ccc;
	border-bottom: 3px solid #ccc;
    padding: 10px 10px 10px 10px;
    background: #fff;
    border-radius: 5px !important;
    box-shadow: none;
    margin-bottom: 10px;
}

.card .card-content {
	padding: 0px 10px !important;
}
	  </style>
	</head>
	
    <body>
		<!-- {{nombre_usuario}} -->
		<ul id="profilename" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/me">Home</a></li>
		  <li><a href="<?php echo $site; ?>/perfil/<?php echo $user_q['username']; ?>">Mi perfil</a></li>
		  <li><a href="<?php echo $site; ?>/mensajes">Mensajes</a></li>
		  <div class="divider"></div>
		  <li><a href="<?php echo $site; ?>/ajustes">Ajustes</a></li>
		  <li><a href="<?php echo $site; ?>/logout">Salir</a></li>
		</ul>
		<ul id="profilename_phone" class="dropdown-content">
		  <li><a href="<?php echo $site; ?>/me">Home</a></li>
		  <li><a href="<?php echo $site; ?>/perfil/<?php echo $user_q['username']; ?>">Mi perfil</a></li>
		  <li><a href="<?php echo $site; ?>/mensajes">Mensajes</a></li>
		  <div class="divider"></div>
		  <li><a href="<?php echo $site; ?>/ajustes">Ajustes</a></li>
		  <li><a href="<?php echo $site; ?>/logout">Salir</a></li>
		</ul>
		<!-- {{shop}} -->
		<ul id="shop" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/tienda/placas">Placas</a></li>
		  <li><a href="<?php echo $site; ?>/tienda/raros">Rares</a></li>
		</ul>
		<ul id="shop_phone" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/tienda/placas">Placas</a></li>
		  <li><a href="<?php echo $site; ?>/tienda/raros">Rares</a></li>
		</ul>
		<!-- {{ Comunidad }} -->
		<ul id="community" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/info">Información</a></li>
		  <li><a href="<?php echo $site; ?>/noticias">Notícias</a></li>
		</ul>
		<ul id="community_phone" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/info">Información</a></li>
		  <li><a href="<?php echo $site; ?>/noticias">Notícias</a></li>
		</ul>
		<!-- {{team}} -->
		<ul id="teamhabbo" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/equipo">Staff</a></li>
		  <li><a href="<?php echo $site; ?>/alfa">Alfa</a></li>
		</ul>
		<ul id="teamhabbo_phone" class="dropdown-content" style="top: 59px;">
		  <li><a href="<?php echo $site; ?>/equipo">Staff</a></li>
		  <li><a href="<?php echo $site; ?>/alfa">Alfa</a></li>
		</ul>	
		<div class="header">
		  <div class="container">
		  <img style="margin-top: 30px;position: absolute;" src="http://www.habbcrazy.net/resources/fonts/118/<?php echo $sitename; ?>.gif">
		  <p style="position: absolute;margin-top: 72px;color: #fff;background: rgba(0, 0, 0, 0.48);border-radius: 4px;padding: 1px 7px;"><x style="    height: 30px;    width: 20px;    background: url(<?php echo $site; ?>/web/images/myhabbo_icons.png) no-repeat 0% 76.2%;    border: none;    padding-left: 19px;" /> <?php echo $users_online; ?> usuarios conectados</p>
		  <p style="position: absolute;margin-top: 98px;color: #fff;background: rgba(0, 0, 0, 0.48);border-radius: 4px;padding: 1px 7px;"><x style="    height: 30px;    width: 20px;    background: url(<?php echo $site; ?>/web/images/info_icons.png) no-repeat 0% 77.1%;    border: none;    padding-left: 19px;" /> <?php echo $rooms_active; ?> salas abiertas</p>
			<?php if ($hotel_q['status_client'] == 'on') { ?>
		  <a href="<?php echo $site; ?>/hotel" style="float: right;    margin-top: 50px;    height: 77px;    width: 200px;    line-height: 20px;    font-size: 17px;    font-weight: bold;    padding: 17px;" class="waves-effect waves-light btn"> Entrar en<br> <?php echo $sitename; ?> Hotel </a>				
		    <?php } ?>
			<?php if ($hotel_q['status_client'] == 'off') { ?>
		  <a href="<?php echo $site; ?>/#<?php echo $sitename; ?>-off" style="background: #af4c4c;float: right;    margin-top: 50px;    height: 77px;    width: 200px;    line-height: 20px;    font-size: 17px;    font-weight: bold;    padding: 17px;" class="waves-effect waves-light btn"> Entrar en<br> <?php echo $sitename; ?> Hotel </a>				
		    <?php } ?>
		  </div>
		</div>
		<style>
		.header {
			background: url('<?php echo $site; ?>/web/images/header.png') no-repeat 70% 0%, url('http://i.imgur.com/4p4P80m.png') 0% 40%;
			width: 100%;
			height: 170px;
		}
		</style>
	  <nav style="    margin-bottom: 10px;
    position: relative;
    font-size: 17px;
    padding: 0px 10px;
	box-shadow: 0px 3px 5px rgba(0, 0, 0, 0.26), 0px -3px 5px rgba(0, 0, 0, 0.26);
    ">
		<div class="nav-wrapper">
		<div class="container">
		  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		  <ul class="right hide-on-med-and-down">
		    <li><a style="font-size: 17px;" class="dropdown-button" href="#" data-activates="profilename"><?php echo $user_q['username']; ?></a></li>
			<li><a style="font-size: 17px;" class="dropdown-button" href="#" data-activates="community">Comunidad</a></li>
			<li><a style="font-size: 17px;" class="dropdown-button" href="#" data-activates="shop">Tienda</a></li>
			<li><a style="font-size: 17px;" class="dropdown-button" href="#" data-activates="teamhabbo">Equipo</a></li>
			<?php if($user_q['rank'] >= ''. $minrank .'') { ?><li style="background: #F44336;"><a style="color: #fff;font-size: 17px;" href="<?php echo $hk; ?>">Administraci&oacute;n</a></li><?php } ?>
		  </ul>
		 
		  <ul class="side-nav" id="mobile-demo">
		    <li><a style="font-size: 17px;" class="dropdown-button" href="#" data-activates="profilename_phone"><?php echo $user_q['username']; ?></a></li>
			<li><a style="font-size: 17px;" class="dropdown-button" href="#" data-activates="community_phone">Comunidad</a></li>
			<li><a style="font-size: 17px;" class="dropdown-button" href="<?php echo $site; ?>" data-activates="shop_phone">Tienda</a></li>
			<li><a style="font-size: 17px;" class="dropdown-button" href="<?php echo $site; ?>" data-activates="teamhabbo_phone">Equipo</a></li>
			<?php if($user_q['rank'] >= ''. $minrank .'') { ?><li style="border-left: 1px solid #607D8B;"><a style="font-size: 17px;" href="<?php echo $hk; ?>">Administraci&oacute;n</a></li><?php } ?>
		  </ul>
		</div>
		</div>
	  </nav>
	  
	  
