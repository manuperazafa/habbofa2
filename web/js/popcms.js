$( document ).ready(function(){
	$(".button-collapse").sideNav();
	$(".dropdown-button").dropdown();
	$('.modal-trigger').leanModal();
	$('.slider').slider('start');
	$('.materialboxed').materialbox();
	$('.carousel').carousel();
	$('.carousel.carousel-slider').carousel({full_width: true});
})