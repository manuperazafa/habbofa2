<?php
require_once 'heliocms/core.php'; 
if (isset($_POST['forgotten_submit'])) {
$email_forgotten = $_POST['emailAddress'];
if (empty($email_forgotten)) {
$message = '<div class="redmsg #d50000 red accent-4">No dejes campos vacios</div>';
}else{
if (!preg_match("/^[A-Z0-9._-]{2,}+@[A-Z0-9._-]{2,}\.[A-Z0-9._-]{2,}$/i", $email_forgotten)) {
$message = '<div class="redmsg #d50000 red accent-4">Introduce un correo electrónico válido</div>';
}else{
mysql_query("INSERT INTO heliocms_forgotten (email) VALUES ('$email_forgotten')");
$message = '<div class="redmsg #d50000 red accent-4">En breve te enviaremos un mensaje al correo <b>'.$email_forgotten.'</b> para recuperar tu contrseña.</div>';
header( "refresh:2; url=". $site ."/index.php" );
}}}
?>
<html>
  <head>
	<title><?php echo $sitename; ?> Hotel</title>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
	<script src="<?php echo $site; ?>/web/js/v3_landing_bottom.js" type="text/javascript"></script>
	<script src="<?php echo $site; ?>/web/js/popcms.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript" src="<?php echo $site; ?>/web/js/registration.js?v=1.9" ></script>
	
	<style>
	body {
		margin: 0;
		background: url('<?php echo $site; ?>/web/images/index/bg.png');
	}
	
	.nav-logo {
		background: url('http://www.habbcrazy.net/resources/fonts/118/<?php echo $sitename; ?>.gif') no-repeat;
		width: 300px;
		height: 38px;
		margin-top: 9px;
		transition-duration: 0.4s;
	}
		
	.nav-logo:hover {
		opacity: 0.6;
		transition-duration: 0.4s;
	}
	
	.bubble {
	    background: white;
		height: 36px;
		width: 122px;
		color: #969696;
		border-radius: 4px;
		font-family: Roboto;
		line-height: 36px;
		text-align: center;
		margin-top: 10px;
		font-size: 16px;
	}
	
	.bubble .arrow {
		border-style: solid;
		border-color: transparent white transparent transparent;
		border-width: 10px;
		margin-left: -20px;
		margin-top: 7px;
		float: left;
	}
	
	#newsSliderBox > .newsContainer > div > div > .newsTitle {
		color: white;
		font-family: Roboto;
		font-weight: bold;
		font-size: 26px;
		line-height: 470px;
	}
	
	input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 90% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
	
	.redmsg {
		width: 100%;
		background: red;
		padding: 10px 0px;
		text-align: center;
		color: #fff;
	}
	
	.mercury_btn {
		background: url('<?php echo $site; ?>/web/images/contentHeaderBg.png') #1ea701 0%;
		margin-bottom: 10px;
		width: 97%;
	}
	
	.lost_password {
		float: right;
		margin-top: -10px;
		color: #cbcbcb;
		text-shadow: 0px 1px 0px #fff;
		margin-right: 7px;
	}
	
	.home-creditos {
		background: #23231f;
		border: 1px solid #4d5349;
		border-radius: 4px;
		padding: 2px 6px;
		width: 100%;
		height: 28px;
	}
	
	.left-box {
		color: #d0ae49;
		font-weight: bold;
		float: left;
	}
	
	.center-box {
		font-weight: bold;
		float: right;
		margin-right: 11px;
	}
	
	.right-box {
		float: right;
		background: url(<?php echo $site; ?>/web/images/icon_credits.png) no-repeat 50%;
		width: 29px;
		height: 29px;
		margin-top: -3px;
		margin-right: -13px;
		background-color: #aa7f0f;
		border: 2px solid #c8af4f;
		border-radius: 3px;
	}
	
	.mercury_register {
		background: linear-gradient(180deg, #23890a 50%, #197008 50%);
		border: 3px solid #197008;
		width: 100%;
		font-weight: bold;
		text-shadow: 0px -1px 0px #000;
		line-height: 45px;
		height: 50px;
		transition: 1s;
	}
	
	.mercury_register:hover {
		background: linear-gradient(180deg, #23890a 50%, #1d7b0a 50%);
		border: 3px solid #1d7b0a;
		transition: 1s;
	}
	</style>
	<script>
	$( document ).ready(function(){
		$(".button-collapse").sideNav();
		$(".dropdown-button").dropdown();
		$('.modal-trigger').leanModal();
	})
	</script>
  </head>
	
	<body>
	<?php echo $message; ?>
	<br><br>
		<div class="container">	
			<div class="row">
				<div class="col s12 m12">
					<div class="card-panel" style="border-radius: 5px;padding: 10px 0px;border: 2px solid #e28701;background: #FF9800;text-align: center;">
					  <span class="white-text text-darken-2"><b>Panel de soporte al usuario</b></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">	
			<div class="row">
				<div class="col s12 m12">
					<div class="card-panel" style="color: rgba(255, 255, 255, 0.86);background: url(<?php echo $site; ?>/web/images/view_generic.png) 50% 45%;box-shadow: none;">
					 <br>
					 
					 <div class="row" style="margin-top: -15px;">
					<div class="input-field col s12 m2"></div>
					 <div class="input-field col s12 m8">
						<div class="card-panel" style="padding: 15px 17px;border-radius: 5px;box-shadow: 0px 0px 2px #c7c9c8;background: #eee;">
						  <span class="text-darken-2" style="text-shadow: 0px 1px 0px #fff;color: #939393;font-weight: 300;font-size: 21px;">He olvidado mi contraseña ..</span>
						  <hr style="background: rgba(147, 146, 146, 0.59);height: 1px;border: none;border-bottom: 1px solid rgba(255, 255, 255, 0.92);">
						  <p style="color: #939393;text-shadow: 0px 1px 0px #fff;">Debes introducir el correo con el usuario que te registraste.</p>
						  <form name="claimPasswordForm" method="post">
							<center>
							<input name="emailAddress" type="email" placeholder="Introduce tu correo electrónico">
							<button type="submit" name="forgotten_submit" class="waves-effect waves-light btn mercury_register" style="width: 90%;" href="#!">SOLICITAR CONTRASEÑA</button>
							</center>
						  </form>
						</div>
						</div>
					<div class="input-field col s12 m2"></div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>