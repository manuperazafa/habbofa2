<?php
require_once ('heliocms/core.php');
require_once ('heliocms/session.php');
require_once ('web/templates/header.php');
if ($_GET['save'] == "$w") {
$message = '<div class="msg"><b>EXITO!</b> has actualizado tu perfil correctamente</div>';
}
$profilevisible_a = mysql_query("SELECT * FROM heliocms_profilesettings WHERE email='$user_q[mail]'");
$profilevisible_q = mysql_fetch_assoc($profilevisible_a);
if (isset($_POST['save'])) {
$block_newfriends = $_POST['block_newfriends'];
$hide_online = $_POST['hide_online'];
$profile_visible = $_POST['profile_visible'];
$user_radio = $_POST['user_radio'];
$motto = $_POST['motto'];
mysql_query ("UPDATE users SET block_newfriends='$block_newfriends', hide_online='$hide_online', profile_visible='$profile_visible', user_radio='$user_radio', motto='$motto' WHERE id='$user_q[id]'");
header ("Location: $site/ajustes/perfil?saved=$w");
}
?>
<style>
.msg {
	color: #fff;
	background: green;
	padding: 5px;
	width: 100%;
}

input {
	background-color: transparent !important; 
    border: 1px solid #ccc !important;
    border-radius: 0;
	height: 20px !important;
    outline: none;
    width: 50% !important;
}

input:focus {border-bottom: none 1px solid #ccc;box-shadow: none !important;}

[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:after {background-color: #424242 !important;}
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:before, [type="radio"].with-gap:checked+label:after {border: 2px solid #424242 !important;}
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {padding-left: 25px !important;}

#slideselector {
    position: absolue;
    top:0;
    left:0;
    border: 2px solid black;
    padding-top: 1px;
}
.slidebutton {
    height: 21px;
    margin: 2px;
}
#slideshow { 
    margin: 50px auto; 
    position: relative; 
    width: 240px; 
    height: 240px; 
    padding: 10px; 
    box-shadow: 0 0 20px rgba(0,0,0,0.4); 
}

#slideshow > div { 
    position: absolute; 
    top: 10px; 
    left: 10px; 
    right: 10px; 
    bottom: 10px;
    overflow:hidden;
}

.imgLike {
    width:100%;
    height:100%;
}
/* Radio */

input[type="radio"] {
    background-color: #ddd;
    background-image: -webkit-linear-gradient(0deg, transparent 20%, hsla(0,0%,100%,.7), transparent 80%),
                      -webkit-linear-gradient(90deg, transparent 20%, hsla(0,0%,100%,.7), transparent 80%);
    border-radius: 10px;
    box-shadow: inset 0 1px 1px hsla(0,0%,100%,.8),
                0 0 0 1px hsla(0,0%,0%,.6),
                0 2px 3px hsla(0,0%,0%,.6),
                0 4px 3px hsla(0,0%,0%,.4),
                0 6px 6px hsla(0,0%,0%,.2),
                0 10px 6px hsla(0,0%,0%,.2);
    cursor: pointer;
    display: inline-block;
    height: 15px;
    margin-right: 15px;
    position: relative;
    width: 15px;
    -webkit-appearance: none;
}
input[type="radio"]:after {
    background-color: #444;
    border-radius: 25px;
    box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.4),
                0 1px 1px hsla(0,0%,100%,.8);
    content: '';
    display: block;
    height: 7px;
    left: 4px;
    position: relative;
    top: 4px;
    width: 7px;
}
input[type="radio"]:checked:after {
    background-color: #f66;
    box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.4),
                inset 0 2px 2px hsla(0,0%,100%,.4),
                0 1px 1px hsla(0,0%,100%,.8),
                0 0 2px 2px hsla(0,70%,70%,.4);
}

.mercuryzero_btn {
	background: #fff;
    border: 3px solid #000;
    border-bottom: 5px solid #000;
    border-radius: 4px;
    font-weight: 600;
    padding: 2px 13px;
}
</style>
<div class="container">
		<div class="row">
			<div class="col s12 m3">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Ajustes</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <b><a href="<?php echo $site; ?>/ajustes/perfil" style="color: #000;">Mi perfil</a><br></b>
				  <a href="<?php echo $site; ?>/ajustes/correo" style="color: #000;">Email y verificaci&oacute;n</a><br>
				  <a href="<?php echo $site; ?>/ajustes/contrasena" style="color: #000;">Mi contrase&ntilde;a</a><br>
				</div>
			  </div>
			</div>
			
			<div class="col s12 m9">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Cambiar tu perfil</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				 <?php echo $message; ?>
				 <form method="post">
				  <p>
				    <b>Tu estado</b><br>
				    Tu estado podr&aacute; ser visto por todos, pi&eacute;nsalo bien!<br>
				    Estado: <input name="motto" value="<?php echo $user_q['motto']; ?>">
				  </p>
				  <p>
				    <b>Peticiones de amigos</b><br>
					Elige si est&aacute;n activas tus peticiones de amistad:<br>
					<input type="radio" id="friendRequestEnabled" name="block_newfriends" class="regular-radio" value="0" <?php if ($user_q['block_newfriends'] == '0') { ?>checked="checked"<?php } ?>/><label for="friendRequestEnabled">Si</label>
					<input type="radio" id="friendRequestEnabled2" name="block_newfriends" class="regular-radio" value="1" <?php if ($user_q['block_newfriends'] == '1') { ?>checked="checked"<?php } ?>/><label for="friendRequestEnabled2">No</label>
					<br><br>
				  </p>
				  <p>
					<b>Online</b><br>
					Qui&eacute;n puede ver tu estado online:<br>
					<input type="radio" id="onlineStatusVisible" name="hide_online" class="regular-radio" value="0" <?php if ($user_q['hide_online'] == '0') { ?>checked="checked"<?php } ?>/><label for="onlineStatusVisible">Todos</label>
					<input type="radio" id="onlineStatusVisible2" name="hide_online" class="regular-radio" value="1" <?php if ($user_q['hide_online'] == '1') { ?>checked="checked"<?php } ?>/><label for="onlineStatusVisible2">Nadie</label>
					<br><br>
				  </p>
				  <p>
					<b>Visibilidad de perfil</b><br>
					Qui&eacute;n puede ver tu perfil d&oacute;nde sale toda tu informaci&oacute;n:<br>
					<input type="radio" id="profileVisible2" name="profile_visible" class="regular-radio" value="1" <?php if ($user_q['profile_visible'] == '1') { ?>checked="checked"<?php } ?>/><label for="profileVisible2">Todos</label>
					<input type="radio" id="profileVisible" name="profile_visible" class="regular-radio" value="0" <?php if ($user_q['profile_visible'] == '0') { ?>checked="checked"<?php } ?>/><label for="profileVisible">Nadie</label>
					<br><br>
				  </p>
				  <p>
					<b>Radio</b><br>
					Elige si la radio de <?php echo $sitename; ?> est&aacute; activa:<br>
					<input type="radio" id="radioHotel" name="user_radio" class="regular-radio" value="1" <?php if ($user_q['user_radio'] == '1') { ?>checked="checked"<?php } ?>/><label for="radioHotel">Si</label>
					<input type="radio" id="radioHotel2" name="user_radio" class="regular-radio" value="0" <?php if ($user_q['user_radio'] == '0') { ?>checked="checked"<?php } ?>/><label for="radioHotel2">No</label>
					<br><br>
				  </p>
				  
				  <button type="submit" class="mercuryzero_btn" name="save">Salvar cambios</button>
				 </form>
				</div>
			  </div>
			</div>
		</div>
</div>
<?php require_once 'web/templates/footer.php'; ?>