<?php
require_once ('heliocms/core.php');
require_once ('heliocms/session.php');
require_once ('web/templates/header.php');
if ($_GET['save'] == "$w") {
$message = '<div class="msg"><b>EXITO!</b> has actualizado tu perfil correctamente</div>';
}
if (isset($_POST['save'])) {
$password = $_POST['passwordNew'];
$password_repeat = $_POST['passwordNewRepeated'];
$passwordcurrent = $_POST['passwordCurrent'];
$password_verify = mysql_query("SELECT * FROM users WHERE mail='$user_q[mail]' AND password='".MD5($passwordcurrent)."'");
if (empty($password)) {
$error = '1';
$passwordNew_errors = '<div class="box-rojo">Este campo es obligatorio.</div>';
$passwordnew_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}else{
if(strlen($password) < 6) {
$error = '1';
$passwordNew_errors = '<div class="box-rojo">Seguridad de la contrase&ntilde;a: <b>baja</b>.<br>Tu contrase&ntilde;a debe tener como m&iacute;nimo 6 car&aacute;cteres.</div>';
$passwordnew_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
}else{
if (empty($password_repeat)) {
$error = '1';
$passwordNewRepeat_errors = '<div class="box-rojo">Este campo es obligatorio.</div>';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}else{
if ($password <> $password_repeat) {
$error = '1';
$passwordNewRepeat_errors = '<div class="box-rojo">Las contrase&ntilde;as no coinciden.</div>';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}}}}
if (empty($passwordcurrent)) {
$error = '1';
$passwordcurrent_errors = '<div class="box-rojo">Este campo es obligatorio.</div>';
$passwordcurrent_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
}else{
if (mysql_num_rows($password_verify) == 0) {
$error = '1';
$passwordcurrent_errors = '<div class="box-rojo">Tu conectar&aacute;a no es correcta, ¡inténtalo de nuevo!</div>';
$passwordcurrent_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}}
if ($error <> 1) {
mysql_query ("UPDATE users SET password='".MD5($password)."' WHERE mail='$user_q[mail]'");
header ("Location: $site/ajustes/contrasena?save=$w");
}}
?>
<style>
.msg {
	color: #fff;
	background: green;
	padding: 5px;
	width: 100%;
}

input {
	background-color: transparent !important; 
    border: 1px solid #ccc !important;
    border-radius: 0;
	height: 20px !important;
    outline: none;
    width: 50% !important;
}

input:focus {border-bottom: none 1px solid #ccc;box-shadow: none !important;}

[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:after {background-color: #424242 !important;}
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:before, [type="radio"].with-gap:checked+label:after {border: 2px solid #424242 !important;}
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {padding-left: 25px !important;}

#slideselector {
    position: absolue;
    top:0;
    left:0;
    border: 2px solid black;
    padding-top: 1px;
}
.slidebutton {
    height: 21px;
    margin: 2px;
}
#slideshow { 
    margin: 50px auto; 
    position: relative; 
    width: 240px; 
    height: 240px; 
    padding: 10px; 
    box-shadow: 0 0 20px rgba(0,0,0,0.4); 
}

#slideshow > div { 
    position: absolute; 
    top: 10px; 
    left: 10px; 
    right: 10px; 
    bottom: 10px;
    overflow:hidden;
}

.imgLike {
    width:100%;
    height:100%;
}
/* Radio */

input[type="radio"] {
    background-color: #ddd;
    background-image: -webkit-linear-gradient(0deg, transparent 20%, hsla(0,0%,100%,.7), transparent 80%),
                      -webkit-linear-gradient(90deg, transparent 20%, hsla(0,0%,100%,.7), transparent 80%);
    border-radius: 10px;
    box-shadow: inset 0 1px 1px hsla(0,0%,100%,.8),
                0 0 0 1px hsla(0,0%,0%,.6),
                0 2px 3px hsla(0,0%,0%,.6),
                0 4px 3px hsla(0,0%,0%,.4),
                0 6px 6px hsla(0,0%,0%,.2),
                0 10px 6px hsla(0,0%,0%,.2);
    cursor: pointer;
    display: inline-block;
    height: 15px;
    margin-right: 15px;
    position: relative;
    width: 15px;
    -webkit-appearance: none;
}
input[type="radio"]:after {
    background-color: #444;
    border-radius: 25px;
    box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.4),
                0 1px 1px hsla(0,0%,100%,.8);
    content: '';
    display: block;
    height: 7px;
    left: 4px;
    position: relative;
    top: 4px;
    width: 7px;
}
input[type="radio"]:checked:after {
    background-color: #f66;
    box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.4),
                inset 0 2px 2px hsla(0,0%,100%,.4),
                0 1px 1px hsla(0,0%,100%,.8),
                0 0 2px 2px hsla(0,70%,70%,.4);
}

.mercuryzero_btn {
	background: #fff;
    border: 3px solid #000;
    border-bottom: 5px solid #000;
    border-radius: 4px;
    font-weight: 600;
    padding: 2px 13px;
}

.box-rojo {
	width: 60%;
	background: rgba(255, 0, 0, 0.64);
	color: #fff;
	border-radius: 3px;
	text-align: center;
}
</style>
<div class="container">
		<div class="row">
			<div class="col s12 m3">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Ajustes</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				  <a href="<?php echo $site; ?>/ajustes/perfil" style="color: #000;">Mi perfil</a><br>
				  <a href="<?php echo $site; ?>/ajustes/correo" style="color: #000;">Email y verificaci&oacute;n</a><br>
				  <b><a href="<?php echo $site; ?>/ajustes/contrasena" style="color: #000;">Mi contrase&ntilde;a</a><br></b>
				</div>
			  </div>
			</div>
			
			<div class="col s12 m9">
			  <div class="card blue-white darken-1" style="border-radius: 5px;">
			  <div class="box-blue" style="background: #424242;">
				<div class="title">Contrase&ntilde;a</div>
			  </div>
				<div class="card-content black-text" style="padding: 0px 20px 2px 20px;">
				 <?php echo $message; ?>
				 <form name="emailChangeForm" method="post">
				  <p>
				    <b>Contrase&ntilde;a actual</b><br>
					<?php echo $passwordcurrent_errors; ?>
				    Contrase&ntilde;a: <input type="password" name="passwordCurrent" value="<?php echo $passwordcurrent; ?>">
				  </p>
				  <p>
				    <b>Nueva contrase&ntilde;a</b><br>
					<?php echo $passwordNew_errors; ?>
				    Con esta contrase&ntilde;a te conectar&aacute;s a <?php echo $sitename; ?> Hotel<br>
				    Contrase&ntilde;a: <input type="password" name="passwordNew" value="<?php echo $password; ?>">
				  </p>
				  <p>
				    <b>Repite de nuevo la contrase&ntilde;a</b><br>
					<?php echo $passwordNewRepeat_errors; ?>
				    Por metodos de seguridad pedimos que introduzcas tu contrase&ntilde;a<br>
				    Contrase&ntilde;a: <input type="password" name="passwordNewRepeated" value="<?php echo $password_repeat; ?>">
				  </p>
				  
				  <button type="submit" class="mercuryzero_btn" name="save">Salvar cambios</button>
				 </form>
				</div>
			  </div>
			</div>
		</div>
</div>
<?php require_once 'web/templates/footer.php'; ?>