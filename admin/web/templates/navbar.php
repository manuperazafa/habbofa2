<?php
if (!isset($_SESSION['id2'])) {
header ("Location: index.php");
}
?>
<html>
<head>
<title><?php echo $sitename; ?></title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="<?php echo $site; ?>/web/css/materialize.min.css"  media="screen,projection"/>
	  <link type="text/css" rel="stylesheet" href="<?php echo $site; ?>/web/css/mercuryzero-hk.css">
	  
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo $site; ?>/web/js/popcms.js"></script>
	  <script src="<?php echo $hk; ?>/ckeditor/ckeditor.js"></script>
      <script type="text/javascript" src="<?php echo $site; ?>/web/js/materialize.min.js"></script>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	  <style>
		.<?php echo $sitename; ?>-navbar {
			color: #fff;
		}
		.<?php echo $sitename; ?>-navbar .color {
			color: #fff;
			font-weight: lighter;
			margin-top: 5px;
		}
		.<?php echo $sitename; ?>-navbar .status-offline {
			background: #ab433f;
			width: 10px;
			height: 10px;
			border-radius: 60px;
			float: left;
			margin-right: 5px;
			margin-top: 9px;
		}
		.<?php echo $sitename; ?>-navbar .status-online {
			background: #50ab3f;
			width: 10px;
			height: 10px;
			border-radius: 60px;
			float: left;
			margin-right: 5px;
			margin-top: 9px;
		}
		.side-nav li.active {
			background: #1e282c !important;
		}
		
		.side-nav .collapsible-body li a, .side-nav.fixed .collapsible-body li a {
			background: #2c3b41;
		}
		.side-nav .collapsible-body li a:hover, .side-nav.fixed .collapsible-body li a:hover {
			color: #fff;
		}
		
		.side-nav.fixed {
			background: #222d32;
		}
	  </style>
	  </head>
	  <body>
	   <nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo" style="font-weight: lighter;"><b><?php echo $sitename; ?></b> Staff</a>
      <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">more_vert</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a><img style="margin-top: 17px;margin-left: -35px;position: absolute;" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&direction=3&size=s&head_direction=3&headonly=1"><?php echo $user_q['username']; ?></a></li>
      </ul>
	  <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li style="visibility: hidden;">ooooooooooooooo</li>
        <li style="visibility: hidden;">oooooooooooioo</li>
        <li style="visibility: hidden;">oooooooooo</li>
        <li style="line-height: 54px;"><b style="font-weight: 400;font-size: 25px;">MercuryCMS</b> <font style="font-weight: lighter;">Version 1.0</font></li>
      </ul>
    </div>
  </nav>
  
  
	   <ul id="slide-out" class="side-nav fixed">
      <li style="height: 64px;background: #327eab;text-align: center;font-size: 28px;line-height: 58px;font-weight: lighter;margin-left: -19px;">
		<div class="<?php echo $sitename; ?>-navbar"><b><?php echo $sitename; ?></b> Staff</div>
	  </li>
      <li class="<?php echo $sitename; ?>-navbar" style=";height: 90px;background: #222d32;font-size: 15px;line-height: 58px;">
		<table>
			<tbody>
				<tr>
					<td style="float: left;"><img style="" src="<?php echo $avatarimage; ?>/habbo-imaging/avatarimage?figure=<?php echo $user_q['look']; ?>&direction=3&head_direction=3&headonly=1"></td>
					<td style="float: left;margin-left: 10px;width: 150px;"><b style="color: #fff;line-height: 25px;"><?php echo $user_q['username']; ?></b><br>
					<?php if ($user_q['online'] == '0') { ?><div class="status-offline"></div><div class="color">Offline</div><?php } ?>
					<?php if ($user_q['online'] == '1') { ?><div class="status-online"></div><div class="color">Online</div><?php } ?></td>
				</tr>
			</tbody>
		</table>
	  </li>
      <li style="background: #1a2226;color: #4a6470;padding-left: 15px;">MEN&Uacute; PRINCIPAL</li>
	  <li><a class="waves-effect" href="home.php"><i style="margin: 0 10px 0 0;color: #7fa0af;" class="left material-icons">home</i> Home</a></li>
	  
	  <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i style="margin: 0 10px 0 0;color: #7fa0af;" class="left material-icons">computer</i>Admin<i style="color: #7fa0af;" class="right material-icons">keyboard_arrow_left</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="config-hotel.php">Configuración <?php echo $sitename; ?></a></li>
                <li><a href="config-new-important.php?user=1">Notícias importantes</a></li>
				<li><a href="config-ranks.php">Administrar rangos</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
	  
	  <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i style="margin: 0 10px 0 0;color: #7fa0af;" class="left material-icons">folder</i>Archivador<i style="color: #7fa0af;" class="right material-icons">keyboard_arrow_left</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="config-comands.php">Comandos staff</a></li>
                <li><a href="config-logs.php">Movimientos</a></li>
                <li><a href="config-badges.php">Subir placas</a></li>
				<li><a href="config-catalogue.php">Catálogo</a></li>
                <li><a href="config-filter.php">Filtros</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li
	  
	  <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i style="margin: 0 10px 0 0;color: #7fa0af;" class="left material-icons">information</i>Diverso<i style="color: #7fa0af;" class="right material-icons">keyboard_arrow_left</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="config-shop-badges.php">Tienda de placas</a></li>
                <li><a href="config-shop-rares.php">Tienda de rares</a></li>
				<li><a href="config-news.php">Noticias</a></li>
				<li><a href="config-give-rank.php">Dar rango</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
	  
	  <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header"><i style="margin: 0 10px 0 0;color: #7fa0af;" class="left material-icons">warning</i>Moderación<i style="color: #7fa0af;" class="right material-icons">keyboard_arrow_left</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="config-fpass.php">Contraseña perdidas</a></li>
                <li><a href="config-bans.php">Banear un usuario</a></li>
				<li><a href="config-users.php">Buscar usuario</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
	  
	  <li><a class="waves-effect #b71c1c red darken-4" style="color: #fff;" href="<?php echo $site; ?>"><i style="margin: 0 10px 0 0;color: #fff;" class="left material-icons">highlight_off</i> Cerrar sesión</a></li>
	  
    </ul>
          