<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Movimientos</h5>
				<table class="centered striped">
				<thead>
					<th>#</th>
					<th>Usuario</th>
					<th>Categoría</th>
					<th>Acción</th>
					<th>Rango</th>
					<?php if($user_q['rank'] >= ''. $maxrank .'') { ?><th>Opciones</th><?php } ?>
				</thead>
				<tbody>
<?php
       if (isset($_POST['delete_'.$rangos['id'].''])) {
											mysql_query("DELETE FROM stafflogs WHERE id=$rangos[id]");
											header ("Location: config-logs.php?deleted=$w");
											}
        $stafflogs = mysql_query("SELECT * FROM stafflogs ORDER BY id DESC LIMIT 6");
        while($logs = mysql_fetch_assoc($stafflogs)) {
            $user = mysql_query("SELECT id, username FROM users WHERE id ='". $logs['userid'] ."'");
            $id = mysql_fetch_assoc($user);
            $rango = mysql_query("SELECT * FROM ranks WHERE id ='". $logs['note'] ."'");
            $rank = mysql_fetch_assoc($rango);
?> 											
											 
				  <tr>
					<td style="font-size: 12px;"><b><?php echo $logs['id']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $id['username']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $logs['action']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $logs['message']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $rank['name']; ?></b></td>
					<?php if($user_q['rank'] >= ''. $maxrank .'') { ?><td style="font-size: 14px;"><form method="post"><button style="background: #b71c1c;box-shadow: none;" name="delete_<?php echo $rangos['id']; ?>" class="btn btn-xs btn-danger">
																<i class="material-icons">delete</i>
					</button></form></td><?php } ?>
				  </tr>
				<?php } ?>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>