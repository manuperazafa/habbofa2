<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}
</style>
<div class="container">
 <div class="row">
	<div class="col s12 m4">
		<div class="card blue-white darken-1">
            <div class="card-content black-text" style="padding: 0px !important;">
              <p>
				<table>
				  <tbody>
					<tr>
						<td style="padding: 0px;float: left;"><div class="box-info blue"><i class="medium material-icons">supervisor_account</i></div></td>
						<td style="float: left;margin-top: -11px;margin-left: 6px;font-weight: 300;"><font style="font-size: 22px;">Usuarios</font><br><b><?php $users = mysql_query("SELECT * from users"); echo mysql_num_rows($users); ?></b></td>
					</tr>
				  </tbody>
				</table>
			  </p>
            </div>
        </div>
	</div>
	
	<div class="col s12 m4">
		<div class="card blue-white darken-1">
            <div class="card-content black-text" style="padding: 0px !important;">
              <p>
				<table>
				  <tbody>
					<tr>
						<td style="padding: 0px;float: left;"><div class="box-info red"><i class="medium material-icons">block</i></div></td>
						<td style="float: left;margin-top: -11px;margin-left: 6px;font-weight: 300;"><font style="font-size: 22px;">Baneos</font><br><b><?php $users = mysql_query("SELECT * from bans"); echo mysql_num_rows($users); ?></b></td>
					</tr>
				  </tbody>
				</table>
			  </p>
            </div>
        </div>
	</div>
	
	<div class="col s12 m4">
		<div class="card blue-white darken-1">
            <div class="card-content black-text" style="padding: 0px !important;">
              <p>
				<table>
				  <tbody>
					<tr>
						<td style="padding: 0px;float: left;"><div class="box-info orange"><i class="medium material-icons">store</i></div></td>
						<td style="float: left;margin-top: -11px;margin-left: 6px;font-weight: 300;"><font style="font-size: 22px;">Salas</font><br><b><?php $users = mysql_query("SELECT * from rooms"); echo mysql_num_rows($users); ?></b></td>
					</tr>
				  </tbody>
				</table>
			  </p>
            </div>
        </div>
	</div>
 </div>
 
  <div class="row">
	<div class="col s12 m8">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Últimos movimientos</h5>
				<table class="centered striped">
				<thead>
				  <tr>
					  <th data-field="id">#</th>
					  <th data-field="id">Usuario</th>
					  <th data-field="name">Acción</th>
					  <th data-field="price">Rango</th>
				  </tr>
				</thead>

				<tbody>
				 <?php
        if(isset($_POST['borrar']))
        {
            $borrarlogs = mysql_query("TRUNCATE TABLE stafflogs");
            if($borrarlogs) {
                echo '<!-- Logs borrados -->';
            } else {
                echo '<!-- Logs no borrados -->';
            }
        }
        $stafflogs = mysql_query("SELECT * FROM stafflogs ORDER BY id DESC LIMIT 6");
        while($logs = mysql_fetch_assoc($stafflogs)) {
            $user = mysql_query("SELECT id, username FROM users WHERE id ='". $logs['userid'] ."'");
            $id = mysql_fetch_assoc($user);
            $rango = mysql_query("SELECT * FROM ranks WHERE id ='". $logs['note'] ."'");
            $rank = mysql_fetch_assoc($rango);
?> 
				  <tr>
					<td><?php echo $logs['id']; ?></td>
					<td><?php echo $id['username']; ?></td>
					<td><?php echo $logs['message']; ?></td>
					<td><?php echo $rank['name']; ?></td>
				  </tr>
				<?php } ?>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
	
	<div class="col s12 m4">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				Bienvenido al panel staff de <?php echo $sitename; ?>!<br>
				<br>
				Por favor, usa esto con cuidado y bajo tu propio riesgo. Todas las acciones se registran y si te pillan haciendo algo que no deberías hacer. 
				Se tomarán medidas contra su cuenta.<br>
				<br>
				<b>Recuerda que toda la información dada en el panel no es que deba darse a cualquier persona sin importar que son un miembro del personal o no.
				El personal puede buscar por sí mismos!</b><br>
				<br>
				Si te encuentras con algún problema por favor deja un mensaje a un técnico!
            </div>
        </div>
	</div>
  </div>
</div>