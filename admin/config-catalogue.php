<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Catálogo <i>(Hay <?php $users = mysql_query("SELECT * from catalog_pages"); echo mysql_num_rows($users); ?> páginas y <?php $users = mysql_query("SELECT * from catalog_items"); echo mysql_num_rows($users); ?> furnis)</i></h5>
				<a style="background: #50ab3f;box-shadow: none;" class="btn" href="config-catalogue-add.php">Añadir página</a>
				<table class="centered striped">
				<thead>
					<th>#</th>
					<th>Página</th>
					<th>Rango</th>
					<th>Opciones</th>
				</thead>
				<tbody>
				<?php
											$users_a = mysql_query("SELECT * from catalog_pages");
											$i = 0; while($rangos = mysql_fetch_assoc($users_a)){
											$rango = mysql_query("SELECT * FROM ranks WHERE id ='". $rangos['min_rank'] ."'");
											$rank = mysql_fetch_assoc($rango);
											if (isset($_POST['delete_'.$rangos['id'].''])) {
											mysql_query("DELETE FROM catalog_pages WHERE id=$rangos[id]");
											header ("Location: config-catalogue.php?deleted=$w");
											}
											?>
											 
				  <tr>
					<td style="font-size: 12px;"><b><?php echo $rangos['id']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $rangos['caption']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $rank['name']; ?></b></td>
					<td style="font-size: 14px;"><form method="post"><a class="btn" style="background: #1e282c;box-shadow: none;margin-right: 3px;" href="config-catalogue-edit.php?user=<?php echo $rangos['id']; ?>"><i class="material-icons">create</i></a><button style="background: #b71c1c;box-shadow: none;" name="delete_<?php echo $rangos['id']; ?>" class="btn btn-xs btn-danger">
																<i class="material-icons">delete</i>
															</button></form></td>
				  </tr>
				<?php } ?>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>