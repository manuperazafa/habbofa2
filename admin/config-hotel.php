<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
  
    $client_a = mysql_query("SELECT * FROM heliocms_hotel");
	$client_q = mysql_fetch_assoc($client_a);
	if ($_GET['saved'] == $w) {
	$message = '<div class="message">Se ha guardado correctamente.</div>';
	}
	if (isset($_POST['save'])) {
	$host = $_POST['host'];
	$port = $_POST['port'];
	$external_variables = $_POST['external_variables'];
	$external_flash_texts = $_POST['external_flash_texts'];
	$external_flash_override_texts = $_POST['external_flash_override_texts'];
	$external_override_variables = $_POST['external_override_variables'];
	$furnidata = $_POST['furnidata'];
	$figuredata = $_POST['figuredata'];
	$base = $_POST['base'];
	$habbo_swf = $_POST['habbo_swf'];
	$sitename = $_POST['sitename'];
	$site = $_POST['site'];
	$productdata = $_POST['productdata'];
	$avatarimage = $_POST['avatarimage'];
	$swfimages = $_POST['swfimages'];
	$hk = $_POST['hk'];
	$minrank = $_POST['minrank'];
	$chatid = $_POST['chatid'];
	$status_client = $_POST['status_client'];
	mysql_query("UPDATE heliocms_hotel SET 
	host='$host', 
	port='$port', 
	external_variables='$external_variables', 
	external_flash_texts='$external_flash_texts', 
	external_flash_override_texts='$external_flash_override_texts', 
	external_override_variables='$external_override_variables', 
	furnidata='$furnidata', 
	figuredata='$figuredata', 
	base='$base', 
	habbo_swf='$habbo_swf', 
	sitename='$sitename', 
	site='$site', 
	productdata='$productdata', 
	avatarimage='$avatarimage', 
	swfimages='$swfimages', 
	hk='$hk', 
	minrank='$minrank', 
	chatid='$chatid', 
	status_client='$status_client'");
	header ("Location: config-hotel.php?saved=$w");
	mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Configuración ". $sitename ."', 'Ha cambiado los ajustes de ". $sitename ."', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
	}
?>
<style>
.message {
	    background: green;
    color: #fff;
    padding: 10px 0px;
    text-align: center;
    border-radius: 3px;
}

.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
	<div class="col s12 m9">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<?php echo $message; ?>
				<h5>Configuración de <?php echo $sitename; ?> Hotel</h5>
				<table class="centered striped">
				<form method="post">
				<tbody>
				  <tr>
					<td style="font-size: 12px;"><b>HOST</b></td>
					<td><input name="host" value="<?php echo $client_q['host']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PUERTO</b></td>
					<td><input name="port" value="<?php echo $client_q['port']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>EXT. VARIABLES</b></td>
					<td><input name="external_variables" value="<?php echo $client_q['external_variables']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>EXT. FLASH TEXTS</b></td>
					<td><input name="external_flash_texts" value="<?php echo $client_q['external_flash_texts']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>EXT. FLASH OVERRIDE</b></td>
					<td><input name="external_flash_override_texts" value="<?php echo $client_q['external_flash_override_texts']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>EXT. OVERRIDE VARIABLES</b></td>
					<td><input name="external_override_variables" value="<?php echo $client_q['external_override_variables']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>FURNIDATA</b></td>
					<td><input name="furnidata" value="<?php echo $client_q['furnidata']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>FIGUREDATA</b></td>
					<td><input name="figuredata" value="<?php echo $client_q['figuredata']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>BASE</b></td>
					<td><input name="base" value="<?php echo $client_q['base']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>HABBO .SWF</b></td>
					<td><input name="habbo_swf" value="<?php echo $client_q['habbo_swf']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>NOMBRE SERVIDOR</b></td>
					<td><input name="sitename" value="<?php echo $client_q['sitename']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>URL SERVIDOR</b></td>
					<td><input name="site" value="<?php echo $client_q['site']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PRODUCTDATA</b></td>
					<td><input name="productdata" value="<?php echo $client_q['productdata']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>IMAGEN AVATAR</b></td>
					<td><input name="avatarimage" value="<?php echo $client_q['avatarimage']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>IMAGENES DE SWF</b></td>
					<td><input name="swfimages" value="<?php echo $client_q['swfimages']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>URL PANEL</b></td>
					<td><input name="hk" value="<?php echo $client_q['hk']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>RANGO PANEL</b><br>Rango mínimo para entrar</td>
					<td><input name="minrank" value="<?php echo $client_q['minrank']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>CHAT ID (xat.com)</b></td>
					<td><input name="chatid" value="<?php echo $client_q['chatid']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>ESTADO CLIENT</b><br>Está <?php if ($client_q['status_client'] == 'on') { ?><font color="green" style="font-weight: bold;">abierto</font><?php } ?><?php if ($client_q['status_client'] == 'off') { ?><font color="red" style="font-weight: bold;">cerrado</font><?php } ?></td>
					<td><select name="status_client"><?php if ($client_q['status_client'] == 'off') { ?><option value="off">Cerrado</option><option value="on">Abrir</option><?php } ?><?php if ($client_q['status_client'] == 'on') { ?><option value="on">Abierto</option><option value="off">Cerrar</option><?php } ?></select></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"></td>
					<td><button name="save" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</tbody>
				</form>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>