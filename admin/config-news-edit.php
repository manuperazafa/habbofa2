<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
  
$user_id = $_GET['user'];
$user_edit_a = mysql_query("SELECT * FROM heliocms_news WHERE id='$user_id'");
if (mysql_num_rows($user_edit_a) == 0) {
header ("Location: config-news.php?error=$w");
}
$user_edit_q = mysql_fetch_assoc($user_edit_a);
if (isset($_POST['save'])) {
$title = $_POST['title'];
$image_url = $_POST['image_url'];
$stext = $_POST['stext'];
$btext = $_POST['btext'];
$color = $_POST['color'];
mysql_query ("UPDATE heliocms_news SET title='$title', image_url='$image_url', stext='$stext', btext='$btext', color='$color' WHERE id=$user_edit_q[id]");
header ("Location: config-news.php?saved=$w");
mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Noticias', 'Ha editado noticia id <b>". $user_edit_q['id'] ."</b>.', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
}
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Noticias > <?php echo $user_edit_q['title']; ?></h5>
				<table class="centered striped">
				<tbody>
				<form method="post">
				  <tr>
					<td style="font-size: 12px;"><b>TITULO</b></td>
					<td><input name="title" value="<?php echo $user_edit_q['title']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>IMÁGEN</b></td>
					<td><input name="image_url" value="<?php echo $user_edit_q['image_url']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>TEXTO CORTO</b></td>
					<td><input name="stext" value="<?php echo $user_edit_q['stext']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>CONTENIDO<BR>TEXTO</b></td>
					<td><textarea name="btext" id="texto" style="height: 200px;background: #fff;color: rgba(184, 183, 183, 0.72) !important;border: 1px solid rgba(184, 183, 183, 0.72) !important;border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;"><?php echo $user_edit_q['btext']; ?></textarea></td>
					<script>CKEDITOR.replace( 'texto' );</script>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><b>COLOR</b></td>
					<td><select name="color"><option value="#6627ab">Lila</option><option value="#ffa500">Naranja</option><option value="#2767a7">Azul</option><option value="#469543">Verde</option><option value="#532D40">Marrón</option><option value="#a40429">Rojo</option><option value="#424242">Gris</option><option value="#dec900">Dorado</option></select></td>
				  </tr>
				  <tr>
					<td></td>
					<td><button name="save" type="submit" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>