<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input[type="text"] {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
input[type="file"] {
	
}
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Placas <i>(Hay <?php $users = mysql_query("SELECT * from badge_definitions"); echo mysql_num_rows($users); ?> placas)</i></h5>
				Recuerda que si subes una placa que se llame "EJEMPLO.gif" el nombre de la placa debe ser "EJEMPLO", sino no podrás tener la placa.
				<table class="centered striped">
				<tbody>
				<form action="config-badges-upload.php" method="post" enctype="multipart/form-data">
				  <tr>
					<td style="font-size: 12px;"><b>CÓDIGO</b><br>Ejemplo: ADM</td>
					<td><input type="text" name="badge_code"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>NOMBRE</b><br>Ejemplo: Administrador</td>
					<td><input type="text" name="badge_title"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>DESCRIPCIÓN</b><br>Ejemplo: Administrador <br>en <?php echo $sitename; ?> Hotel</td>
					<td><input type="text" name="badge_desc"></td>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><b>ARCHIVO</b><br>Solo <b>.gif</b></td>
					<td><input type="file" name="fileToUpload" id="fileToUpload"></td>
				  </tr>
				  <tr>
					<td></td>
					<td><input name="submit" type="submit" class="btn" style="background: #1e282c;box-shadow: none;" value="Añadir placa"></button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>