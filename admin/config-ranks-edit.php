<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
  
$user_id = $_GET['user'];
$user_edit_a = mysql_query("SELECT * FROM ranks WHERE id='$user_id'");
if (mysql_num_rows($user_edit_a) == 0) {
header ("Location: config-ranks.php?error=$w");
}
$user_edit_q = mysql_fetch_assoc($user_edit_a);
if (isset($_POST['save'])) {
$id = $_POST['id'];
$name = $_POST['name'];
$badgeid = $_POST['badgeid'];
$tab_colour = $_POST['tab_colour'];
mysql_query ("UPDATE ranks SET id='$id', name='$name', badgeid='$badgeid', tab_colour='$tab_colour' WHERE id=$user_edit_q[id]");
header ("Location: config-ranks.php?saved=$w");
mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Administrar rangos', 'Ha editado el rango ". $user_edit_q['name'] .".', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
}
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Administrar rangos > <?php echo $user_edit_q['name']; ?></h5>
				<table class="centered striped">
				<tbody>
				<form method="post">
				  <tr>
					<td style="font-size: 12px;"><b>ID</b></td>
					<td><input name="id" value="<?php echo $user_edit_q['id']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><b>NOMBRE</b></td>
					<td><input name="name" value="<?php echo $user_edit_q['name']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><b>PLACA</b></td>
					<td><input name="badgeid" value="<?php echo $user_edit_q['badgeid']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><div style="width: 10px;height: 10px;background: <?php echo $user_edit_q['tab_colour']; ?>;float: right;"></div><b>COLOR</b></td>
					<td><input name="tab_colour" value="<?php echo $user_edit_q['tab_colour']; ?>"></td>
				  </tr>
				  <tr>
					<td></td>
					<td><button name="save" type="submit" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>