<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
  
$user_id = $_GET['user'];
$user_edit_a = mysql_query("SELECT * FROM catalog_pages WHERE id='$user_id'");
if (mysql_num_rows($user_edit_a) == 0) {
header ("Location: config-catalogue.php?error=$w");
}
$user_edit_q = mysql_fetch_assoc($user_edit_a);
if (isset($_POST['save'])) {
$id = $_POST['id'];
$parent_id = $_POST['parent_id'];
$caption = $_POST['caption'];
$icon_image = $_POST['icon_image'];
$visible = $_POST['visible'];
$enabled = $_POST['enabled'];
$min_rank = $_POST['min_rank'];
$order_num = $_POST['order_num'];
$page_link = $_POST['page_link'];
$page_layout = $_POST['page_layout'];
$page_strings_1 = $_POST['page_strings_1'];
$page_strings_2 = $_POST['page_strings_2'];
mysql_query ("UPDATE catalog_pages SET id='$id', parent_id='$parent_id', caption='$caption', icon_image='$icon_image', visible='$visible', enabled='$enabled', min_rank='$min_rank', order_num='$order_num', page_link='$page_link', page_layout='$page_layout', page_strings_1='$page_strings_1', page_strings_2='$page_strings_2' WHERE id=$user_edit_q[id]");
header ("Location: config-catalogue.php?saved=$w");
mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Catálogo', 'Ha editado el catálogo (". $user_edit_q['caption'] .").', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
}
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Cátalogo > Editar página > <?php echo $user_edit_q['caption']; ?></h5>
				<table class="centered striped">
				<tbody>
				<form method="post">
				  <tr>
					<td style="font-size: 12px;"><b>ID</b></td>
					<td><input name="id" value="<?php echo $user_edit_q['id']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PARENT ID</b></td>
					<td><input name="parent_id" value="<?php echo $user_edit_q['parent_id']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PÁGINA</b></td>
					<td><input name="caption" value="<?php echo $user_edit_q['caption']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>ICONO IMG</b></td>
					<td><input name="icon_image" value="<?php echo $user_edit_q['icon_image']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>VISIBILIDAD</b></td>
					<td><select name="visible"><?php if ($user_edit_q['visible'] == '0') { ?><option value="0">Oculto</option><option value="1">Mostrar</option><?php } ?><?php if ($user_edit_q['visible'] == '1') { ?><option value="1">Visible</option><option value="0">Ocultar</option><?php } ?></select></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>ESTADO</b></td>
					<td><select name="enabled"><?php if ($user_edit_q['enabled'] == '0') { ?><option value="0">Desactivado</option><option value="1">Activar</option><?php } ?><?php if ($user_edit_q['enabled'] == '1') { ?><option value="1">Activado</option><option value="0">Desactivar</option><?php } ?></select></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>RANGO</b></td>
					<td><select name="min_rank"><?php $b = mysql_query("SELECT * FROM ranks"); while($a = mysql_fetch_assoc($b)){ ?><option value="<?php echo $a['id']; ?>"><?php echo $a['name']; ?></option><?php } ?></select></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>ORDER NUM</b></td>
					<td><input name="order_num" value="<?php echo $user_edit_q['order_num']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PAGE LINK</b></td>
					<td><input name="page_link" value="<?php echo $user_edit_q['page_link']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PAGE LAYOUT</b></td>
					<td><input name="page_layout" value="<?php echo $user_edit_q['page_layout']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PAGE STRINGS 1</b></td>
					<td><input name="page_strings_1" value="<?php echo $user_edit_q['page_strings_1']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>PAGE STRINGS 2</b></td>
					<td><input name="page_strings_2" value="<?php echo $user_edit_q['page_strings_2']; ?>"></td>
				  </tr>
				  
				  <tr>
					<td></td>
					<td><button name="save" type="submit" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>