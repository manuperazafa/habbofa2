<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
  
$user_id = $_GET['user'];
$user_edit_a = mysql_query("SELECT * FROM heliocms_badgestore WHERE id='$user_id'");
if (mysql_num_rows($user_edit_a) == 0) {
header ("Location: config-shop-badges.php?error=$w");
}
$user_edit_q = mysql_fetch_assoc($user_edit_a);
if (isset($_POST['save'])) {
$code = $_POST['code'];
$price = $_POST['price'];
mysql_query ("UPDATE heliocms_badgestore SET code='$code', price='$price' WHERE id=$user_edit_q[id]");
header ("Location: config-shop-badges.php?saved=$w");
mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Tienda de placas', 'Ha editado <img src='". $swfimages ."". $user_edit_q['code'] .".gif'>.', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
}
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Tienda de placas > <img src="<?php echo $swfimages; ?><?php echo $user_edit_q['code']; ?>.gif"></h5>
				<table class="centered striped">
				<tbody>
				<form method="post">
				  <tr>
					<td style="font-size: 14px;"><b>CÓDIGO</b><br>Ejemplo: <?php echo $user_edit_q['code']; ?></td>
					<td><input name="code" value="<?php echo $user_edit_q['code']; ?>"></td>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><b>PRECIO (<img src="<?php echo $site; ?>/web/images/icon_diamonds.png">)</b></td>
					<td><input name="price" value="<?php echo $user_edit_q['price']; ?>"></td>
				  </tr>
				  <tr>
					<td></td>
					<td><button name="save" type="submit" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>