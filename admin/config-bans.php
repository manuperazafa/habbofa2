<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
  
  
if (isset($_POST['save'])) {
$value = $_POST['value'];
$reason = $_POST['reason'];
$expire = $_POST['expire'];
mysql_query("INSERT INTO bans (value,reason,expire,added_by,added_date) VALUES ('$value','$reason','$expire','$user_q[username]','". time() ."')");
header ("Location: config-bans.php?saved=$w");
mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Banear un usuario', 'Ha baneado a $value', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
}
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Banear un usuario</h5>
				<table class="centered striped">
				<tbody>
				<form method="post">
				  <tr>
					<td style="font-size: 12px;"><b>Usuario</b></td>
					<td><input name="value" required></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>Razón</b></td>
					<td><input name="reason" required></td>
				  </tr>
				  <tr>
					<td style="font-size: 12px;"><b>Tiempo</b></td>
					<td><select name="expire">
					<option value="1476123000">1 hora</option>
					<option value="1476133000">6 horas</option>
					<option value="1476173000">12 horas</option>
					<option value="86400">1 día</option>
					<option value="604800">1 semana</option>
					<option value="1478800000">1 mes</option>
					<option value="1507600000">1 año</option>
					<option value="1539100000">2 años</option>
					<option value="1791600000">10 años</option>
					</select></td>
				  </tr>
				  <tr>
					<td></td>
					<td><button name="save" type="submit" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
			  
			  <hr>
			  <table class="centered striped">
				<thead>
					<tr>
						<th data-field="id">#</th>
						<th data-field="id">Usuario</th>
						<th data-field="id">Expira</th>
						<th data-field="id">Baneado</th>
					</tr>
				</thead>
				<tbody>
			  <?php $b = mysql_query("SELECT * FROM bans ORDER BY id DESC"); while($a = mysql_fetch_assoc($b)){ ?>
				<tr>
					<td style="font-size: 12px;"><b><?php echo $a['id']; ?></b></td>
					<td style="font-size: 12px;"><b><?php echo $a['value']; ?></b></td>
					<td style="font-size: 12px;"><b><?php print date_numeric("d/M/Y H:i", $a['expire']); ?></b></td>
					<td style="font-size: 12px;"><b><?php echo $a['added_by']; ?></b></td>
				  </tr>
			  <?php } ?>
			    </tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>