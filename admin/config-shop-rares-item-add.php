<?php
require_once('core.php');
require_once('session.php');
require_once('web/templates/navbar.php');
require_once('../heliocms/core.php');
if (isset($_POST['post'])) {
$furni_id = $_POST['furni_id'];
$furni_selected = $_POST['furni_selected'];
mysql_query("INSERT INTO mercuryzero_rares_items (furni_id,furni_selected) VALUES ('$furni_id','$furni_selected')");
header("Location: config-shop-rares.php?created=$w");
mysql_query("INSERT INTO stafflogs (action, message, note, userid, timestamp) VALUES ('Tienda de rares', '[ITEM] Rare añadido.', '". $user_q['rank'] ."', '". $user_q['id'] ."', '". time() ."')");
}
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Tienda de rares > Añadir item</h5>
				Recuerda que estás agregando un furni en la base de datos. Esto <b>NO</b> quiere decir que se pondrá en la tienda, sino al momento de añadir un rare
				ahí saldrá el furni que hayas agregado aquí.<br>
				<a href="https://habtium.es/furni" target="_blank">Haciendo clic aquí encontrarás muchos furnis</a>
				<table class="centered striped">
				<tbody>
				<form method="post">
				  <tr>
					<td style="font-size: 14px;"><b>NOMBRE</b><br>Ejemplo: Infobus</td>
					<td><input name="furni_selected" placeholder="Introduce el nombre del rare"></td>
				  </tr>
				  <tr>
					<td style="font-size: 14px;"><b>ID FURNI</b><br>Ejemplo: 101</td>
					<td><input name="furni_id" placeholder="Introduce el id del rare"></td>
				  </tr>
				  <tr>
					<td></td>
					<td><button name="post" type="submit" class="btn" style="background: #1e282c;box-shadow: none;">Guardar cambios</button></td>
				  </tr>
				</form>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>