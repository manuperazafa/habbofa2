<?php
  require_once('core.php');
  require_once('web/templates/navbar.php');
  require_once('../heliocms/core.php');
  require_once('session.php');
?>
<style>
.box-info {
	color: #fff;
	width: 70px;
	padding-top: 9px;
	height: 80px;
	border-radius: 2px 0px 0px 2px;
	text-align: center;
}

.box-info .blue {background: #00bff1;}
.box-info .red {background: #df4a32;}
.box-info .orange {background: #f59d00;}
.box-info .green {background: #00e67c;}

.bantype {
    color: #fff;
    padding: 8px 0px;
    border-radius: 4px;
}
.bantype .red {background: #F44336;}
.bantype .orange {background: #ff9800;}
.bantype .blue {background: #2196F3;}

input {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 0px 0px 2px !important;
		margin-bottom: 0px !important;
	}
	
	select {
		background: #fff !important;
		border: 1px solid rgba(184, 183, 183, 0.72) !important;
		border-bottom: 2px solid rgba(184, 183, 183, 0.72) !important;
		border-radius: 3px !important;
		width: 100% !important;
		color: rgba(184, 183, 183, 0.72) !important;
		height: 35px !important;
		padding: 0px 9px !important;
		margin-bottom: 0px !important;
	}
	
	input:focus {
		border-bottom: none 1px solid rgba(184, 183, 183, 0.72);
		box-shadow: none !important;
	}
</style>
<div class="container">
 
  <div class="row">
 
	<div class="col s12 m12">
		<div class="card blue-white darken-1">
            <div class="card-content black-text">
				<h5>Contraseñas perdidas <i>(Hay <?php $users = mysql_query("SELECT * from heliocms_forgotten"); echo mysql_num_rows($users); ?> ticket's)</i></h5>
				<table class="centered striped">
				<thead>
					<th>#</th>
					<th>Usuario</th>
					<th>Correo electrónico</th>
					<th>Opciones</th>
				</thead>
				<tbody>
	<?php
	$forgotten_a = mysql_query("SELECT * from heliocms_forgotten");
	$i = 0; while($forgotten_q = mysql_fetch_assoc($forgotten_a)){
	$user = mysql_query("SELECT * FROM users WHERE mail ='". $forgotten_q['email'] ."'");
	$id = mysql_fetch_assoc($user);
	if (isset($_POST['delete_'.$forgotten_q['id'].''])) {
	mysql_query("DELETE FROM heliocms_forgotten WHERE id=$forgotten_q[id]");
	header ("Location: config-fpass.php?deleted=$w");
	}
	?>
											 
				  <tr>
					<td style="font-size: 12px;"><b><?php echo $forgotten_q['id']; ?></b></td>
					<td style="font-size: 12px;"><b><?php echo $id['username']; ?></b></td>
					<td style="font-size: 14px;"><b><?php echo $forgotten_q['email']; ?></b></td>
					<td style="font-size: 14px;"><form method="post"><a class="btn" style="background: #1e282c;box-shadow: none;margin-right: 3px;" href="config-fpass-define.php?user=<?php echo $forgotten_q['email']; ?>&fid=<?php echo $forgotten_q['id']; ?>"><i class="material-icons">done_all</i></a></form></td>
				  </tr>
				<?php } ?>
				</tbody>
			  </table>
            </div>
        </div>
	</div>
  </div>
</div>