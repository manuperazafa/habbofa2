/**
 * @author Atome Server
 * @page www.atomeserver.com
 * @copyright Atome 2012
 */

function start_quiz(){
document.getElementById('overlay').style.display='block';
document.getElementById('quiz_layer').style.display='block';
}

function close_quiz(){
document.getElementById('overlay').style.display='none';
document.getElementById('quiz_layer').style.display='none';
Load('ajax/ajax.quiz.php?step=1&ajax=true');
}

function Load(page,step){
if(step >= 1){
if(document.getElementById('option_1').checked){
var option = 1;
}else if(document.getElementById('option_2').checked){
var option = 2;
}else if(document.getElementById('option_3').checked){
var option = 3;
}

$.post("ajax/ajax.quiz_options.php?step="+step+"&option="+option+"");
}
var content = document.getElementById('quiz_content').innerHTML;
$('#quiz_content').html(content);
$('#quiz_content').load(page);
}

function radio_select(option){
document.getElementById('option_'+option+'').checked = true;
if(option == "1"){
document.getElementById('radio_1').src='/images/quiz_radio_b.png';
document.getElementById('radio_2').src='/images/quiz_radio_a.png';
document.getElementById('radio_3').src='/images/quiz_radio_a.png';
}else if(option == "2"){
document.getElementById('radio_1').src='/images/quiz_radio_a.png';
document.getElementById('radio_2').src='/images/quiz_radio_b.png';
document.getElementById('radio_3').src='/images/quiz_radio_a.png';
}else if(option == "3"){
document.getElementById('radio_1').src='/images/quiz_radio_a.png';
document.getElementById('radio_2').src='/images/quiz_radio_a.png';
document.getElementById('radio_3').src='/images/quiz_radio_b.png';
}
}

function anularBotonDerecho(e) {
 
 if (navigator.appName == 'Netscape'
       && (e.which == 3 || e.which == 2)){
   alert(sMensaje);
   return false;
 } else if (navigator.appName == 'Microsoft Internet Explorer'
       && (event.button == 2)) {
   alert(sMensaje);
 }
}
document.onmousedown=anularBotonDerecho;
document.oncontextmenu=new Function("return false");

function event_false() {
window.event.returnValue = false
}
document.onselectstart = event_false