<?php
require_once ('heliocms/core.php');
if (isset($_SESSION['id'])) {
header ("Location: $site");
}
$emailaddress_class = 'form__input ng-pristine ng-invalid ng-invalid-required ng-valid-email ng-touched';
$passwordnewrepeat_class = 'form__input ng-pristine ng-untouched password-toggle-mask ng-invalid ng-invalid-required ng-valid-matches';
$passwordnew_class = 'form__input ng-pristine password-toggle-mask ng-invalid ng-invalid-required ng-valid-password-name ng-valid-password-email ng-valid-minlength ng-valid-maxlength ng-valid-password-pattern ng-touched';
$username_class = 'form__input ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-email';
$day_class = 'form__select birthdate__day ng-pristine ng-untouched ng-valid';
$month_class = 'form__select birthdate__month ng-pristine ng-untouched ng-valid';
$year_class = 'form__select birthdate__year ng-pristine ng-untouched ng-valid';
if (isset($_POST['register'])) {
$email = $_POST['emailAddress'];
$password = $_POST['passwordNew'];
$password_repeat = $_POST['passwordNewRepeated'];
$username = $_POST['username'];
$termsOfServiceAccepted = $_POST['termsOfServiceAccepted'];
$email_verify = mysql_query("SELECT * FROM users WHERE mail='$email' LIMIT 1");
if (mysql_num_rows($email_verify) == 1) {
$error = '1';
$emailaddress_errors = '<div ng-if="form.emailAddress.$invalid &amp;&amp; (!form.emailAddress.$pristine || form.$submitted)" ng-messages="form.emailAddress.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: email, remoteDataEmailInvalid --><!-- ngMessage: required --><!-- ngMessage: remoteDataEmailUsedInRegistration --><div ng-message="remoteDataEmailUsedInRegistration">El email proporcionado ya se ha utilizado. Si estás tratando de crear un nuevo usuario, inicia sesión con tu cuenta de <?php echo $sitename; ?> y crea otro avatar.</div><!-- ngMessage: remoteDataEmailUsedInChange --></div>';
$emailaddress_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}else{
if (empty($email)) {
$error = '1';
$emailaddress_errors = '<div ng-if="form.emailAddress.$invalid &amp;&amp; (!form.emailAddress.$pristine || form.$submitted)" ng-messages="form.emailAddress.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: email, remoteDataEmailInvalid --><div ng-message="email, remoteDataEmailInvalid">Este campo es obligatorio.</div><!-- ngMessage: required --><!-- ngMessage: remoteDataEmailUsedInRegistration --><!-- ngMessage: remoteDataEmailUsedInChange --></div>';
$emailaddress_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}else{
if (!preg_match("/^[A-Z0-9._-]{2,}+@[A-Z0-9._-]{2,}\.[A-Z0-9._-]{2,}$/i", $email)) {
$error = '1';
$emailaddress_errors = '<div ng-if="form.emailAddress.$invalid &amp;&amp; (!form.emailAddress.$pristine || form.$submitted)" ng-messages="form.emailAddress.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: email, remoteDataEmailInvalid --><div ng-message="email, remoteDataEmailInvalid">Tienes que ingresar un email válido.</div><!-- ngMessage: required --><!-- ngMessage: remoteDataEmailUsedInRegistration --><!-- ngMessage: remoteDataEmailUsedInChange --></div>';
$emailaddress_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}}}
if (empty($password)) {
$error = '1';
$passwordNew_errors = '<div ng-if="form.passwordNew.$invalid &amp;&amp; (!form.passwordNew.$pristine || form.$submitted)" ng-messages="form.passwordNew.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><div ng-message="required">Este campo es obligatorio.</div><!-- ngMessage: maxlength --><!-- ngMessage: minlength --><!-- ngMessage: passwordPattern --><!-- ngMessage: passwordName, remoteDataPasswordName --><!-- ngMessage: passwordEmail, remoteDataPasswordEmail --></div>';
$passwordnew_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}else{
if(strlen($password) < 6) {
$error = '1';
$passwordNew_errors = '<div ng-if="form.passwordNew.$invalid &amp;&amp; (!form.passwordNew.$pristine || form.$submitted)" ng-messages="form.passwordNew.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><!-- ngMessage: maxlength --><!-- ngMessage: minlength --><div ng-message="minlength" class=""><div class="password-strength" model-value="passwordNew" model-name="passwordNew" error-label="ERROR_PASSWORD_MIN_LENGTH"><div class="password-strength__label"><span>Seguridad de la contraseña:</span> <span>Baja</span></div><div class="password-strength__field"><div ng-class="(score | strengthRating)" class="password-strength__indicator fail" style="width: 0%;"></div></div><!-- ngIf: errorLabel --><p ng-if="errorLabel" class="password-strength__error">Tienes que introducir una contraseña más larga. Al menos 6 caracteres.</p><!-- end ngIf: errorLabel --></div></div><!-- ngMessage: passwordPattern --><!-- ngMessage: passwordName, remoteDataPasswordName --><!-- ngMessage: passwordEmail, remoteDataPasswordEmail --></div>';
$passwordnew_class = 'form__input password-toggle-mask ng-valid-password-name ng-valid-password-email ng-valid-maxlength ng-dirty ng-valid-parse ng-touched ng-valid-password-pattern ng-invalid ng-valid-required ng-invalid-minlength';
}else{
if (empty($password_repeat)) {
$error = '1';
$passwordNewRepeat_errors = '<div ng-if="form.passwordNew.$invalid &amp;&amp; (!form.passwordNew.$pristine || form.$submitted)" ng-messages="form.passwordNew.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><div ng-message="required">Este campo es obligatorio.</div><!-- ngMessage: maxlength --><!-- ngMessage: minlength --><!-- ngMessage: passwordPattern --><!-- ngMessage: passwordName, remoteDataPasswordName --><!-- ngMessage: passwordEmail, remoteDataPasswordEmail --></div>';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}else{
if ($password <> $password_repeat) {
$error = '1';
$passwordNewRepeat_errors = '<div ng-if="form.passwordNew.$valid &amp;&amp; form.passwordNewRepeated.$invalid" ng-messages="form.passwordNewRepeated.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><!-- ngMessage: matches --><div ng-message="matches">Las contraseñas no coinciden.</div></div>';
$passwordnewrepeat_class = 'form__input password-toggle-mask ng-dirty ng-valid-parse ng-valid-required ng-touched ng-invalid ng-invalid-matches';
}}}}
$username_verify = mysql_query("SELECT * FROM users WHERE username='$username' LIMIT 1");
if (mysql_num_rows($username_verify) == 1) {
$error = '1';
$username_errors = '<div ng-if="form.emailAddress.$invalid &amp;&amp; (!form.emailAddress.$pristine || form.$submitted)" ng-messages="form.emailAddress.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: email, remoteDataEmailInvalid --><div ng-message="email, remoteDataEmailInvalid">Ya existe un usuario con ese nombre en <?php echo $sitename; ?>.</div><!-- ngMessage: required --><!-- ngMessage: remoteDataEmailUsedInRegistration --><!-- ngMessage: remoteDataEmailUsedInChange --></div>';
$username_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}else{
if (empty($username)) {
$error = '1';
$username_errors = '<div ng-if="form.emailAddress.$invalid &amp;&amp; (!form.emailAddress.$pristine || form.$submitted)" ng-messages="form.emailAddress.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: email, remoteDataEmailInvalid --><div ng-message="email, remoteDataEmailInvalid">Este campo es obligatorio.</div><!-- ngMessage: required --><!-- ngMessage: remoteDataEmailUsedInRegistration --><!-- ngMessage: remoteDataEmailUsedInChange --></div>';
$username_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}else{
if (!preg_match("/^[A-Z0-9=?!@:.-]{2,15}$/i", $username)) {
$error = '1';
$username_errors = '<div ng-if="form.emailAddress.$invalid &amp;&amp; (!form.emailAddress.$pristine || form.$submitted)" ng-messages="form.emailAddress.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: email, remoteDataEmailInvalid --><div ng-message="email, remoteDataEmailInvalid">Tienes que ingresar un nombre de usuario válido.</div><!-- ngMessage: required --><!-- ngMessage: remoteDataEmailUsedInRegistration --><!-- ngMessage: remoteDataEmailUsedInChange --></div>';
$username_class = 'form__input ng-invalid ng-dirty ng-invalid-email ng-valid-required ng-touched';
}}}
if ($termsOfServiceAccepted <> 1) {
$error = '1';
$termsOfServiceAccepted_errors = '<div ng-if="form.termsOfServiceAccepted.$invalid &amp;&amp; (!form.termsOfServiceAccepted.$pristine || form.$submitted)" ng-messages="form.termsOfServiceAccepted.$error" class="form__popover form__popover--error ng-active" style=""><!-- ngMessage: required --><div ng-message="required">Este campo es obligatorio.</div></div>';
}
if ($error <> 1) {
mysql_query("INSERT INTO users (online,vip_points,vip,username,password,mail,auth_ticket,rank,look,gender,motto,last_online,account_created,ip_last,ip_reg) VALUES ('0','0','0','$username','".MD5($password)."','$email','','1','ch-215-1408.hd-195-1.lg-285-82.hr-115-45.sh-290-91','M','','".time()."','".time()."','$ip','$ip')");
$user_verify = mysql_query("SELECT * FROM users WHERE mail='$email' && password='".MD5($password)."'");
$user_fetch = mysql_fetch_assoc($user_verify);
mysql_query("INSERT INTO heliocms_profilesettings (email,profile_visible,safety_questions) VALUES ('$email','1','0')");
mysql_query("INSERT INTO heliocms_sessions (user_id,last) VALUES ('$user_fetch[id]','".time()."')");
mysql_query("INSERT INTO heliocms_avatars (user_id,parent_email) VALUES ('$user_fetch[id]','$user_fetch[mail]')");
$_SESSION['id'] = $user_fetch['id'];
header ("Location: $site/hotel");
}}
?>
<html>
  <head>
	<title><?php echo $sitename; ?></title>
	<link type="text/css" href="<?php echo $site; ?>/web/css/animate.css" rel="stylesheet">
  <style>
	body {
		background: url(<?php echo $site; ?>/web/images/Reception_hblooza_backdrop_left.png) no-repeat 0% 100%,url(<?php echo $site; ?>/web/images/clouds.png) #00BCD4;
		font-family: "Roboto", sans-serif;
		font-weight: normal;
		color: grey;
	}
	
	.container {
		margin-left: auto;
		margin-right: auto;
		width: 25%;
	}
	
	.card {
		width: 100%;
		padding: 13px;
		background: #fff;
		margin-top: 5px;
		border-radius: 5px;
	}
	
	input {
		border-radius: 3px;
		margin-bottom: 3px;
		border: 1px solid grey;
		width: 100%;
		padding: 15px;
		font-size: 15px;
		color: grey;
	}
	
	.button-radio {
		width: 25px;
		margin: -4px;
	}
	
	.button-register {
		background: orange;
		width: 100%;
		border: none;
		border-radius: 3px;
		color: #fff;
		padding: 15px;
		margin: 20px 0px 5px 0px;
	}
	
	.button-back {
		background: url('<?php echo $site; ?>/web/images/front_page_beach.png') no-repeat #bf1d1d -4% 18%;
		width: 100% !important;
		border: none;
		color: #fff;
		padding: 15px;
		margin-left: 12px;
		margin-top: 4px;
		border-radius: 3px;
	}
	
	@media (min-width: 0px) and (max-width: 600px) {
    .container {
		width: 80%;
	}
	}
  </style>
  </head>
  
	<body>
	<div class="container animated fadeInLeftBig">
		<img src="http://www.habbcrazy.net/resources/fonts/118/<?php echo $sitename; ?>.gif" alt="<?php echo $sitename; ?> logo" style="margin-top: 37px;">
		<div class="card">
		<?php echo $message; ?>
	  <form method="post" name="registrationForm">
		<input class="<?php echo $username_class; ?>" value="<?php echo $username; ?>" name="username" type="text" placeholder="Nombre de usuario" required>
		<input class="<?php echo $passwordnew_class; ?>" value="<?php echo $password; ?>" name="passwordNew" type="password" placeholder="Contraseña" required>
		<input class="<?php echo $passwordnewrepeat_class; ?>" value="<?php echo $password_repeat; ?>" name="passwordNewRepeated" type="password" placeholder="Repite contraseña" required>
		<input class="<?php echo $emailaddress_class; ?>" value="<?php echo $email; ?>" name="emailAddress" type="email" placeholder="Correo electrónico" required>
		<br>
		<br>
		<center>
		<input name="termsOfServiceAccepted" value="1" type="radio" class="button-radio" required <?php if ($termsOfServiceAccepted == '1') { ?>checked<?php } ?>>
		<label>Acepto los <a style="color: grey;text-decoration: none;" href="https://help.habbo.es/hc/es/articles/221679748">Términos y Condiciones</a></label>
		</center>
		<button name="register" type="submit" class="button-register">Registrarme</button>
		</div>
	  </form>
		<br>
		<a href="<?php echo $site; ?>"><button class="button-back">Volver a atrás</button></a>
	</div>
	</body>
</html>